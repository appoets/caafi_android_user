package com.caafisom.android.user.view.activity;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.ScheduleResponse;
import com.caafisom.android.user.presenter.SchedulePresenter;
import com.caafisom.android.user.presenter.ipresenter.ISchedulePresenter;
import com.caafisom.android.user.view.adapter.ScheduleAdapter;
import com.caafisom.android.user.view.adapter.listener.IScheduleListener;
import com.caafisom.android.user.view.iview.IScheduleView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

public class ScheduleActivity extends BaseActivity<ISchedulePresenter> implements IScheduleView {

    @BindView(R.id.ibBack)
    ImageView ibBack;
    @BindView(R.id.rcvScheduledList)
    RecyclerView rcvScheduledList;
    @BindView(R.id.calendarView)
    CalendarView calendarView;
    @BindView(R.id.noData)
    TextView noData;

    private String selectedDate;
    //private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private ScheduleAdapter adapter;
    private List<ScheduleResponse> wholeresponse = new ArrayList<>();
    private List<ScheduleResponse> selectedDateList;
    private String tempDate;
    private IScheduleListener iScheduleListener;


    @Override
    protected int attachLayout() {
        return R.layout.activity_schedule;
    }

    @Override
    ISchedulePresenter initialize() {
        selectedDate = dateFormat.format(Calendar.getInstance().getTime());
        return new SchedulePresenter(this);
    }

    /*    @Override
    public void setAdapter(ScheduleAdapter adapter, IScheduleListener iScheduleListener) {
        if (adapter.getItemCount() > 0) {
            noData.setVisibility(View.GONE);
            rcvScheduledList.setVisibility(View.VISIBLE);
            rcvScheduledList.setAdapter(adapter);
        } else {
            rcvScheduledList.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }
    }*/

    @OnClick({R.id.ibBack})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        iPresenter.getScheduledList();
    }

    @Override
    public void setAdapter(List<ScheduleResponse> list, IScheduleListener iScheduleListener) {
        if (list.size() > 0) {
            wholeresponse = list;
            this.iScheduleListener = iScheduleListener;
            searchForSelectedDate();
            if (selectedDateList.size() > 0) {
                noData.setVisibility(View.GONE);
                rcvScheduledList.setVisibility(View.VISIBLE);
                adapter = new ScheduleAdapter(selectedDateList, iScheduleListener);
                rcvScheduledList.setAdapter(adapter);
            } else {
                rcvScheduledList.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
            }
        } else {
            rcvScheduledList.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void moveToDetail(ScheduleResponse data) {

        if (data.getStatus().equalsIgnoreCase("CANCELLED")) {
            showToast("Request was Cancelled");
        }/* else if (data.getStatus().equalsIgnoreCase("COMPLETED")) {

        } else if (data.getStatus().equalsIgnoreCase("SCHEDULED")) {

        }*/ else {
            startActivity(new Intent(ScheduleActivity.this, ScheduleDetailActivity.class).putExtra("id", ""+data.getId()).putExtra("status", data.getStatus()));
        }
    }

    @Override
    public void initSetUp() {
        rcvScheduledList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvScheduledList.setItemAnimator(new DefaultItemAnimator());

        calendarView.setDate(Calendar.getInstance().getTimeInMillis(), false, true);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                try {
                    //selectedDate = year + "-" + (month + 1) + "-" + dayOfMonth;
                    Date enddateObj = dateFormat.parse(year + "-" + (month + 1) + "-" + dayOfMonth);
                    selectedDate = dateFormat.format(enddateObj);
                    //selectedDate = dateFormat.format(selectedDate);
                }catch (Exception e) {
                    e.printStackTrace();
                }
                if (wholeresponse.size() > 0)
                    getSelectedDateList();
            }
        });
    }

    private void getSelectedDateList() {
        searchForSelectedDate();
        if (selectedDateList.size() > 0) {
            adapter = new ScheduleAdapter(selectedDateList, iScheduleListener);
            rcvScheduledList.setAdapter(adapter);
            noData.setVisibility(View.GONE);
            rcvScheduledList.setVisibility(View.VISIBLE);
        } else {
            rcvScheduledList.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }
    }

    private void searchForSelectedDate() {
        tempDate = "";
        selectedDateList = new ArrayList<>();
        try {
            for (int i = 0; i < wholeresponse.size(); i++) {
                if (wholeresponse.get(i).getScheduleAt() != null && !wholeresponse.get(i).getScheduleAt().equalsIgnoreCase(null)) {
                    Date enddateObj = dateFormat.parse(wholeresponse.get(i).getScheduleAt().split(" ")[0]);
                    tempDate = dateFormat.format(enddateObj);
                    if (tempDate.equalsIgnoreCase(selectedDate)) {
                        selectedDateList.add(wholeresponse.get(i));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
