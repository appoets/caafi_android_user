package com.caafisom.android.user.videocall;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.dto.common.videocalldata.Data;
import com.caafisom.android.user.model.dto.common.videocalldata.Fcm;
import com.caafisom.android.user.model.dto.common.videocalldata.FcmNotification;
import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;
import com.caafisom.android.user.util.CodeSnippet;


import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;
import static com.caafisom.android.user.common.Constants.StoragePath.WEB_PATH;
import static timber.log.Timber.*;


public class IncomingCallActivity extends AppCompatActivity {

    Activity thisActivity;
    @BindView(R.id.lblName)
    TextView lblName;
    @BindView(R.id.imgProfile)
    CircleImageView imgProfile;
    @BindView(R.id.imgEndCall)
    ImageView imgEndCall;
    @BindView(R.id.imgAcceptCall)
    ImageView imgAcceptCall;


    Handler h = new Handler();
    int delay = 15 * 1000; //1 second=1000 milisecond, 15*1000=15seconds
    Runnable runnable;

//    Vibrator v;

    String senderID = "", senderName = "", senderAvatar = "", senderMobile = "", sender_device_token = "", call_type = "";


    BroadcastReceiver mReceiver;
    IntentFilter mIntentFilter;

    String TAG = "IncomingCallActivity";

    String chatPath;
    @BindView(R.id.lblCallType)
    TextView lblCallType;

    static boolean active = false;



    @Override
    protected void onStart() {
        super.onStart();

        active = true;

    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        setContentView(R.layout.activity_incoming_call);
        ButterKnife.bind(this);
        thisActivity = this;

//        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }


        senderID = extras.getString("id");
        senderName = extras.getString("name");
        senderAvatar = extras.getString("avatar");
        chatPath = extras.getString("chatpath");
        senderMobile = extras.getString("mobile");
        sender_device_token = extras.getString("device_token");
        call_type = extras.getString("call_type");

        //Utilities.printV("senderName====>", senderName);
       // Utilities.printV("senderAvatar====>", senderAvatar);


        lblName.setText(senderName);

        if (call_type.equalsIgnoreCase("audio_call"))
            lblCallType.setText("ZTOMED VOICE CALL");
        else
            lblCallType.setText("ZTOMED VIDEO CALL");

        if (senderAvatar != null && !senderAvatar.equalsIgnoreCase("") && senderAvatar.length() > 0) {

            Glide.with(this).load(WEB_PATH+senderAvatar).apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user)).into(imgProfile);
        }


        mIntentFilter = new IntentFilter("cut_call");


        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                finish();
               // updateAPICall("INCOMING");
            }
        };


    }


    @Override
    public void onBackPressed() {

    }

    @OnClick({R.id.imgEndCall, R.id.imgAcceptCall})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgEndCall:
                initCall("REJECTCALL");
                finish();
                break;
            case R.id.imgAcceptCall:
                initCall("ATTENDCALL");

                /*SharedHelper.putKey(thisActivity, "call_sender_id", senderID);
                SharedHelper.putKey(thisActivity, "call_sender_type", "INCOMING");

                SharedHelper.putKey(thisActivity, "call_sender_name", senderName);
                SharedHelper.putKey(thisActivity, "call_sender_pic", senderAvatar);

                Utilities.printV("CSK== SEND id=>",SharedHelper.getKey(thisActivity, "call_sender_type"));
                Utilities.printV("CSK== SEND call_sender_id=>", SharedHelper.getKey(thisActivity, "call_sender_id"));*/

                Intent i = new Intent(getApplicationContext(), VideoChatActivity.class);
                i.putExtra("chat_path", chatPath);
                startActivity(i);

                stopmReceiver();

                break;
        }
    }

    @Override
    protected void onResume() {

        //SharedHelper.putKey(thisActivity, "call_start_time", Utilities.getCurrentTime());
        registerReceiver(mReceiver, mIntentFilter);


        //start handler as activity become visible


// Start without a delay
// Each element then alternates between vibrate, sleep, vibrate, sleep...
        // If you want to vibrate  in a pattern
//        long pattern[] = {0, 800, 200, 800, 300, 800, 400, 800, 500, 800, 600, 800, 700, 800, 800, 800, 900, 800, 1000, 800};
//        // 2nd argument is for repetition pass -1 if you do not want to repeat the Vibrate
//        v.vibrate(pattern, -1);


        /*runnable = new Runnable() {
            public void run() {


                //Utilities.printV("UPDATE MISSED CALL STATUS===>","MISSED");

                //MISSed call here
                if (active == true)
                    updateAPICall("MISSED");
            }
        };

        h.postDelayed(runnable, delay);*/


        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void finish() {

        super.finish();
    }


    void stopmReceiver() {


//        v.cancel();


        h.removeCallbacks(runnable);


        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
            mReceiver = null;
        }

        thisActivity.finish();


    }


    private void initCall(String type) {


        String timeStamp = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) + "";
      //  Utilities.printV("timeStamp===>", timeStamp);

//        {data={"payload":{"type":"audio"},"mobile":"+918220274902","name":"Esack","from":"make_call","avatar":"http:\/\/whatsapp.venturedemos.com\/uploads\/7de1733c7927d36f5ad1ee620965606617d91fc6.jpg"}}

        String device_token = getApplicationInstance().getFCMToken();
        device_token = new CodeSnippet().encodeBase64(device_token);

        Data fcmData = new Data();
        fcmData.setId(senderID);
        fcmData.setCallType(type);
        fcmData.setPatientName(senderName);
        fcmData.setAvatar(senderAvatar);
        fcmData.setMobile(senderMobile);
        fcmData.setDevice_token(device_token);
        fcmData.setName(chatPath);
        fcmData.setCall_duration("");


        FcmNotification notification = new FcmNotification();
        notification.setTitle(senderName);
        notification.setBody(senderName+" Calling");

        Fcm fcm = new Fcm();
        fcm.setTo(sender_device_token);
        fcm.setData(fcmData);
        // fcm.setFcmNotification(notification);


        // SendFCM(fcmData);
        SendFCM(fcm);

    }


    private void SendFCM(Fcm fcm) {


        // deviceToken = "cMcMegRtrVQ:APA91bHO_exwZR4rI37dpq3HjQxc2o_wlI4dZRDOBKRpIzvEn5LpibgAEzhhxAXArIW61lBeNRwh323IzDtrNn2FPFdKwe4fbt5dFi1Pwhi-iRuecvqLkjpQd2wEQ9JZfea0yodTvrQO";

        Call<BaseResponse> call = new ApiClient().getClient(Constants.URL.FCM_URL).create(ApiInterface.class).sendFCM(fcm);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull Response<BaseResponse> response) {
                v("Send FCM ");
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                v("FCM Failure");
            }
        });
    }


}
