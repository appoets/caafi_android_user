package com.caafisom.android.user.view.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.presenter.DocotorSpecialityListPresenter;
import com.caafisom.android.user.presenter.SearchProviderPresenter;
import com.caafisom.android.user.presenter.ipresenter.IHospitalListPresenter;
import com.caafisom.android.user.view.adapter.ProviderRecyclerAdapter;
import com.caafisom.android.user.view.iview.IHospitalistView;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;

public class HospitalsListActivity extends BaseActivity<SearchProviderPresenter> implements IHospitalistView {

    @BindView(R.id.rv_hospitals)
    RecyclerView rvhospitals;
    @BindView(R.id.heading)
    TextView heading;
    @BindView(R.id.tv_empty)
    TextView tvempty;
    private Integer service_id;


    @Override
    int attachLayout() {
        return R.layout.activity_hospitals_list;
    }

    @SuppressLint("LogNotTimber")
    @Override
    public void initSetUp() {

        Intent intent = getIntent();
        if (intent != null) {
            service_id = intent.getIntExtra("service_id", 0);
        }

        iPresenter.getHosptialsList(String.valueOf(service_id));
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.v(TAG, "Token Received ==>> " + token);
    }

    @Override
    public void moveToDoctoBook(Provider data) {
//        Toast.makeText(this, "Selected: " + data.getId() + " " + data.getFirstName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, DoctorsSpecialityListActivity.class);
        intent.putExtra("hospital_id", data.getId());
        startActivity(intent);
    }

    @Override
    SearchProviderPresenter initialize() {
        return new SearchProviderPresenter(this);
    }


    @Override
    public void setProviderAdapter(ProviderRecyclerAdapter providerRecyclerAdapter) {
        rvhospitals.setHasFixedSize(true);
        rvhospitals.setLayoutManager(new LinearLayoutManager(this));
        rvhospitals.setItemAnimator(new DefaultItemAnimator());
        if (providerRecyclerAdapter.getItemCount() > 0) {
            tvempty.setVisibility(View.GONE);
            rvhospitals.setVisibility(View.VISIBLE);
            rvhospitals.setAdapter(providerRecyclerAdapter);
        } else {
            rvhospitals.setVisibility(View.GONE);
            tvempty.setVisibility(View.VISIBLE);
        }
    }


}