package com.caafisom.android.user.model.dto.common;

import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class City extends BaseResponse {
    @SerializedName("data")
    @Expose
    private List<CityList> data = null;

    public List<CityList> getData() {
        return data;
    }

    public void setData(List<CityList> data) {
        this.data = data;
    }
}
