
package com.caafisom.android.user.model.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.caafisom.android.user.model.dto.common.Services;

import java.util.List;

public class ServicesResponse extends BaseResponse {

    @SerializedName("service")
    @Expose
    private List<Services> services;

    public List<Services> getServices() {
        return services;
    }

    public void setServices(List<Services> services) {
        this.services = services;
    }
}
