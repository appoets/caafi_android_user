package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.model.dto.common.City;
import com.caafisom.android.user.model.dto.common.CityList;
import com.caafisom.android.user.presenter.ipresenter.ICityListPresenter;
import com.caafisom.android.user.view.adapter.CityListRecyclerAdater;

/**
 * Created by Tranxit Technologies.
 */

public interface ICityListView extends IView<ICityListPresenter> {
    void setAdapter(CityListRecyclerAdater adapter);
    void getCity(CityList data);
    void initSetUp();
}
