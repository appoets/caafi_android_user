package com.caafisom.android.user.view.adapter.listener;

import com.caafisom.android.user.model.dto.common.City;
import com.caafisom.android.user.model.dto.common.CityList;
import com.caafisom.android.user.model.dto.common.History;

/**
 * Created by Tranxit Technologies.
 */

public interface ICityListRecyclerAdapter extends BaseRecyclerListener<CityList> {
}
