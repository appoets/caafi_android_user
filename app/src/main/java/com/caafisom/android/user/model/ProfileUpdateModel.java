package com.caafisom.android.user.model;



import com.caafisom.android.user.model.dto.response.ProfileResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ProfileUpdateModel extends BaseModel<ProfileResponse> {

    public ProfileUpdateModel(IModelListener<ProfileResponse> listener) {
        super(listener);
    }

    public void updateProfile(HashMap<String, RequestBody> params, MultipartBody.Part filePart){
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).postProfileUpdate(params,filePart));
    }

    @Override
    public void onSuccessfulApi(ProfileResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<ProfileResponse> response) {

    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
