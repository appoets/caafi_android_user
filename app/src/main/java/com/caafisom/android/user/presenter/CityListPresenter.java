package com.caafisom.android.user.presenter;

import android.os.Bundle;

import com.caafisom.android.user.model.CityListModel;
import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.HistoryModel;
import com.caafisom.android.user.model.dto.common.City;
import com.caafisom.android.user.model.dto.common.CityList;
import com.caafisom.android.user.model.dto.common.History;
import com.caafisom.android.user.model.dto.response.HistoryResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.ICityListPresenter;
import com.caafisom.android.user.view.adapter.CityListRecyclerAdater;
import com.caafisom.android.user.view.adapter.HistoryRecyclerAdater;
import com.caafisom.android.user.view.adapter.listener.ICityListRecyclerAdapter;
import com.caafisom.android.user.view.adapter.listener.IHistoryRecyclerAdapter;
import com.caafisom.android.user.view.iview.ICityListView;
import org.jetbrains.annotations.NotNull;
import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class CityListPresenter extends BasePresenter<ICityListView> implements ICityListPresenter {

    public CityListPresenter(ICityListView iView) {
        super(iView);
        getCityList();
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();

    }

    ICityListRecyclerAdapter iCityListRecyclerAdapter = new ICityListRecyclerAdapter() {
        @Override
        public void onClickItem(int pos, CityList data) {
            iView.getCity(data);
        }
    };

    @Override
    public void getCityList() {
        iView.showProgressbar();
        new CityListModel(new IModelListener<City>() {
            @Override
            public void onSuccessfulApi(@NotNull City response) {
                iView.dismissProgressbar();
                iView.setAdapter(new CityListRecyclerAdater(response.getData(),iCityListRecyclerAdapter));
            }

            @Override
            public void onSuccessfulApi(@NotNull List<City> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {

            }

            @Override
            public void onUnauthorizedUser(CustomException e) {

            }

            @Override
            public void onNetworkFailure() {

            }
        }).getCityList();


    }
}
