package com.caafisom.android.user.presenter;

import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.ProviderModel;
import com.caafisom.android.user.model.SendRequestModel;
import com.caafisom.android.user.model.ServicesModel;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.common.Services;
import com.caafisom.android.user.model.dto.response.ProviderResponse;
import com.caafisom.android.user.model.dto.response.SendRequestResponse;
import com.caafisom.android.user.model.dto.response.ServicesResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.ISpecialistFragmentPresenter;
import com.caafisom.android.user.view.adapter.ProviderRecyclerAdapter;
import com.caafisom.android.user.view.adapter.ServiceRecyclerAdapter;
import com.caafisom.android.user.view.adapter.listener.IProviderRecyclerAdapter;
import com.caafisom.android.user.view.iview.ISpecialistView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class SpecialistFragmentPresenter extends BasePresenter<ISpecialistView> implements ISpecialistFragmentPresenter {

    public SpecialistFragmentPresenter(ISpecialistView iView) {
        super(iView);
    }


    @Override
    public void getServicesList() {
        iView.showProgressbar();
        new ServicesModel(new IModelListener<ServicesResponse>() {

            void onClickItem(int pos, Services data) {
//                searchProvider(data.getId() + "");
                iView.gotoProvider(data.getId());

            }

            @Override
            public void onSuccessfulApi(@NotNull ServicesResponse response) {
                iView.dismissProgressbar();
                iView.setAdapter(new ServiceRecyclerAdapter(response.getServices(), this::onClickItem));
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ServicesResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).getServiceList();
    }


    @Override
    public void searchService(String searchKey) {
        new ServicesModel(new IModelListener<ServicesResponse>() {

            void onClickItem(int pos, Services data) {
                //Item Click function desc here
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ServicesResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull ServicesResponse response) {
                iView.dismissProgressbar();
                iView.setAdapter(new ServiceRecyclerAdapter(response.getServices(), this::onClickItem));
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).searchSpecialist(searchKey);
    }

 /*   @Override
    public void searchProvider(String searchKey) {
        iView.showProgressbar();
        new ProviderModel(new IModelListener<ProviderResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProviderResponse response) {
                iView.dismissProgressbar();
                iView.setProviderAdapter(new ProviderRecyclerAdapter(response.getProvider(), iProviderRecyclerAdapter), response.getProvider().get(0).getService().get(0).getServiceTypeId());
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProviderResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).searchProviderByService(searchKey);
    }*/

    private IProviderRecyclerAdapter iProviderRecyclerAdapter = new IProviderRecyclerAdapter() {
        @Override
        public void onClickItem(int pos, Provider data) {
            iView.moveToDetailView(data);
        }

    };

    @Override
    public void makeVideoCall(Provider data) {
        iView.makeVideoCall(data);
    }

    @Override
    public void scheduleAppointment(Integer serviceTypeId, String date, String time, String broadcast) {
        iView.showProgressbar();
        new SendRequestModel(new IModelListener<SendRequestResponse>() {

            @Override
            public void onSuccessfulApi(@NotNull SendRequestResponse response) {
                iView.dismissProgressbar();
                iView.startCheckStatus(response.getRequest_id());
            }

            @Override
            public void onSuccessfulApi(@NotNull List<SendRequestResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).sendSpecialistRequest(serviceTypeId, date, time, broadcast);
    }
}
