package com.caafisom.android.user.model.dto.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CSS54 on 08-04-2018.
 *
 */

public class CheckStatusResponse extends BaseResponse implements Parcelable {

    @SerializedName("data")
    @Expose
    private List<Data> data = new ArrayList<>();

    private CheckStatusResponse(Parcel in) {
        data = in.createTypedArrayList(Data.CREATOR);
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static final Creator<CheckStatusResponse> CREATOR = new Creator<CheckStatusResponse>() {
        @Override
        public CheckStatusResponse createFromParcel(Parcel in) {
            return new CheckStatusResponse(in);
        }

        @Override
        public CheckStatusResponse[] newArray(int size) {
            return new CheckStatusResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(data);
    }

    public static class Data implements Parcelable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("booking_id")
        @Expose
        private String bookingId;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("provider_id")
        @Expose
        private Integer providerId;
        @SerializedName("current_provider_id")
        @Expose
        private Integer currentProviderId;
        @SerializedName("service_type_id")
        @Expose
        private Integer serviceTypeId;
        @SerializedName("is_meter")
        @Expose
        private Integer isMeter;
        @SerializedName("rental_hours")
        @Expose
        private String rentalHours;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("cancelled_by")
        @Expose
        private String cancelledBy;
        @SerializedName("cancel_reason")
        @Expose
        private String cancelReason;
        @SerializedName("payment_mode")
        @Expose
        private String paymentMode;
        @SerializedName("paid")
        @Expose
        private Integer paid;
        @SerializedName("is_track")
        @Expose
        private String isTrack;
        @SerializedName("distance")
        @Expose
        private String distance;
        @SerializedName("travel_time")
        @Expose
        private Integer travelTime;
        @SerializedName("s_address")
        @Expose
        private String sAddress;
        @SerializedName("s_latitude")
        @Expose
        private Float sLatitude;
        @SerializedName("s_longitude")
        @Expose
        private Float sLongitude;
        @SerializedName("d_address")
        @Expose
        private String dAddress;
        @SerializedName("otp")
        @Expose
        private String otp;
        @SerializedName("d_latitude")
        @Expose
        private Float dLatitude;
        @SerializedName("track_distance")
        @Expose
        private Integer trackDistance;
        @SerializedName("track_latitude")
        @Expose
        private Integer trackLatitude;
        @SerializedName("track_longitude")
        @Expose
        private Integer trackLongitude;
        @SerializedName("d_longitude")
        @Expose
        private Float dLongitude;
        @SerializedName("location_type")
        @Expose
        private String locationType;
        @SerializedName("assigned_at")
        @Expose
        private String assignedAt;
        @SerializedName("schedule_at")
        @Expose
        private String scheduleAt;
        @SerializedName("started_at")
        @Expose
        private String startedAt;
        @SerializedName("finished_at")
        @Expose
        private String finishedAt;
        @SerializedName("user_rated")
        @Expose
        private Integer userRated;
        @SerializedName("provider_rated")
        @Expose
        private Integer providerRated;
        @SerializedName("use_wallet")
        @Expose
        private Integer useWallet;
        @SerializedName("surge")
        @Expose
        private Integer surge;
        @SerializedName("route_key")
        @Expose
        private String routeKey;
        @SerializedName("note_for_driver")
        @Expose
        private String noteForDriver;
        @SerializedName("deleted_at")
        @Expose
        private String deletedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("service_type")
        @Expose
        private ServiceType service_type;
        @SerializedName("provider")
        @Expose
        private Provider provider;
        @SerializedName("provider_service")
        @Expose
        private ProviderService provider_service;
        @SerializedName("payment")
        @Expose
        private Payment payment;

        protected Data(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            bookingId = in.readString();
            if (in.readByte() == 0) {
                userId = null;
            } else {
                userId = in.readInt();
            }
            if (in.readByte() == 0) {
                providerId = null;
            } else {
                providerId = in.readInt();
            }
            if (in.readByte() == 0) {
                currentProviderId = null;
            } else {
                currentProviderId = in.readInt();
            }
            if (in.readByte() == 0) {
                serviceTypeId = null;
            } else {
                serviceTypeId = in.readInt();
            }
            if (in.readByte() == 0) {
                isMeter = null;
            } else {
                isMeter = in.readInt();
            }
            rentalHours = in.readString();
            status = in.readString();
            cancelledBy = in.readString();
            cancelReason = in.readString();
            paymentMode = in.readString();
            if (in.readByte() == 0) {
                paid = null;
            } else {
                paid = in.readInt();
            }
            isTrack = in.readString();
            distance = in.readString();
            if (in.readByte() == 0) {
                travelTime = null;
            } else {
                travelTime = in.readInt();
            }
            sAddress = in.readString();
            if (in.readByte() == 0) {
                sLatitude = null;
            } else {
                sLatitude = in.readFloat();
            }
            if (in.readByte() == 0) {
                sLongitude = null;
            } else {
                sLongitude = in.readFloat();
            }
            dAddress = in.readString();
            otp = in.readString();
            if (in.readByte() == 0) {
                dLatitude = null;
            } else {
                dLatitude = in.readFloat();
            }
            if (in.readByte() == 0) {
                trackDistance = null;
            } else {
                trackDistance = in.readInt();
            }
            if (in.readByte() == 0) {
                trackLatitude = null;
            } else {
                trackLatitude = in.readInt();
            }
            if (in.readByte() == 0) {
                trackLongitude = null;
            } else {
                trackLongitude = in.readInt();
            }
            if (in.readByte() == 0) {
                dLongitude = null;
            } else {
                dLongitude = in.readFloat();
            }
            locationType = in.readString();
            assignedAt = in.readString();
            scheduleAt = in.readString();
            startedAt = in.readString();
            finishedAt = in.readString();
            if (in.readByte() == 0) {
                userRated = null;
            } else {
                userRated = in.readInt();
            }
            if (in.readByte() == 0) {
                providerRated = null;
            } else {
                providerRated = in.readInt();
            }
            if (in.readByte() == 0) {
                useWallet = null;
            } else {
                useWallet = in.readInt();
            }
            if (in.readByte() == 0) {
                surge = null;
            } else {
                surge = in.readInt();
            }
            routeKey = in.readString();
            noteForDriver = in.readString();
            deletedAt = in.readString();
            createdAt = in.readString();
            updatedAt = in.readString();
            user = in.readParcelable(User.class.getClassLoader());
            service_type = in.readParcelable(ServiceType.class.getClassLoader());
            provider = in.readParcelable(Provider.class.getClassLoader());
            provider_service = in.readParcelable(ProviderService.class.getClassLoader());
            payment = in.readParcelable(Payment.class.getClassLoader());
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public Provider getProvider() {
            return provider;
        }

        public void setProvider(Provider provider) {
            this.provider = provider;
        }

        public ProviderService getProvider_service() {
            return provider_service;
        }

        public void setProvider_service(ProviderService provider_service) {
            this.provider_service = provider_service;
        }

        public Payment getPayment() {
            return payment;
        }

        public void setPayment(Payment payment) {
            this.payment = payment;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getBookingId() {
            return bookingId;
        }

        public void setBookingId(String bookingId) {
            this.bookingId = bookingId;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getProviderId() {
            return providerId;
        }

        public void setProviderId(Integer providerId) {
            this.providerId = providerId;
        }

        public Integer getCurrentProviderId() {
            return currentProviderId;
        }

        public void setCurrentProviderId(Integer currentProviderId) {
            this.currentProviderId = currentProviderId;
        }

        public Integer getServiceTypeId() {
            return serviceTypeId;
        }

        public void setServiceTypeId(Integer serviceTypeId) {
            this.serviceTypeId = serviceTypeId;
        }

        public Integer getIsMeter() {
            return isMeter;
        }

        public void setIsMeter(Integer isMeter) {
            this.isMeter = isMeter;
        }

        public String getRentalHours() {
            return rentalHours;
        }

        public void setRentalHours(String rentalHours) {
            this.rentalHours = rentalHours;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCancelledBy() {
            return cancelledBy;
        }

        public void setCancelledBy(String cancelledBy) {
            this.cancelledBy = cancelledBy;
        }

        public String getCancelReason() {
            return cancelReason;
        }

        public void setCancelReason(String cancelReason) {
            this.cancelReason = cancelReason;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public Integer getPaid() {
            return paid;
        }

        public void setPaid(Integer paid) {
            this.paid = paid;
        }

        public String getIsTrack() {
            return isTrack;
        }

        public void setIsTrack(String isTrack) {
            this.isTrack = isTrack;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public Integer getTravelTime() {
            return travelTime;
        }

        public void setTravelTime(Integer travelTime) {
            this.travelTime = travelTime;
        }

        public String getsAddress() {
            return sAddress;
        }

        public void setsAddress(String sAddress) {
            this.sAddress = sAddress;
        }

        public Float getsLatitude() {
            return sLatitude;
        }

        public void setsLatitude(Float sLatitude) {
            this.sLatitude = sLatitude;
        }

        public Float getsLongitude() {
            return sLongitude;
        }

        public void setsLongitude(Float sLongitude) {
            this.sLongitude = sLongitude;
        }

        public String getdAddress() {
            return dAddress;
        }

        public void setdAddress(String dAddress) {
            this.dAddress = dAddress;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public Float getdLatitude() {
            return dLatitude;
        }

        public void setdLatitude(Float dLatitude) {
            this.dLatitude = dLatitude;
        }

        public Integer getTrackDistance() {
            return trackDistance;
        }

        public void setTrackDistance(Integer trackDistance) {
            this.trackDistance = trackDistance;
        }

        public Integer getTrackLatitude() {
            return trackLatitude;
        }

        public void setTrackLatitude(Integer trackLatitude) {
            this.trackLatitude = trackLatitude;
        }

        public Integer getTrackLongitude() {
            return trackLongitude;
        }

        public void setTrackLongitude(Integer trackLongitude) {
            this.trackLongitude = trackLongitude;
        }

        public Float getdLongitude() {
            return dLongitude;
        }

        public void setdLongitude(Float dLongitude) {
            this.dLongitude = dLongitude;
        }

        public String getLocationType() {
            return locationType;
        }

        public void setLocationType(String locationType) {
            this.locationType = locationType;
        }

        public String getAssignedAt() {
            return assignedAt;
        }

        public void setAssignedAt(String assignedAt) {
            this.assignedAt = assignedAt;
        }

        public String getScheduleAt() {
            return scheduleAt;
        }

        public void setScheduleAt(String scheduleAt) {
            this.scheduleAt = scheduleAt;
        }

        public String getStartedAt() {
            return startedAt;
        }

        public void setStartedAt(String startedAt) {
            this.startedAt = startedAt;
        }

        public String getFinishedAt() {
            return finishedAt;
        }

        public void setFinishedAt(String finishedAt) {
            this.finishedAt = finishedAt;
        }

        public Integer getUserRated() {
            return userRated;
        }

        public void setUserRated(Integer userRated) {
            this.userRated = userRated;
        }

        public Integer getProviderRated() {
            return providerRated;
        }

        public void setProviderRated(Integer providerRated) {
            this.providerRated = providerRated;
        }

        public Integer getUseWallet() {
            return useWallet;
        }

        public void setUseWallet(Integer useWallet) {
            this.useWallet = useWallet;
        }

        public Integer getSurge() {
            return surge;
        }

        public void setSurge(Integer surge) {
            this.surge = surge;
        }

        public String getRouteKey() {
            return routeKey;
        }

        public void setRouteKey(String routeKey) {
            this.routeKey = routeKey;
        }

        public String getNoteForDriver() {
            return noteForDriver;
        }

        public void setNoteForDriver(String noteForDriver) {
            this.noteForDriver = noteForDriver;
        }

        public String getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(String deletedAt) {
            this.deletedAt = deletedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public ServiceType getService_type() {
            return service_type;
        }

        public void setService_type(ServiceType service_type) {
            this.service_type = service_type;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            if (id == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(id);
            }
            parcel.writeString(bookingId);
            if (userId == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(userId);
            }
            if (providerId == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(providerId);
            }
            if (currentProviderId == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(currentProviderId);
            }
            if (serviceTypeId == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(serviceTypeId);
            }
            if (isMeter == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(isMeter);
            }
            parcel.writeString(rentalHours);
            parcel.writeString(status);
            parcel.writeString(cancelledBy);
            parcel.writeString(cancelReason);
            parcel.writeString(paymentMode);
            if (paid == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(paid);
            }
            parcel.writeString(isTrack);
            parcel.writeString(distance);
            if (travelTime == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(travelTime);
            }
            parcel.writeString(sAddress);
            if (sLatitude == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeFloat(sLatitude);
            }
            if (sLongitude == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeFloat(sLongitude);
            }
            parcel.writeString(dAddress);
            parcel.writeString(otp);
            if (dLatitude == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeFloat(dLatitude);
            }
            if (trackDistance == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(trackDistance);
            }
            if (trackLatitude == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(trackLatitude);
            }
            if (trackLongitude == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(trackLongitude);
            }
            if (dLongitude == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeFloat(dLongitude);
            }
            parcel.writeString(locationType);
            parcel.writeString(assignedAt);
            parcel.writeString(scheduleAt);
            parcel.writeString(startedAt);
            parcel.writeString(finishedAt);
            if (userRated == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(userRated);
            }
            if (providerRated == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(providerRated);
            }
            if (useWallet == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(useWallet);
            }
            if (surge == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(surge);
            }
            parcel.writeString(routeKey);
            parcel.writeString(noteForDriver);
            parcel.writeString(deletedAt);
            parcel.writeString(createdAt);
            parcel.writeString(updatedAt);
            parcel.writeParcelable(user, i);
            parcel.writeParcelable(service_type, i);
            parcel.writeParcelable(provider, i);
            parcel.writeParcelable(provider_service, i);
            parcel.writeParcelable(payment, i);
        }
    }

    public static class User implements Parcelable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("payment_mode")
        @Expose
        private String paymentMode;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("device_id")
        @Expose
        private String deviceId;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("login_by")
        @Expose
        private String loginBy;
        @SerializedName("social_unique_id")
        @Expose
        private String socialUniqueId;
        @SerializedName("latitude")
        @Expose
        private Double latitude;
        @SerializedName("longitude")
        @Expose
        private Double longitude;
        @SerializedName("stripe_cust_id")
        @Expose
        private String stripeCustId;
        @SerializedName("wallet_balance")
        @Expose
        private Integer walletBalance;
        @SerializedName("rating")
        @Expose
        private String rating;
        @SerializedName("otp")
        @Expose
        private Integer otp;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        protected User(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            firstName = in.readString();
            lastName = in.readString();
            paymentMode = in.readString();
            email = in.readString();
            gender = in.readString();
            mobile = in.readString();
            picture = in.readString();
            deviceToken = in.readString();
            deviceId = in.readString();
            deviceType = in.readString();
            loginBy = in.readString();
            socialUniqueId = in.readString();
            if (in.readByte() == 0) {
                latitude = null;
            } else {
                latitude = in.readDouble();
            }
            if (in.readByte() == 0) {
                longitude = null;
            } else {
                longitude = in.readDouble();
            }
            stripeCustId = in.readString();
            if (in.readByte() == 0) {
                walletBalance = null;
            } else {
                walletBalance = in.readInt();
            }
            rating = in.readString();
            if (in.readByte() == 0) {
                otp = null;
            } else {
                otp = in.readInt();
            }
            updatedAt = in.readString();
        }

        public static final Creator<User> CREATOR = new Creator<User>() {
            @Override
            public User createFromParcel(Parcel in) {
                return new User(in);
            }

            @Override
            public User[] newArray(int size) {
                return new User[size];
            }
        };

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getLoginBy() {
            return loginBy;
        }

        public void setLoginBy(String loginBy) {
            this.loginBy = loginBy;
        }

        public String getSocialUniqueId() {
            return socialUniqueId;
        }

        public void setSocialUniqueId(String socialUniqueId) {
            this.socialUniqueId = socialUniqueId;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getStripeCustId() {
            return stripeCustId;
        }

        public void setStripeCustId(String stripeCustId) {
            this.stripeCustId = stripeCustId;
        }

        public Integer getWalletBalance() {
            return walletBalance;
        }

        public void setWalletBalance(Integer walletBalance) {
            this.walletBalance = walletBalance;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public Integer getOtp() {
            return otp;
        }

        public void setOtp(Integer otp) {
            this.otp = otp;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public static Creator<User> getCREATOR() {
            return CREATOR;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            if (id == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(id);
            }
            parcel.writeString(firstName);
            parcel.writeString(lastName);
            parcel.writeString(paymentMode);
            parcel.writeString(email);
            parcel.writeString(gender);
            parcel.writeString(mobile);
            parcel.writeString(picture);
            parcel.writeString(deviceToken);
            parcel.writeString(deviceId);
            parcel.writeString(deviceType);
            parcel.writeString(loginBy);
            parcel.writeString(socialUniqueId);
            if (latitude == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(latitude);
            }
            if (longitude == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(longitude);
            }
            parcel.writeString(stripeCustId);
            if (walletBalance == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(walletBalance);
            }
            parcel.writeString(rating);
            if (otp == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(otp);
            }
            parcel.writeString(updatedAt);
        }
    }

    public static class ServiceType implements Parcelable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("provider_name")
        @Expose
        private String providerName;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("capacity")
        @Expose
        private Integer capacity;
        @SerializedName("fixed")
        @Expose
        private Double fixed;
        @SerializedName("price")
        @Expose
        private Double price;
        @SerializedName("airport_price")
        @Expose
        private Double airportPrice;
        @SerializedName("minute")
        @Expose
        private Integer minute;
        @SerializedName("hour")
        @Expose
        private String hour;
        @SerializedName("distance")
        @Expose
        private String distance;
        @SerializedName("calculator")
        @Expose
        private String calculator;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("status")
        @Expose
        private Integer status;

        protected ServiceType(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            name = in.readString();
            providerName = in.readString();
            image = in.readString();
            if (in.readByte() == 0) {
                capacity = null;
            } else {
                capacity = in.readInt();
            }
            if (in.readByte() == 0) {
                fixed = null;
            } else {
                fixed = in.readDouble();
            }
            if (in.readByte() == 0) {
                price = null;
            } else {
                price = in.readDouble();
            }
            if (in.readByte() == 0) {
                airportPrice = null;
            } else {
                airportPrice = in.readDouble();
            }
            if (in.readByte() == 0) {
                minute = null;
            } else {
                minute = in.readInt();
            }
            hour = in.readString();
            distance = in.readString();
            calculator = in.readString();
            description = in.readString();
            if (in.readByte() == 0) {
                status = null;
            } else {
                status = in.readInt();
            }
        }

        public static final Creator<ServiceType> CREATOR = new Creator<ServiceType>() {
            @Override
            public ServiceType createFromParcel(Parcel in) {
                return new ServiceType(in);
            }

            @Override
            public ServiceType[] newArray(int size) {
                return new ServiceType[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            if (id == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(id);
            }
            parcel.writeString(name);
            parcel.writeString(providerName);
            parcel.writeString(image);
            if (capacity == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(capacity);
            }
            if (fixed == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(fixed);
            }
            if (price == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(price);
            }
            if (airportPrice == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(airportPrice);
            }
            if (minute == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(minute);
            }
            parcel.writeString(hour);
            parcel.writeString(distance);
            parcel.writeString(calculator);
            parcel.writeString(description);
            if (status == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(status);
            }
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProviderName() {
            return providerName;
        }

        public void setProviderName(String providerName) {
            this.providerName = providerName;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Integer getCapacity() {
            return capacity;
        }

        public void setCapacity(Integer capacity) {
            this.capacity = capacity;
        }

        public Double getFixed() {
            return fixed;
        }

        public void setFixed(Double fixed) {
            this.fixed = fixed;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public Double getAirportPrice() {
            return airportPrice;
        }

        public void setAirportPrice(Double airportPrice) {
            this.airportPrice = airportPrice;
        }

        public Integer getMinute() {
            return minute;
        }

        public void setMinute(Integer minute) {
            this.minute = minute;
        }

        public String getHour() {
            return hour;
        }

        public void setHour(String hour) {
            this.hour = hour;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getCalculator() {
            return calculator;
        }

        public void setCalculator(String calculator) {
            this.calculator = calculator;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public static Creator<ServiceType> getCREATOR() {
            return CREATOR;
        }
    }

    public static class Provider implements Parcelable {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("first_name")
        @Expose
        public String firstName;
        @SerializedName("last_name")
        @Expose
        public String lastName;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("gender")
        @Expose
        public String gender;
        @SerializedName("mobile")
        @Expose
        public String mobile;
        @SerializedName("dob")
        @Expose
        public String dob;
        @SerializedName("address")
        @Expose
        public String address;
        @SerializedName("badge_id")
        @Expose
        public Integer badgeId;
        @SerializedName("bill")
        @Expose
        public String bill;
        @SerializedName("stripe_cust_id")
        @Expose
        public String stripeCustId;
        @SerializedName("avatar")
        @Expose
        public String avatar;
        @SerializedName("rating")
        @Expose
        public String rating;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("fleet")
        @Expose
        public Integer fleet;
        @SerializedName("latitude")
        @Expose
        public Float latitude;
        @SerializedName("longitude")
        @Expose
        public Float longitude;
        @SerializedName("otp")
        @Expose
        public Integer otp;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;
        @SerializedName("login_by")
        @Expose
        public String loginBy;
        @SerializedName("social_unique_id")
        @Expose
        public Object socialUniqueId;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Integer getBadgeId() {
            return badgeId;
        }

        public void setBadgeId(Integer badgeId) {
            this.badgeId = badgeId;
        }

        public String getBill() {
            return bill;
        }

        public void setBill(String bill) {
            this.bill = bill;
        }

        public String getStripeCustId() {
            return stripeCustId;
        }

        public void setStripeCustId(String stripeCustId) {
            this.stripeCustId = stripeCustId;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Integer getFleet() {
            return fleet;
        }

        public void setFleet(Integer fleet) {
            this.fleet = fleet;
        }

        public Float getLatitude() {
            return latitude;
        }

        public void setLatitude(Float latitude) {
            this.latitude = latitude;
        }

        public Float getLongitude() {
            return longitude;
        }

        public void setLongitude(Float longitude) {
            this.longitude = longitude;
        }

        public Integer getOtp() {
            return otp;
        }

        public void setOtp(Integer otp) {
            this.otp = otp;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getLoginBy() {
            return loginBy;
        }

        public void setLoginBy(String loginBy) {
            this.loginBy = loginBy;
        }

        public Object getSocialUniqueId() {
            return socialUniqueId;
        }

        public void setSocialUniqueId(Object socialUniqueId) {
            this.socialUniqueId = socialUniqueId;
        }

        protected Provider(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            firstName = in.readString();
            lastName = in.readString();
            email = in.readString();
            gender = in.readString();
            mobile = in.readString();
            dob = in.readString();
            address = in.readString();
            if (in.readByte() == 0) {
                badgeId = null;
            } else {
                badgeId = in.readInt();
            }
            bill = in.readString();
            stripeCustId = in.readString();
            avatar = in.readString();
            rating = in.readString();
            status = in.readString();
            if (in.readByte() == 0) {
                fleet = null;
            } else {
                fleet = in.readInt();
            }
            if (in.readByte() == 0) {
                latitude = null;
            } else {
                latitude = in.readFloat();
            }
            if (in.readByte() == 0) {
                longitude = null;
            } else {
                longitude = in.readFloat();
            }
            if (in.readByte() == 0) {
                otp = null;
            } else {
                otp = in.readInt();
            }
            createdAt = in.readString();
            updatedAt = in.readString();
            loginBy = in.readString();
        }

        public static final Creator<Provider> CREATOR = new Creator<Provider>() {
            @Override
            public Provider createFromParcel(Parcel in) {
                return new Provider(in);
            }

            @Override
            public Provider[] newArray(int size) {
                return new Provider[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            if (id == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(id);
            }
            parcel.writeString(firstName);
            parcel.writeString(lastName);
            parcel.writeString(email);
            parcel.writeString(gender);
            parcel.writeString(mobile);
            parcel.writeString(dob);
            parcel.writeString(address);
            if (badgeId == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(badgeId);
            }
            parcel.writeString(bill);
            parcel.writeString(stripeCustId);
            parcel.writeString(avatar);
            parcel.writeString(rating);
            parcel.writeString(status);
            if (fleet == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(fleet);
            }
            if (latitude == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeFloat(latitude);
            }
            if (longitude == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeFloat(longitude);
            }
            if (otp == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(otp);
            }
            parcel.writeString(createdAt);
            parcel.writeString(updatedAt);
            parcel.writeString(loginBy);
        }
    }
    public static class ProviderService implements Parcelable {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("provider_id")
        @Expose
        public Integer providerId;
        @SerializedName("service_type_id")
        @Expose
        public Integer serviceTypeId;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("service_number")
        @Expose
        public String serviceNumber;
        @SerializedName("start_date")
        @Expose
        public String startDate;
        @SerializedName("register_no")
        @Expose
        public String registerNo;
        @SerializedName("service_model")
        @Expose
        public Object serviceModel;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getProviderId() {
            return providerId;
        }

        public void setProviderId(Integer providerId) {
            this.providerId = providerId;
        }

        public Integer getServiceTypeId() {
            return serviceTypeId;
        }

        public void setServiceTypeId(Integer serviceTypeId) {
            this.serviceTypeId = serviceTypeId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getServiceNumber() {
            return serviceNumber;
        }

        public void setServiceNumber(String serviceNumber) {
            this.serviceNumber = serviceNumber;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getRegisterNo() {
            return registerNo;
        }

        public void setRegisterNo(String registerNo) {
            this.registerNo = registerNo;
        }

        public Object getServiceModel() {
            return serviceModel;
        }

        public void setServiceModel(Object serviceModel) {
            this.serviceModel = serviceModel;
        }

        protected ProviderService(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            if (in.readByte() == 0) {
                providerId = null;
            } else {
                providerId = in.readInt();
            }
            if (in.readByte() == 0) {
                serviceTypeId = null;
            } else {
                serviceTypeId = in.readInt();
            }
            status = in.readString();
            serviceNumber = in.readString();
            startDate = in.readString();
            registerNo = in.readString();
        }

        public static final Creator<ProviderService> CREATOR = new Creator<ProviderService>() {
            @Override
            public ProviderService createFromParcel(Parcel in) {
                return new ProviderService(in);
            }

            @Override
            public ProviderService[] newArray(int size) {
                return new ProviderService[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            if (id == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(id);
            }
            if (providerId == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(providerId);
            }
            if (serviceTypeId == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(serviceTypeId);
            }
            parcel.writeString(status);
            parcel.writeString(serviceNumber);
            parcel.writeString(startDate);
            parcel.writeString(registerNo);
        }
    }

    public static class Payment implements Parcelable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("request_id")
        @Expose
        private Integer requestId;
        @SerializedName("promocode_id")
        @Expose
        private Object promocodeId;
        @SerializedName("payment_id")
        @Expose
        private Object paymentId;
        @SerializedName("payment_mode")
        @Expose
        private Object paymentMode;
        @SerializedName("fixed")
        @Expose
        private Double fixed;
        @SerializedName("distance")
        @Expose
        private Integer distance;
        @SerializedName("commision")
        @Expose
        private Double commision;
        @SerializedName("discount")
        @Expose
        private Double discount;
        @SerializedName("tax")
        @Expose
        private Double tax;
        @SerializedName("wallet")
        @Expose
        private Double wallet;
        @SerializedName("surge")
        @Expose
        private Double surge;
        @SerializedName("total")
        @Expose
        private Double total;
        @SerializedName("payable")
        @Expose
        private Double payable;
        @SerializedName("provider_commission")
        @Expose
        private Double providerCommission;
        @SerializedName("provider_pay")
        @Expose
        private Double providerPay;
        @SerializedName("tip")
        @Expose
        private Double tip;

        protected Payment(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            if (in.readByte() == 0) {
                requestId = null;
            } else {
                requestId = in.readInt();
            }
            if (in.readByte() == 0) {
                fixed = null;
            } else {
                fixed = in.readDouble();
            }
            if (in.readByte() == 0) {
                distance = null;
            } else {
                distance = in.readInt();
            }
            if (in.readByte() == 0) {
                commision = null;
            } else {
                commision = in.readDouble();
            }
            if (in.readByte() == 0) {
                discount = null;
            } else {
                discount = in.readDouble();
            }
            if (in.readByte() == 0) {
                tax = null;
            } else {
                tax = in.readDouble();
            }
            if (in.readByte() == 0) {
                wallet = null;
            } else {
                wallet = in.readDouble();
            }
            if (in.readByte() == 0) {
                surge = null;
            } else {
                surge = in.readDouble();
            }
            if (in.readByte() == 0) {
                total = null;
            } else {
                total = in.readDouble();
            }
            if (in.readByte() == 0) {
                payable = null;
            } else {
                payable = in.readDouble();
            }
            if (in.readByte() == 0) {
                providerCommission = null;
            } else {
                providerCommission = in.readDouble();
            }
            if (in.readByte() == 0) {
                providerPay = null;
            } else {
                providerPay = in.readDouble();
            }
            if (in.readByte() == 0) {
                tip = null;
            } else {
                tip = in.readDouble();
            }
        }

        public static final Creator<Payment> CREATOR = new Creator<Payment>() {
            @Override
            public Payment createFromParcel(Parcel in) {
                return new Payment(in);
            }

            @Override
            public Payment[] newArray(int size) {
                return new Payment[size];
            }
        };

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getRequestId() {
            return requestId;
        }

        public void setRequestId(Integer requestId) {
            this.requestId = requestId;
        }

        public Object getPromocodeId() {
            return promocodeId;
        }

        public void setPromocodeId(Object promocodeId) {
            this.promocodeId = promocodeId;
        }

        public Object getPaymentId() {
            return paymentId;
        }

        public void setPaymentId(Object paymentId) {
            this.paymentId = paymentId;
        }

        public Object getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(Object paymentMode) {
            this.paymentMode = paymentMode;
        }

        public Double getFixed() {
            return fixed;
        }

        public void setFixed(Double fixed) {
            this.fixed = fixed;
        }

        public Integer getDistance() {
            return distance;
        }

        public void setDistance(Integer distance) {
            this.distance = distance;
        }

        public Double getCommision() {
            return commision;
        }

        public void setCommision(Double commision) {
            this.commision = commision;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

        public Double getWallet() {
            return wallet;
        }

        public void setWallet(Double wallet) {
            this.wallet = wallet;
        }

        public Double getSurge() {
            return surge;
        }

        public void setSurge(Double surge) {
            this.surge = surge;
        }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        public Double getPayable() {
            return payable;
        }

        public void setPayable(Double payable) {
            this.payable = payable;
        }

        public Double getProviderCommission() {
            return providerCommission;
        }

        public void setProviderCommission(Double providerCommission) {
            this.providerCommission = providerCommission;
        }

        public Double getProviderPay() {
            return providerPay;
        }

        public void setProviderPay(Double providerPay) {
            this.providerPay = providerPay;
        }

        public Double getTip() {
            return tip;
        }

        public void setTip(Double tip) {
            this.tip = tip;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            if (id == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(id);
            }
            if (requestId == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(requestId);
            }
            if (fixed == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(fixed);
            }
            if (distance == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(distance);
            }
            if (commision == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(commision);
            }
            if (discount == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(discount);
            }
            if (tax == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(tax);
            }
            if (wallet == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(wallet);
            }
            if (surge == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(surge);
            }
            if (total == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(total);
            }
            if (payable == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(payable);
            }
            if (providerCommission == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(providerCommission);
            }
            if (providerPay == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(providerPay);
            }
            if (tip == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeDouble(tip);
            }
        }
    }
}
