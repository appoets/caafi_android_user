package com.caafisom.android.user.model;


import com.caafisom.android.user.model.dto.response.ForgotPasswordResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class ForgotPasswordModel extends BaseModel<ForgotPasswordResponse> {

    public ForgotPasswordModel(IModelListener<ForgotPasswordResponse> listener) {
        super(listener);
    }

    public void getOTPDetails(String email) {
       enQueueTask(new ApiClient().getClient().create(ApiInterface.class).forgotPassword(email));
    }

    @Override
    public void onSuccessfulApi(ForgotPasswordResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<ForgotPasswordResponse> response) {

    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
