package com.caafisom.android.user.presenter;

import android.os.Bundle;

import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.ProfileModel;
import com.caafisom.android.user.model.ProfileUpdateModel;
import com.caafisom.android.user.model.dto.response.ProfileResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.IProfilePresenter;
import com.caafisom.android.user.view.iview.IProfileView;


import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;


public class ProfilePresenter extends BasePresenter<IProfileView> implements IProfilePresenter {

    public ProfilePresenter(IProfileView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getUserDetails();
    }

    private void getUserDetails() {
        iView.showProgressbar();
        new ProfileModel(new IModelListener<ProfileResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProfileResponse response) {
                iView.dismissProgressbar();
                getApplicationInstance().setCurrency(response.getCurrency());
                iView.updateUserDetails(response);
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProfileResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getUserDetails();
    }




    @Override
    public void updateProfile(HashMap<String, RequestBody> params, MultipartBody.Part filePart) {
        iView.showProgressbar();
        new ProfileUpdateModel(new IModelListener<ProfileResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProfileResponse response) {
                iView.dismissProgressbar();
                getUserDetails();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProfileResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).updateProfile(params,filePart);
    }
}
