package com.caafisom.android.user.view.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.caafisom.android.user.R;
import com.caafisom.android.user.presenter.DocotorSpecialityListPresenter;
import com.caafisom.android.user.presenter.DoctorBookingPresenter;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.iview.IDoctorBookingView;
import com.caafisom.android.user.view.iview.IDoctorSpecialityListView;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class DoctorBookingActivity extends BaseActivity<DoctorBookingPresenter> implements IDoctorBookingView {

    @BindView(R.id.dateLayout)
    LinearLayout dateLayout;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvSpl)
    TextView tvSpl;
    @BindView(R.id.civProfile)
    CircleImageView civProfile;
    @BindView(R.id.tv_selectDate)
    TextView tvselectDate;
    @BindView(R.id.scheduleDate)
    TextView scheduleDate;
    @BindView(R.id.btn_zaad)
    TextView btnzaad;
    @BindView(R.id.btn_edahab)
    TextView btnedahab;
    @BindView(R.id.ibBack)
    ImageView ibBack;

    @BindView(R.id.ll_date)
    LinearLayout lldate;
    @BindView(R.id.ll_after_booking)
    LinearLayout llafterbooking;
    @BindView(R.id.selected_date)
    TextView selected_date;
    @BindView(R.id.selected_fee)
    TextView selected_fee;

    Integer doctor_id, doctor_spl_id, doctor_consultationFee,hospital_id;
    String doctor_firstname, doctor_lastname, doctor_image, doctor_spl, selectedDate;

    @Override
    int attachLayout() {
        return R.layout.activity_doctor_booing;
    }

    @Override
    DoctorBookingPresenter initialize() {
        return new DoctorBookingPresenter(this);
    }

    @Override
    public void initSetUp() {
        Intent intent = getIntent();
        if (intent != null) {
            doctor_id = intent.getIntExtra("doctor_id", 0);
            doctor_firstname = intent.getStringExtra("doctor_firstname");
            doctor_lastname = intent.getStringExtra("doctor_lastname");
            doctor_image = intent.getStringExtra("doctor_image");
            doctor_spl = intent.getStringExtra("doctor_spl");
            doctor_spl_id = intent.getIntExtra("doctor_spl_id", 0);
            doctor_consultationFee = intent.getIntExtra("doctor_consultationFee", 0);
            hospital_id = intent.getIntExtra("hospital_id", 0);
        }

        tvName.setText(doctor_firstname + " " + doctor_lastname);
        tvSpl.setText(doctor_spl);
        CodeSnippet.loadImageCircle(String.valueOf(doctor_image), civProfile, R.drawable.ic_dummy_user);

    }

    @Override
    public void startCheckFailStatus() {
        Toast.makeText(this, "Transaction Failed!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(DoctorBookingActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick({R.id.dateLayout, R.id.btn_zaad, R.id.btn_edahab,R.id.ibBack})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.dateLayout:
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                // set day of month , month and year value in the edit text
                                String choosedMonth = "";
                                String choosedDate = "";

                                if (dayOfMonth < 10) {
                                    choosedDate = "0" + dayOfMonth;
                                } else {
                                    choosedDate = "" + dayOfMonth;
                                }

                                if ((monthOfYear + 1) < 10) {
                                    choosedMonth = "0" + (monthOfYear + 1);
                                } else {
                                    choosedMonth = "" + (monthOfYear + 1);
                                }
                                selectedDate = choosedDate + "-" + choosedMonth + "-" + year;
                                tvselectDate.setVisibility(View.GONE);
                                scheduleDate.setVisibility(View.VISIBLE);
                                if (isAfterToday(year, (monthOfYear + 1), dayOfMonth)) {
                                    scheduleDate.setText(choosedDate + "-" + choosedMonth + "-" + year);

                                    lldate.setVisibility(View.GONE);
                                    llafterbooking.setVisibility(View.VISIBLE);
                                    selected_date.setText(selectedDate);
                                    selected_fee.setText(" " + doctor_consultationFee);

                                } else {
                                    //scheduleDate.setText("");
                                    showToast("Select Date After Today's Date");
                                }
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.getDatePicker().setMaxDate((System.currentTimeMillis() - 1000) + (1000 * 60 * 60 * 24 * 7));
                datePickerDialog.show();
                break;

            case R.id.btn_zaad:
                iPresenter.doctorAppointment(doctor_id, scheduleDate.getText().toString(), "ZAAD",hospital_id);
                break;

            case R.id.btn_edahab:
                iPresenter.doctorAppointment(doctor_id, scheduleDate.getText().toString(), "EDAHAB",hospital_id);
                break;
            case R.id.ibBack:
                onBackPressed();
                break;

        }
    }

    public static boolean isAfterToday(int year, int month, int day) {
        Calendar today = Calendar.getInstance();
        Calendar myDate = Calendar.getInstance();

        myDate.set(year, month, day);
        return !myDate.before(today);
    }

    @Override
    public void startCheckStatus() {
        Toast.makeText(this, "New request Created!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(DoctorBookingActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();

    }


}