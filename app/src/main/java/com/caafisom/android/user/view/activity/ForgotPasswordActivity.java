package com.caafisom.android.user.view.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.caafisom.android.user.R;
import com.caafisom.android.user.presenter.ForgotPasswordPresenter;
import com.caafisom.android.user.presenter.ipresenter.IForgotPasswordPresenter;
import com.caafisom.android.user.view.iview.IForgotPasswordView;

import butterknife.BindView;
import butterknife.OnClick;

public class ForgotPasswordActivity extends BaseActivity<IForgotPasswordPresenter> implements IForgotPasswordView {

    @BindView(R.id.btnConfirm)
    Button btnConfirm;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @Override
    protected int attachLayout() {
        return R.layout.activity_forget_password;
    }

    @Override
    IForgotPasswordPresenter initialize() {
        return new ForgotPasswordPresenter(this);
    }

    @OnClick({R.id.btnConfirm})
    public void OnClickView(View view){
        switch (view.getId()){

            case R.id.btnConfirm:
                validateForgotPassword();
                break;

        }
    }

    private void validateForgotPassword() {
        String email = etEmail.getText().toString().trim();
        if(TextUtils.isEmpty(email)){
            showSnackBar(getString(R.string.please_enter_email));
        }else if (!getCodeSnippet().isEmailValid(email)){
            showSnackBar(getString(R.string.please_enter_valid_email));
        }else {
            iPresenter.getOTPDetails(email);
        }
    }

    @Override
    public void goToOneTimePassword() {
        startActivity(new Intent(this,OneTimePasswordActivity.class));
    }
}
