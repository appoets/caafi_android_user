package com.caafisom.android.user.view.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.presenter.ipresenter.IPresenter;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.util.PermissionUtils;
import com.caafisom.android.user.view.activity.BaseActivity;
import com.caafisom.android.user.view.iview.IView;
import com.caafisom.android.user.view.widget.CustomProgressbar;

import java.util.Objects;

import butterknife.ButterKnife;
import timber.log.Timber;


public abstract class BaseFragment<P extends IPresenter> extends Fragment implements IView<P> {

    protected PermissionUtils mPermission;
    protected P iPresenter;
    private String arrowBase = " <---------- ";
    private View mParentView;
    private  CustomProgressbar mCustomProgressbar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        Timber.tag("On Create").v("On Create ---------->  %s", arrowBase);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mParentView = Objects.requireNonNull(getActivity()).getWindow().getDecorView().findViewById(android.R.id.content);
        View mRootView = inflater.inflate(getLayoutID(), container, false);
        ButterKnife.bind(this, mRootView);
        iPresenter = initialize();
        iPresenter.onCreate(getArguments());
        setUp();
        return mRootView;
    }


    protected void setUp() {

    }

    protected abstract int getLayoutID();

    protected abstract P initialize();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroy() {
        Timber.tag("On Destroy").v("On Destroy ---------->  %s", arrowBase);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.tag("On onResume").v("On onResume ---------->  %s", arrowBase);
    }

    @Override
    public void onStart() {
        super.onStart();
        Timber.tag("On onStart").v("On onStart ---------->  %s", arrowBase);
    }

    @Override
    public void onStop() {
        super.onStop();
        Timber.tag("On onStop").v("On onStop ---------->  %s", arrowBase);
    }

    @Override
    public void onPause() {
        super.onPause();
        Timber.tag("On onPause").v("On onPause ---------->  %s", arrowBase);
    }


    private CustomProgressbar getProgressBar() {
        if (mCustomProgressbar == null)
            mCustomProgressbar = new CustomProgressbar(getContext());
        return mCustomProgressbar;
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(int resId) {
        Toast.makeText(getActivity(), getString(resId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(CustomException e) {
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAlertDialog(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getText(android.R.string.ok), (dialog, which) -> dialog.cancel());
        alertDialog.show();
    }

    @Override
    public void showAlertDialog(int resId) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(getString(resId));
        alertDialog.setPositiveButton(getText(android.R.string.ok), (dialog, which) -> dialog.cancel());
        alertDialog.show();
    }

    @Override
    public void showAlertDialog(String message, BaseActivity.whenClicked clicked) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getText(android.R.string.ok), (dialog, which) -> {
            dialog.cancel();
            clicked.clickEvent();
        });
        alertDialog.show();
    }

    public interface DialogClick {
        void positiveButtonClick();
        void negativeButtonClick();
    }

    @Override
    public void showAlertDialogWithNegativeButton(String message, BaseActivity.DialogClick listener) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getText(android.R.string.ok), (dialog, which) -> {
            dialog.cancel();
            listener.positiveButtonClick();
        });
        alertDialog.setNegativeButton(getText(android.R.string.cancel),((dialog, which) -> {
            dialog.cancel();
            listener.negativeButtonClick();
        }));
        alertDialog.show();
    }

    @Override
    public void showProgressbar() {
        try {
            getProgressBar().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismissProgressbar() {
        Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
            try {
                getProgressBar().dismissProgress();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void showSnackBar(String message) {
        if (mParentView != null) {
            Snackbar snackbar = Snackbar.make(mParentView, message, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    public void showSnackBar(@NonNull View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        snackbar.show();
    }

    @Override
    public void showSnackBarWithDelayExit(String message,long delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        },delay);
        if (mParentView != null) {
            Snackbar snackbar = Snackbar.make(mParentView, message, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    public void showSnackBarWithDelayExit(String message) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getFragmentManager().popBackStack();
            }
        },1500);
        if (mParentView != null) {
            Snackbar snackbar = Snackbar.make(mParentView, message, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    public void showNetworkMessage() {
        if (mParentView != null) {
            Snackbar snackbar = Snackbar.make(mParentView, Constants.HttpErrorMessage.NO_NETWORK_FOUND, Snackbar.LENGTH_INDEFINITE);
            snackbar.setActionTextColor(Color.RED);
            snackbar.setAction(Constants.HttpErrorMessage.SETTINGS, view -> getCodeSnippet().showNetworkSettings());
            snackbar.show();
        }
    }

    @Override
    public CodeSnippet getCodeSnippet() {
       /* if (mCodeSnippet == null) {
            mCodeSnippet = new CodeSnippet(getApplicationContext());
            return mCodeSnippet;
        }
        return mCodeSnippet;*/
        return new CodeSnippet();
    }

    @Override
    public boolean isNetworkEnabled() {
        ConnectivityManager connectivityManager = (ConnectivityManager) Objects.requireNonNull(getActivity()).getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null) return info.getState() == NetworkInfo.State.CONNECTED;
        }
        return false;
    }

    @Override
    public void makeLogout() {
        //iPresenter.onLogout();
    }

    @Override
    public void navigateTo(Class<?> cls, boolean isFinishActivity, Bundle bundle) {
        startActivity(new Intent(getActivity(), cls).putExtras(bundle));
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);      /*      anim in     */
        if (isFinishActivity) getActivity().finish();
    }


    @Override
    public void changeFragment(Bundle bundle,
                               @IdRes int containerViewId,
                               @NonNull Fragment fragment,
                               @NonNull String fragmentTag) {
        fragment.setArguments(bundle);
        Objects.requireNonNull(getActivity()).getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(this.getClass().getSimpleName())
                .commit();
    }

    @Override
    public void addFragment(Bundle bundle,
                            @IdRes int containerViewId,
                            @NonNull Fragment fragment,
                            @NonNull String fragmentTag) {
        fragment.setArguments(bundle);
        Objects.requireNonNull(getActivity()).getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(this.getClass().getSimpleName())
                .commit();
    }

    @Override
    public void showTextInputLayoutError(TextInputLayout til, EditText et, String msg) {
        til.setError(msg);
        if (et.requestFocus())
            Objects.requireNonNull(getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    /*@Override
    public void finish() {
        super.finish();
        System.out.println("BaseActivity.finish" + TAG);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);     *//*      anim out    *//*
    }*/

    @Override
    public void onActivityForResult(int requestCode, int resultCode, Intent data) {

    }


    @Override
    public void onLogout(CustomException e) {

    }

    @Override
    public void onLogout() {

    }


    @Override
    public boolean checkLocationStatus() {
        final LocationManager manager = (LocationManager) Objects.requireNonNull(getActivity()).getSystemService(Context.LOCATION_SERVICE);
        assert manager != null;
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    @Override
    public PermissionUtils getPermissionUtils() {
        if (mPermission == null) {
            mPermission = new PermissionUtils();
            return mPermission;
        }
        return mPermission;
    }

    @Override
    public String getStringRes(int resId) {
        return getString(resId);
    }

    @Override
    public String getTextRes(int res) {
        return getString(res);
    }
}
