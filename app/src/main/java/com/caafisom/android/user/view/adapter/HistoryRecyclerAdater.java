package com.caafisom.android.user.view.adapter;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.common.History;
import com.caafisom.android.user.model.dto.response.HistoryResponse;
import com.caafisom.android.user.view.adapter.listener.IHistoryRecyclerAdapter;
import com.caafisom.android.user.view.adapter.viewholder.HistoryViewHolder;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class HistoryRecyclerAdater extends BaseRecyclerAdapter<IHistoryRecyclerAdapter, HistoryResponse,HistoryViewHolder> {

    List<HistoryResponse> historyList;
    IHistoryRecyclerAdapter iHistoryRecyclerAdapter;

    public HistoryRecyclerAdater(List<HistoryResponse> historyList, IHistoryRecyclerAdapter iHistoryRecyclerAdapter) {
        super(historyList, iHistoryRecyclerAdapter);
        this.historyList = historyList;
        this.iHistoryRecyclerAdapter = iHistoryRecyclerAdapter;

    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HistoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.history_list_item,parent,false),iHistoryRecyclerAdapter);
    }
}
