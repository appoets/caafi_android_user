package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.presenter.ipresenter.ITwilloVideoPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface ITwilloVideoView extends IView<ITwilloVideoPresenter> {
    void twiloVideoToken(Object token);
}
