package com.caafisom.android.user.presenter.ipresenter;

import com.caafisom.android.user.model.dto.request.LoginRequest;

public interface ILoginPresenter extends IPresenter {
    void postLogin(LoginRequest request);
    void goToRegistration();
    void goToForgotPassword();
}