package com.caafisom.android.user.presenter;

import android.os.Bundle;

import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.NotificationModel;
import com.caafisom.android.user.model.dto.response.NotificationResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.INotificationPresenter;
import com.caafisom.android.user.view.adapter.NotificationRecyclerAdapter;
import com.caafisom.android.user.view.adapter.listener.INotificationRecyclerAdapter;
import com.caafisom.android.user.view.iview.INotificationView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class NotificationPresenter extends BasePresenter<INotificationView> implements INotificationPresenter {

    public NotificationPresenter(INotificationView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
    }

    INotificationRecyclerAdapter iNotificationRecyclerAdapter = new INotificationRecyclerAdapter() {
        @Override
        public void onClickItem(int pos, NotificationResponse data) {

        }
    };

    @Override
    public void getNotification() {
        iView.showProgressbar();
        new NotificationModel(new IModelListener<NotificationResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<NotificationResponse> response) {
                iView.dismissProgressbar();
                iView.setAdapter(new NotificationRecyclerAdapter(response,iNotificationRecyclerAdapter));
            }

            @Override
            public void onSuccessfulApi(@NotNull NotificationResponse response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getNotificationDetail();
    }
}
