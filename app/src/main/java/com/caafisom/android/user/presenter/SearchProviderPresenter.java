package com.caafisom.android.user.presenter;

import android.os.Bundle;

import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.ProviderModel;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.response.ProviderResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.IHospitalListPresenter;
import com.caafisom.android.user.view.adapter.ProviderRecyclerAdapter;
import com.caafisom.android.user.view.adapter.listener.IProviderRecyclerAdapter;
import com.caafisom.android.user.view.iview.IHospitalistView;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class SearchProviderPresenter extends BasePresenter<IHospitalistView> implements IHospitalListPresenter {


    public SearchProviderPresenter(IHospitalistView iView) {
        super(iView);

    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();

    }

    private IProviderRecyclerAdapter iProviderRecyclerAdapter = new IProviderRecyclerAdapter() {

        @Override
        public void onClickItem(int pos, Provider data) {
            iView.moveToDoctoBook(data);
        }
    };

    @Override
    public void getHosptialsList(String searchKey) {
        iView.showProgressbar();
        new ProviderModel(new IModelListener<ProviderResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProviderResponse response) {
                iView.dismissProgressbar();
                iView.setProviderAdapter(new ProviderRecyclerAdapter(response.getProvider(), iProviderRecyclerAdapter));
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProviderResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).searchProviderByService(searchKey);
    }



}
