package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.model.dto.response.HelpResponse;
import com.caafisom.android.user.presenter.ipresenter.IHelpPresenter;

public interface IHelpView extends IView<IHelpPresenter> {
        void updateHelpDetails(HelpResponse response);
}
