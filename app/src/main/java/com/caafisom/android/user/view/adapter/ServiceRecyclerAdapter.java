package com.caafisom.android.user.view.adapter;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.common.Services;
import com.caafisom.android.user.view.adapter.listener.IServiceRecyclerAdapter;
import com.caafisom.android.user.view.adapter.viewholder.ServiceViewHolder;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class ServiceRecyclerAdapter extends BaseRecyclerAdapter<IServiceRecyclerAdapter,Services,ServiceViewHolder> {

    private IServiceRecyclerAdapter iServiceRecyclerAdapter;

    public ServiceRecyclerAdapter(List<Services> data, IServiceRecyclerAdapter iServiceRecyclerAdapter) {
        super(data, iServiceRecyclerAdapter);
        this.iServiceRecyclerAdapter = iServiceRecyclerAdapter;
    }

    @NonNull
    @Override
    public ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ServiceViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.specialist_list_item,parent,false),iServiceRecyclerAdapter);
    }

}
