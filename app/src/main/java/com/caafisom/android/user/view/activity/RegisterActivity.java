package com.caafisom.android.user.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.countrypicker.Country;
import com.caafisom.android.user.countrypicker.CountryPicker;
import com.caafisom.android.user.countrypicker.CountryPickerListener;
import com.caafisom.android.user.model.dto.request.RegisterRequest;
import com.caafisom.android.user.presenter.RegisterPresenter;
import com.caafisom.android.user.presenter.ipresenter.IRegisterPresenter;
import com.caafisom.android.user.view.iview.IRegisterView;
import com.cooltechworks.creditcarddesign.CardEditActivity;
import com.cooltechworks.creditcarddesign.CreditCardUtils;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity<IRegisterPresenter> implements IRegisterView {

    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etLastName)
    EditText etLastName;
    @BindView(R.id.etMobileNumber)
    EditText etMobileNumber;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.etCountryCode)
    EditText etCountryCode;
    @BindView(R.id.ll_city)
    LinearLayout llcity;
    @BindView(R.id.etCity)
    EditText etCity;

    private int selectedCityID = 0;
    private String selectedCityName;

    private CountryPicker mCountryPicker;
    final int GET_CITY = 420;

    @Override
    protected int attachLayout() {
        return R.layout.activity_register;
    }

    @Override
    IRegisterPresenter initialize() {

        mCountryPicker = CountryPicker.newInstance("Select Country");

        // You can limit the displayed countries
        ArrayList<Country> nc = new ArrayList<>();
        for (Country c : Country.getAllCountries()) {
            nc.add(c);
        }
        // and decide, in which order they will be displayed
        Collections.reverse(nc);
        mCountryPicker.setCountriesList(nc);
        setListener();
        return new RegisterPresenter(this);
    }

    private void setListener() {
        mCountryPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode,
                                        int flagDrawableResID) {
                etCountryCode.setText(dialCode);
                mCountryPicker.dismiss();
            }
        });

        getUserCountryInfo();
    }

    private void getUserCountryInfo() {
        Country country = Country.getCountryFromSIM(getApplicationContext());
        if (country != null) {
            etCountryCode.setText(country.getDialCode());
        } /*else {
            Toast.makeText(this, "Required Sim", Toast.LENGTH_SHORT).show();
        }*/
    }


    @OnClick({R.id.btnSignUp, R.id.tvSignIN, R.id.etCountryCode, R.id.etCity})
    public void OnViewClick(View view) {

        switch (view.getId()) {
            case R.id.btnSignUp:
                validateSignUp();
                break;

            case R.id.tvSignIN:
                iPresenter.goToLogin();
                break;
            case R.id.etCity:
                Intent intent = new Intent(getActivity(), CityListActivity.class);
                startActivityForResult(intent, GET_CITY);
                break;
            case R.id.etCountryCode:
                mCountryPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;

        }

    }

    private void validateSignUp() {
        String email = etEmail.getText().toString().trim();
        String first_name = etName.getText().toString().trim();
        String last_name = etLastName.getText().toString().trim();
        String mobile_number = etMobileNumber.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            showSnackBar(getString(R.string.please_enter_email));
        } else if (!getCodeSnippet().isEmailValid(email)) {
            showSnackBar(getString(R.string.please_enter_valid_email));
        } else if (first_name.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_name));
        } else if (mobile_number.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_mobile_number));
        } else if (etCountryCode.getText().toString().isEmpty()) {
            showSnackBar(getString(R.string.please_enter_dial_code));
        } else if (password.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_password));
        } else if (password.length() < 6) {
            showSnackBar(getString(R.string.invalid_password));
        } else if (etCity.getText().toString().isEmpty()) {
            showSnackBar(getString(R.string.please_enter_city));
        }
        else {
            RegisterRequest registerRequest = new RegisterRequest();
            registerRequest.setFirst_name(first_name);
            registerRequest.setLast_name(last_name);
            registerRequest.setEmail(email);
            registerRequest.setMobile(etCountryCode.getText().toString() + mobile_number);
            registerRequest.setPassword(password);
            registerRequest.setCity_id(String.valueOf(selectedCityID));
            registerRequest.setPassword_confirmation(password);
            registerRequest.setDevice_id(Constants.WebConstants.DEVICE_ID);
            registerRequest.setDevice_token(Constants.WebConstants.DEVICE_TOKEN);
            registerRequest.setDevice_type(Constants.WebConstants.DEVICE_TYPE);
            registerRequest.setLogin_by(Constants.WebConstants.LOGIN_BY);
            iPresenter.postRegister(registerRequest);
        }
    }

    @Override
    public void goToLogin() {
        navigateTo(LoginActivity.class, false, new Bundle());
        finishAffinity();
    }

    @Override
    public void goToHome() {
        navigateTo(HomeActivity.class, true, new Bundle());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == GET_CITY) {
            selectedCityID = data.getIntExtra("selectedCityID",0);
            selectedCityName = data.getStringExtra("selectedCityName");
            //Toast.makeText(this, "selected City " + selectedCityID + " " + selectedCityName, Toast.LENGTH_SHORT).show();
            etCity.setText(selectedCityName);
        }

    }
}
