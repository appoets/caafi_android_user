package com.caafisom.android.user.view.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.NotificationResponse;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.adapter.listener.INotificationRecyclerAdapter;

import butterknife.BindView;

/**
 * Created by Tranxit Technologies.
 */

public class NotificationViewHolder extends BaseViewHolder<NotificationResponse,INotificationRecyclerAdapter> {

    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvCallStatus)
    TextView tvCallStatus;
    @BindView(R.id.tvMissedCallStatus)
    TextView tvMissedCallStatus;


    public NotificationViewHolder(View itemView, INotificationRecyclerAdapter listener) {
        super(itemView, listener);
    }

    @Override
    void populateData(NotificationResponse data) {

        String date = data.getScheduleAt();
        String dateNotification = data.getScheduleAt();
        dateNotification = CodeSnippet.parseDateFormatTo12hoursFormat(dateNotification);
        date = CodeSnippet.parseDateTimeFormatToDate(date);

        tvDate.setText(date);
        String missedCallType = "Scheduled Missed Call";


        tvCallStatus.setText(missedCallType);

        String callStatus = data.getProvider().getFirstName()+" "+data.getProvider().getLastName() +" scheduled video missed call on "+dateNotification;

        tvMissedCallStatus.setText(callStatus);

    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }
}
