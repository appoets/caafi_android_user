package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.response.DoctorsSpecialityList;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class DoctorSpecialityListModel extends BaseModel<DoctorsSpecialityList> {

    public DoctorSpecialityListModel(IModelListener<DoctorsSpecialityList> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(DoctorsSpecialityList response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<DoctorsSpecialityList> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getDoctorsSpeciality(Integer id) {
        enQueueListTask(new ApiClient().getClient().create(ApiInterface.class).getDoctorsSpeciality(id));
    }

    public void getDoctorsHospitalList(Integer id) {
        enQueueListTask(new ApiClient().getClient().create(ApiInterface.class).getDoctorsHospitalList(id));
    }


}
