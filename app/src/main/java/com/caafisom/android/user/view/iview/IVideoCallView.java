package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.presenter.ipresenter.IVideoCallPresenter;
import com.caafisom.android.user.view.adapter.NotificationRecyclerAdapter;

public interface IVideoCallView extends IView<IVideoCallPresenter> {
    void setAdapter(NotificationRecyclerAdapter adapter);
    /*void moveToChat(History data);*/
    void initSetUp();
}
