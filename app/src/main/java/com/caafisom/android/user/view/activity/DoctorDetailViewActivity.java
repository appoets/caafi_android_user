package com.caafisom.android.user.view.activity;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.caafisom.android.user.model.dto.response.SendRequestResponse;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;
import com.caafisom.android.user.view.activity.twilio.TwilloVideoActivity;
import com.bumptech.glide.Glide;
import com.caafisom.android.user.R;
import com.caafisom.android.user.MyApplication;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.response.CheckStatusResponse;
import com.caafisom.android.user.presenter.DoctorDetailViewPresenter;
import com.caafisom.android.user.presenter.ipresenter.IDoctorDetailViewPresenter;
import com.caafisom.android.user.service.CheckStatusService;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.iview.IDoctorDetailView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;

public class DoctorDetailViewActivity extends BaseActivity<IDoctorDetailViewPresenter> implements IDoctorDetailView {


    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.tv_doctor_name)
    TextView tvDocName;
    @BindView(R.id.tv_doctor_spl)
    TextView tvDocSpl;
    @BindView(R.id.rb_doctor)
    RatingBar rbDoc;
    @BindView(R.id.iv_video_call)
    ImageView ivVideoCall;
    @BindView(R.id.tv_exp_txt)
    TextView tvExp;
    @BindView(R.id.tv_rate_txt)
    TextView tvRate;
    @BindView(R.id.tv_desc_txt)
    TextView tvDesc;
    @BindView(R.id.tv_qual_txt)
    TextView tv_qual_txt;
    @BindView(R.id.noImage)
    TextView noImage;
    @BindView(R.id.iv_document)
    ImageView iv_document;
    @BindView(R.id.btnSchedule)
    Button btnSchedule;
    @BindView(R.id.scheduleDateLayout)
    RelativeLayout scheduleDateLayout;
    @BindView(R.id.btnNext)
    Button btnNext;
    @BindView(R.id.scheduleDate)
    TextView scheduleDate;
    @BindView(R.id.scheduleTime)
    TextView scheduleTime;
    private String selectedDate = "", tempfromTime, fromTime = "", toTime = "";
    private Provider data;
    private MyReceiver myReceiver;

    AlertDialog alertDialog;
    public static String currentRequestID;

    private androidx.appcompat.app.AlertDialog findingDoctorDialog;

    @Override
    protected int attachLayout() {
        return R.layout.activity_doctor_detail_view;
    }

    @Override
    protected IDoctorDetailViewPresenter initialize() {
        return new DoctorDetailViewPresenter(this);
    }


    @OnClick({R.id.iv_video_call, R.id.ibBack, R.id.btnSchedule, R.id.dateLayout, R.id.timeLayout, R.id.btnNext})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_video_call:
                iPresenter.showVideoAlert(data);
                break;
            case R.id.ibBack:
                onBackPressed();
                break;
            case R.id.btnSchedule:
                selectedDate = "";
                fromTime = "";
                toTime = "";
                scheduleDateLayout.setVisibility(View.VISIBLE);
                btnSchedule.setVisibility(View.GONE);
                break;

            case R.id.scheduleDateLayout:
                break;

            case R.id.btnNext:
                if (scheduleDate.getText().toString().length() > 1 && scheduleTime.getText().toString().length() > 1) {

                    for (int i = 0; i < data.getService().size(); i++) {
                        iPresenter.scheduleAppointment(data.getService().get(i).getServiceTypeId(), data.getId(), scheduleDate.getText().toString(), scheduleTime.getText().toString(), "1");
                    }

                } else {
                    showToast("Select Date and Time");
                }
                break;

            case R.id.dateLayout:
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                // set day of month , month and year value in the edit text
                                String choosedMonth = "";
                                String choosedDate = "";

                                if (dayOfMonth < 10) {
                                    choosedDate = "0" + dayOfMonth;
                                } else {
                                    choosedDate = "" + dayOfMonth;
                                }

                                if ((monthOfYear + 1) < 10) {
                                    choosedMonth = "0" + (monthOfYear + 1);
                                } else {
                                    choosedMonth = "" + (monthOfYear + 1);
                                }
                                selectedDate = choosedDate + "-" + choosedMonth + "-" + year;
                                //String choosedDateFormat = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

                                if (isAfterToday(year, (monthOfYear + 1), dayOfMonth)) {
                                    scheduleDate.setText(choosedDate + "-" + choosedMonth + "-" + year);
                                } else {
                                    //scheduleDate.setText("");
                                    showToast("Select Date After Today's Date");
                                }

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.getDatePicker().setMaxDate((System.currentTimeMillis() - 1000) + (1000 * 60 * 60 * 24 * 7));
                datePickerDialog.show();
                break;
            case R.id.timeLayout:
                if (selectedDate.equalsIgnoreCase("")) {
                    showToast("Please select date first");
                } else {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String choosedHour = "";
                            String choosedMinute = "";

                            /*if (selectedHour > 12) {
                                choosedTimeZone = "PM";
                                selectedHour = selectedHour - 12;
                                if (selectedHour < 10) {
                                    choosedHour = "0" + selectedHour;
                                } else {
                                    choosedHour = "" + selectedHour;
                                }
                            } else {
                                choosedTimeZone = "AM";
                                if (selectedHour < 10) {
                                    choosedHour = "0" + selectedHour;
                                } else {
                                    choosedHour = "" + selectedHour;
                                }
                            }*/

                            if (selectedHour < 10) {
                                choosedHour = "0" + selectedHour;
                            } else {
                                choosedHour = "" + selectedHour;
                            }

                            if (selectedMinute < 10) {
                                choosedMinute = "0" + selectedMinute;
                            } else {
                                choosedMinute = "" + selectedMinute;
                            }
                            fromTime = choosedHour + ":" + choosedMinute;
                            //choosedTime = choosedHour + ":" + choosedMinute + " " + choosedTimeZone;
                            if (!fromTime.equals("")) {
                                Date date = null;
                                try {
                                    date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(selectedDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                long milliseconds = date.getTime();
                                if (!DateUtils.isToday(milliseconds)) {
                                    scheduleTime.setText(fromTime);
                                } else {
                                    if (CodeSnippet.checktimings(fromTime)) {
                                        scheduleTime.setText(fromTime);
                                    } else {
                                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.different_time), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
                break;
        }
    }

    @Override
    public void setUp(Provider data) {
        this.data = data;
        String currency = getApplicationInstance().getCurrency();
        tvDocName.setText(data.getFirstName() + " " + data.getLastName());

        for (int i = 0; i < data.getService().size(); i++) {
            tvDocSpl.setText(data.getService().get(i).getServiceType().getName());
            String price = currency + data.getService().get(i).getProviderPrice() + "/min";
            tvDesc.setText(data.getDescription());
            tvRate.setText(price);
            rbDoc.setRating(Float.parseFloat(data.getRating()));
        }
        String imageUrl = Constants.URL.BASE_URL_STORAGE + data.getAvatar();
        CodeSnippet.loadImageCircle(imageUrl, ivProfile, R.drawable.ic_dummy_user);
        if (data.getExperience() != null)
            tvExp.setText(data.getExperience() > 0 ? "" + data.getExperience() : " - ");
        else {
            tvExp.setText(" - ");
        }


        for (int i = 0; i < data.getService().size(); i++) {
            if (data.getService().get(i).getServiceType().getDescription() != null)
                tvExp.setText(data.getService().get(i).getServiceType().getDescription());
        }


        tv_qual_txt.setText(data.getQualification() != null && data.getQualification().length() > 0 ? data.getQualification() : " - ");
        if (data.getCertificate() != null && data.getCertificate().length() > 0) {
            noImage.setVisibility(View.GONE);
            Glide.with(MyApplication.getApplicationInstance())
                    .load(data.getCertificate())
                    .into(iv_document);

        } else {
            noImage.setVisibility(View.VISIBLE);
            iv_document.setVisibility(View.GONE);
        }
    }

    @Override
    public void showVideoAlert(Provider data) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        builder.setCancelable(true);
        View view = LayoutInflater.from(Objects.requireNonNull(getActivity())).inflate(R.layout.custom_video_alert, null);
        Button noBtn = view.findViewById(R.id.btn_no);
        Button yesBtn = view.findViewById(R.id.btn_yes);
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.setOnShowListener(arg -> {
            alertDialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.colorOrange));
            alertDialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.colorOrange));
        });
        alertDialog.show();
        yesBtn.setOnClickListener(v -> {
            //Make a video call here
           /* iPresenter.makeVideoCall(data);
            alertDialog.dismiss();*/


            //Make a video call here


            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            String selectedDate = dateFormat.format(Calendar.getInstance().getTime());
            Log.d("selectedDate", "showVideoAlert: " + selectedDate);


            Date date = null;
            try {
                date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm");
            String displayTime = timeFormatter.format(date);

            Log.d("displayTime", "showVideoAlert: " + displayTime);

           /* iPresenter.makeVideoCall(data);
            alertDialog.dismiss();*/

            for (int i = 0; i < data.getService().size(); i++) {
                sendRequestVideoCall(data.getService().get(i).getServiceTypeId(), data.getId(), selectedDate, displayTime, "1");
            }

        });
        noBtn.setOnClickListener(v -> alertDialog.dismiss());

    }

    @Override
    public void makeVideoCall(Provider data) {
        int userid = getApplicationInstance().getUserID();
        String chatPath = getCodeSnippet().getRoomID();

        Bundle bundle = new Bundle();
        bundle.putString("request_id", currentRequestID);
        bundle.putInt("sender", data.getId());
        bundle.putString("sender_name", data.getFirstName());
        bundle.putString("sender_avatar", data.getAvatar());
        if (data.getDevice() != null)
            bundle.putString("device_token", data.getDevice().getToken());
        //  bundle.putString("sender_about", senderStatus);
        bundle.putString("sender_mobile", data.getMobile());
        bundle.putString("chat_path", chatPath);
        bundle.putString("call_type", "video_call");
        bundle.putString("provider_id", data.getId() + "");

        for (int i = 0; i < data.getService().size(); i++) {
            bundle.putString("service_type", data.getService().get(i).getServiceTypeId() + "");
        }

        //navigateTo(MakeCallActivity.class, false, bundle);
        navigateTo(TwilloVideoActivity.class, false, bundle);
    }

    @Override
    public void startCheckStatus(String request_id) {

        Toast.makeText(this, "Request Created, Waiting for doctor approval!", Toast.LENGTH_LONG).show();

        startActivity(new Intent(DoctorDetailViewActivity.this, ScheduleDetailActivity.class).putExtra("id", "" + request_id).putExtra("status", "SEARCHING"));


/*
        showWaitingAlertDialog();
        if (!isMyServiceRunning(CheckStatusService.class)) {
            Intent intent = new Intent(this, CheckStatusService.class);
            intent.putExtra("isWithID", true);
            intent.putExtra("id", request_id);
            startService(intent);
        }
*/
    }

    public static boolean isAfterToday(int year, int month, int day) {
        Calendar today = Calendar.getInstance();
        Calendar myDate = Calendar.getInstance();

        myDate.set(year, month, day);
        return !myDate.before(today);
    }

    public void showWaitingAlertDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertdialog_waiting_for_doctor, null);
        builder.setView(dialogView);
        findingDoctorDialog = builder.create();
        findingDoctorDialog.setCancelable(false);
        findingDoctorDialog.show();
    }

    @Override
    public void onBackPressed() {

        if (scheduleDateLayout.getVisibility() == View.VISIBLE) {
            scheduleDateLayout.setVisibility(View.GONE);
            btnSchedule.setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CodeSnippet.USER_ACTION);
        registerReceiver(myReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("DoctorDetailActivity", " -->  onStop");
        unregisterReceiver(myReceiver);
        endServiceMethod();
    }

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            CheckStatusResponse response;
            if (intent != null && intent.getExtras() != null) {

                if (intent.getParcelableExtra("response") != null) {
                    response = intent.getParcelableExtra("response");
                    Log.e("onReceive Response", response.toString());

                    if (response.getData().size() > 0) {
                        if (response.getData().get(0).getStatus().equalsIgnoreCase("SCHEDULED")) {
                            showToast("Request Scheduled Successfully");
                            endServiceMethod();
                        } else if (response.getData().get(0).getStatus().equalsIgnoreCase("CANCELLED")) {
                            showToast("Request Cancelled");
                            endServiceMethod();
                        }
                    } else {
                        endServiceMethod();
                    }

                }
            }
        }
    }

    public void endServiceMethod() {
        if (findingDoctorDialog != null && findingDoctorDialog.isShowing())
            findingDoctorDialog.dismiss();
        if (isMyServiceRunning(CheckStatusService.class)) {
            stopService(new Intent(DoctorDetailViewActivity.this, CheckStatusService.class));
        }
        if (scheduleDateLayout.getVisibility() == View.VISIBLE) {
            scheduleDateLayout.setVisibility(View.GONE);
            btnSchedule.setVisibility(View.VISIBLE);
        }
    }


    private void sendRequestVideoCall(Integer serviceTypeId, Integer providerid, String selectedDate, String displayTime, String broadcast) {


        Call<SendRequestResponse> call = new ApiClient().getClient().create(ApiInterface.class).sendRequestVideoCall(serviceTypeId, providerid + "", selectedDate, displayTime, broadcast, "1");
        call.enqueue(new Callback<SendRequestResponse>() {
            @Override
            public void onResponse(@NonNull Call<SendRequestResponse> call, @NonNull retrofit2.Response<SendRequestResponse> response) {
                if (response.isSuccessful()) {
                    currentRequestID = response.body().getRequest_id();
                    iPresenter.makeVideoCall(data);
                    alertDialog.dismiss();

                } else {

                    if (response.code() == 500) {
                        alertDialog.dismiss();

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(getApplicationContext(), jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else if (response.code() == 422) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(getApplicationContext(), jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }


                }
            }

            @Override
            public void onFailure(@NonNull Call<SendRequestResponse> call, @NonNull Throwable t) {
                Timber.e("Retrofit NetworkError" + t.getMessage());
                alertDialog.dismiss();

            }
        });

    }


}
