package com.caafisom.android.user.view.fragment;


import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.caafisom.android.user.view.activity.HospitalsListActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.response.CheckStatusResponse;
import com.caafisom.android.user.model.dto.response.SendRequestResponse;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;
import com.caafisom.android.user.presenter.SpecialistFragmentPresenter;
import com.caafisom.android.user.service.CheckStatusService;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.activity.DoctorDetailViewActivity;
import com.caafisom.android.user.view.activity.twilio.TwilloVideoActivity;
import com.caafisom.android.user.view.adapter.ProviderRecyclerAdapter;
import com.caafisom.android.user.view.adapter.ServiceRecyclerAdapter;
import com.caafisom.android.user.view.iview.ISpecialistView;
import com.caafisom.android.user.view.widget.CustomProgressbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;

/**
 * A simple {@link Fragment} subclass.
 */
public class SpecialistFragment extends BaseFragment<SpecialistFragmentPresenter> implements ISpecialistView {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.rvDoctors)
    RecyclerView rvDoctors;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.fabSearch)
    FloatingActionButton fabSearch;
    @BindView(R.id.llPlaceHolder)
    LinearLayout llPlaceHolder;
    @BindView(R.id.rootLayout)
    RelativeLayout rootLayout;
    @BindView(R.id.tvPlaceHolder)
    TextView tvPlaceHolder;
    @BindView(R.id.btnSchedule)
    Button btnSchedule;
    @BindView(R.id.scheduleDateLayout)
    FrameLayout scheduleDateLayout;
    @BindView(R.id.btnNext)
    Button btnNext;
    @BindView(R.id.scheduleDate)
    TextView scheduleDate;
    @BindView(R.id.scheduleTime)
    TextView scheduleTime;
    private String selectedDate = "", tempfromTime, fromTime = "", toTime = "";
    private Integer specialistID;
    private androidx.appcompat.app.AlertDialog findingDoctorDialog;
    private MyReceiver myReceiver;

    public static String currentRequestID;
    Provider data;
    AlertDialog alertDialog;

    @Override
    protected SpecialistFragmentPresenter initialize() {
        return new SpecialistFragmentPresenter(this);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_specialist;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            iPresenter.getServicesList();
        }
    }

    @Override
    protected void setUp() {
        super.setUp();
        recyclerView.setVisibility(View.VISIBLE);
        llPlaceHolder.setVisibility(View.GONE);
        iPresenter.getServicesList();


        etSearch.setOnEditorActionListener((v, keyCode, event) -> {
            if (keyCode == EditorInfo.IME_ACTION_DONE) {
                fabSearch.performClick();
            }
            return true;
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                getServiceDetails();
            }
        });


    }

    private void getServiceDetails() {
        String searchKey = etSearch.getText().toString().trim();
        if (searchKey.isEmpty()) {
            iPresenter.getServicesList();
        } else {
            iPresenter.searchService(searchKey);
        }
    }


    @OnClick({R.id.fabMap, R.id.fabSearch, R.id.btnSchedule, R.id.dateLayout, R.id.timeLayout, R.id.btnNext})
    public void OnClickView(View view) {
        switch (view.getId()) {
            case R.id.fabMap:
                LocalBroadcastManager.getInstance(getApplicationInstance()).sendBroadcast(new Intent(Constants.BROADCAST.SWITCH_VIEW));
                break;

            case R.id.fabSearch:
                getServiceDetails();
                break;

            case R.id.btnSchedule:
                selectedDate = "";
                fromTime = "";
                toTime = "";
                scheduleDateLayout.setVisibility(View.VISIBLE);
                break;

            case R.id.scheduleDateLayout:
                break;

            case R.id.btnNext:
                if (scheduleDate.getText().toString().length() > 1 && scheduleTime.getText().toString().length() > 1) {
                    iPresenter.scheduleAppointment(specialistID, scheduleDate.getText().toString(), scheduleTime.getText().toString(), "1");
                    //showWaitingAlertDialog();
                } else {
                    showToast("Select Date and Time");
                }
                break;

            case R.id.dateLayout:
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                // set day of month , month and year value in the edit text
                                String choosedMonth = "";
                                String choosedDate = "";

                                if (dayOfMonth < 10) {
                                    choosedDate = "0" + dayOfMonth;
                                } else {
                                    choosedDate = "" + dayOfMonth;
                                }

                                if ((monthOfYear + 1) < 10) {
                                    choosedMonth = "0" + (monthOfYear + 1);
                                } else {
                                    choosedMonth = "" + (monthOfYear + 1);
                                }
                                selectedDate = choosedDate + "-" + choosedMonth + "-" + year;
                                //String choosedDateFormat = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

                                if (isAfterToday(year, (monthOfYear + 1), dayOfMonth)) {
                                    scheduleDate.setText(choosedDate + "-" + choosedMonth + "-" + year);
                                } else {
                                    //scheduleDate.setText("");
                                    showToast("Select Date After Today's Date");
                                }

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.getDatePicker().setMaxDate((System.currentTimeMillis() - 1000) + (1000 * 60 * 60 * 24 * 7));
                datePickerDialog.show();
                break;
            case R.id.timeLayout:
                if (selectedDate.equalsIgnoreCase("")) {
                    showToast("Please select date first");
                } else {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String choosedHour = "";
                            String choosedMinute = "";

                            /*if (selectedHour > 12) {
                                choosedTimeZone = "PM";
                                selectedHour = selectedHour - 12;
                                if (selectedHour < 10) {
                                    choosedHour = "0" + selectedHour;
                                } else {
                                    choosedHour = "" + selectedHour;
                                }
                            } else {
                                choosedTimeZone = "AM";
                                if (selectedHour < 10) {
                                    choosedHour = "0" + selectedHour;
                                } else {
                                    choosedHour = "" + selectedHour;
                                }
                            }*/

                            if (selectedHour < 10) {
                                choosedHour = "0" + selectedHour;
                            } else {
                                choosedHour = "" + selectedHour;
                            }

                            if (selectedMinute < 10) {
                                choosedMinute = "0" + selectedMinute;
                            } else {
                                choosedMinute = "" + selectedMinute;
                            }
                            fromTime = choosedHour + ":" + choosedMinute;
                            //choosedTime = choosedHour + ":" + choosedMinute + " " + choosedTimeZone;
                            if (!fromTime.equals("")) {
                                Date date = null;
                                try {
                                    date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(selectedDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                long milliseconds = date.getTime();
                                if (!DateUtils.isToday(milliseconds)) {
                                    scheduleTime.setText(fromTime);
                                } else {
                                    if (CodeSnippet.checktimings(fromTime)) {
                                        scheduleTime.setText(fromTime);
                                    } else {
                                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.different_time), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
                break;
        }
    }

    public static boolean isAfterToday(int year, int month, int day) {
        Calendar today = Calendar.getInstance();
        Calendar myDate = Calendar.getInstance();

        myDate.set(year, month, day);
        return !myDate.before(today);
    }

    @Override
    public void setAdapter(ServiceRecyclerAdapter adapter) {
        setOnBackClickListener();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        rvDoctors.setVisibility(View.GONE);
        llPlaceHolder.setVisibility(View.GONE);
        tvPlaceHolder.setText(R.string.no_specialist_found);
        btnSchedule.setVisibility(View.GONE);
        if (adapter.getItemCount() > 0) {
            llPlaceHolder.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.setAdapter(adapter);
        } else {
            recyclerView.setVisibility(View.GONE);
            llPlaceHolder.setVisibility(View.VISIBLE);
        }
    }

    private void setOnBackClickListener() {
        rootLayout.setFocusableInTouchMode(true);
        rootLayout.requestFocus();
        rootLayout.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    if (scheduleDateLayout.getVisibility() == View.VISIBLE) {
                        scheduleDateLayout.setVisibility(View.GONE);
                        return true;
                    } else if (findingDoctorDialog != null && findingDoctorDialog.isShowing()) {
                        findingDoctorDialog.dismiss();
                        scheduleDateLayout.setVisibility(View.GONE);
                        btnSchedule.setVisibility(View.VISIBLE);
                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void showVideoAlert(Provider data) {
        this.data = data;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.custom_video_alert, null);
        Button noBtn = view.findViewById(R.id.btn_no);
        Button yesBtn = view.findViewById(R.id.btn_yes);
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.setOnShowListener(arg -> {
            alertDialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getContext(), R.color.colorOrange));
            alertDialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(getContext(), R.color.colorOrange));
        });
        alertDialog.show();
        yesBtn.setOnClickListener(v -> {
            //Make a video call here
          /*  iPresenter.makeVideoCall(data);
            alertDialog.dismiss();*/

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            String selectedDate = dateFormat.format(Calendar.getInstance().getTime());
            Log.d("selectedDate", "showVideoAlert: " + selectedDate);


            Date date = null;
            try {
                date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm");
            String displayTime = timeFormatter.format(date);

            Log.d("displayTime", "showVideoAlert: " + displayTime);

           /* iPresenter.makeVideoCall(data);
            alertDialog.dismiss();*/

            for (int i = 0; i < data.getService().size(); i++) {
                sendRequestVideoCall(data.getService().get(i).getServiceTypeId(), data.getId(), selectedDate, displayTime, "1");
            }

        });
        noBtn.setOnClickListener(v -> alertDialog.dismiss());
    }

   /* @Override
    public void setProviderAdapter(ProviderRecyclerAdapter adapter, Integer specialistID) {
        this.specialistID = specialistID;
        rvDoctors.setLayoutManager(new LinearLayoutManager(SpecialistFragment.this.getContext()));
        recyclerView.setVisibility(View.GONE);
        llPlaceHolder.setVisibility(View.GONE);
        tvPlaceHolder.setText(getResources().getString(R.string.no_doctors_found));
        if (adapter.getItemCount() > 0) {
            llPlaceHolder.setVisibility(View.GONE);
            btnSchedule.setVisibility(View.VISIBLE);
            rvDoctors.setVisibility(View.VISIBLE);
            rvDoctors.setAdapter(adapter);
        } else {
            rvDoctors.setVisibility(View.GONE);
            btnSchedule.setVisibility(View.GONE);
            llPlaceHolder.setVisibility(View.VISIBLE);
        }
    }*/


    private void sendRequestVideoCall(Integer serviceTypeId, Integer providerid, String selectedDate, String displayTime, String broadcast) {

        CustomProgressbar mCustomProgressbar = new CustomProgressbar(getContext());
        mCustomProgressbar.setCancelable(false);
        mCustomProgressbar.show();

        Call<SendRequestResponse> call = new ApiClient().getClient().create(ApiInterface.class).sendRequestVideoCall(serviceTypeId, providerid + "", selectedDate, displayTime, broadcast,"1");
        call.enqueue(new Callback<SendRequestResponse>() {
            @Override
            public void onResponse(@NonNull Call<SendRequestResponse> call, @NonNull retrofit2.Response<SendRequestResponse> response) {
                mCustomProgressbar.dismiss();
                if (response.isSuccessful()) {
                    currentRequestID = response.body().getRequest_id();
                    iPresenter.makeVideoCall(data);
                    alertDialog.dismiss();

                } else {

                    if (response.code() == 500) {
                        alertDialog.dismiss();

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(getContext(), jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }  else if(response.code() == 422){
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(getContext(), jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }


                }
            }

            @Override
            public void onFailure(@NonNull Call<SendRequestResponse> call, @NonNull Throwable t) {
                Timber.e("Retrofit NetworkError" + t.getMessage());
                mCustomProgressbar.dismiss();
                alertDialog.dismiss();

            }
        });

    }
    @Override
    public void makeVideoCall(Provider data) {
        int userid = getApplicationInstance().getUserID();
        String chatPath = getCodeSnippet().getRoomID();


        Bundle bundle = new Bundle();
        bundle.putString("request_id", currentRequestID);
        bundle.putInt("sender", data.getId());
        bundle.putString("sender_name", data.getFirstName());
        bundle.putString("sender_avatar", data.getAvatar());
        if (data.getDevice() != null)
            bundle.putString("device_token", data.getDevice().getToken());
        //  bundle.putString("sender_about", senderStatus);
        bundle.putString("sender_mobile", data.getMobile());
        bundle.putString("chat_path", chatPath);
        bundle.putString("call_type", "video_call");
        bundle.putString("provider_id", data.getId() + "");

        for (int i = 0; i < data.getService().size(); i++) {
            bundle.putString("service_type", data.getService().get(i).getServiceTypeId() + "");
        }

        //navigateTo(MakeCallActivity.class, false, bundle);
        navigateTo(TwilloVideoActivity.class,false,bundle);
    }

    @Override
    public void moveToDetailView(Provider data) {
        Bundle extras = new Bundle();
        extras.putParcelable("data", data);
        navigateTo(DoctorDetailViewActivity.class, false, extras);
    }

    public void showWaitingAlertDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertdialog_waiting_for_doctor, null);
        builder.setView(dialogView);
        findingDoctorDialog = builder.create();
        findingDoctorDialog.setCancelable(true);
        findingDoctorDialog.show();
        findingDoctorDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                scheduleDateLayout.setVisibility(View.GONE);
                btnSchedule.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void startCheckStatus(String request_id) {
        showWaitingAlertDialog();
        if (!isMyServiceRunning(CheckStatusService.class)) {
            Intent intent = new Intent(getActivity(), CheckStatusService.class);
            intent.putExtra("isWithID", true);
            intent.putExtra("id", request_id);
            Objects.requireNonNull(getActivity()).startService(intent);
        }
    }

    @Override
    public void gotoProvider(Integer id) {
        Intent intent = new Intent(getActivity(), HospitalsListActivity.class);
        intent.putExtra("service_id",id);
        startActivity(intent);

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CodeSnippet.USER_ACTION);
        getActivity().registerReceiver(myReceiver, intentFilter);
    }

    @Override
    public void onStop() {
        getActivity().unregisterReceiver(myReceiver);
        if (isMyServiceRunning(CheckStatusService.class)) {
            getActivity().stopService(new Intent(getActivity(), CheckStatusService.class));
        }
        super.onStop();
    }

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            CheckStatusResponse response;
            if (intent != null && intent.getExtras() != null) {

                if (intent.getParcelableExtra("response") != null) {
                    response = intent.getParcelableExtra("response");
                    Timber.e(response.toString());

                    if (response.getData().size() > 0) {
                        /*if(response.getData().get(0).getStatus().equalsIgnoreCase("SEARCHING")) {
                        }*/
                    } else {
                        if (findingDoctorDialog != null && findingDoctorDialog.isShowing())
                            findingDoctorDialog.dismiss();
                        if (isMyServiceRunning(CheckStatusService.class)) {
                            getActivity().stopService(new Intent(getActivity(), CheckStatusService.class));
                        }
                        if (scheduleDateLayout.getVisibility() == View.VISIBLE) {
                            scheduleDateLayout.setVisibility(View.GONE);
                            btnSchedule.setVisibility(View.VISIBLE);
                        }
                    }

                }
            }
        }
    }

}
