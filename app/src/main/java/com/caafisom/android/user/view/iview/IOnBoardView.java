package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.presenter.ipresenter.IOnBoardPresenter;

public interface IOnBoardView extends IView<IOnBoardPresenter> {

    void initSetUp();

    void gotoLogin();
}