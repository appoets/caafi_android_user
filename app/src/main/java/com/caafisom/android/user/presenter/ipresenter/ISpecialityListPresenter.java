package com.caafisom.android.user.presenter.ipresenter;

import com.caafisom.android.user.model.dto.common.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface ISpecialityListPresenter extends IPresenter {
    void getSpecialityList();

}
