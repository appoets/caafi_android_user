package com.caafisom.android.user.presenter.ipresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IDoctorSpecialityListPresenter extends IPresenter {
    void getDoctorSpecialityList(Integer id);
    void getDoctorsHospitalList(Integer id);

}
