package com.caafisom.android.user.model.dto.response;

import com.google.gson.annotations.SerializedName;

public class RegisterResponse extends  BaseResponse{

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("login_by")
	private String loginBy;

	@SerializedName("email")
	private String email;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("social_unique_id")
	private int socialUniqueId;

	@SerializedName("first_name")
	private String fname;

	@SerializedName("last_name")
	private String lname;

	@SerializedName("device_id")
	private String deviceID;

	@SerializedName("payment_mode")
	private String paymentMode;

	@SerializedName("id")
	private int id;

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getLoginBy() {
		return loginBy;
	}

	public void setLoginBy(String loginBy) {
		this.loginBy = loginBy;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getSocialUniqueId() {
		return socialUniqueId;
	}

	public void setSocialUniqueId(int socialUniqueId) {
		this.socialUniqueId = socialUniqueId;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}