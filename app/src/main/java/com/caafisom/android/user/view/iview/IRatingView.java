package com.caafisom.android.user.view.iview;

import android.os.Bundle;

import com.caafisom.android.user.presenter.ipresenter.IRatingPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IRatingView extends IView<IRatingPresenter> {
    void setUp(Bundle bundle);
    void onSuccess(String message);
}
