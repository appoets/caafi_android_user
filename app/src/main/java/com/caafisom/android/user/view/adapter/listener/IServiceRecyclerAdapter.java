package com.caafisom.android.user.view.adapter.listener;

import com.caafisom.android.user.model.dto.common.Services;

/**
 * Created by Tranxit Technologies.
 */

public interface IServiceRecyclerAdapter extends BaseRecyclerListener<Services> {
}
