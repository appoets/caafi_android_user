package com.caafisom.android.user.view.activity;

import android.content.Intent;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.caafisom.android.user.model.dto.response.HistoryResponse;
import com.google.firebase.iid.FirebaseInstanceId;
import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.common.History;
import com.caafisom.android.user.presenter.HistoryPresenter;
import com.caafisom.android.user.presenter.ipresenter.IHistoryPresenter;
import com.caafisom.android.user.view.adapter.HistoryRecyclerAdater;
import com.caafisom.android.user.view.iview.IHistoryView;

import butterknife.BindView;
import butterknife.OnClick;

public class HistoryActivity extends BaseActivity<IHistoryPresenter> implements IHistoryView {

    @BindView(R.id.rcvHistory)
    RecyclerView rcvHistory;
    @BindView(R.id.ibBack)
    ImageButton ibBack;

    @BindView(R.id.llNoHistory)
    LinearLayout llNoHistory;

    @Override
    protected int attachLayout() {
        return R.layout.activity_history;
    }

    @Override
    IHistoryPresenter initialize() {
        return new HistoryPresenter(this);
    }

    @Override
    public void initSetUp() {
        rcvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvHistory.setItemAnimator(new DefaultItemAnimator());
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.v(TAG,"Token Received ==>> "+token);
    }

    @OnClick({R.id.ibBack})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                finish();
                break;
        }
    }

    @Override
    public void setAdapter(HistoryRecyclerAdater adapter) {
        if (adapter.getItemCount() > 0) {
            llNoHistory.setVisibility(View.GONE);
            rcvHistory.setVisibility(View.VISIBLE);
            rcvHistory.setAdapter(adapter);
        } else {
            rcvHistory.setVisibility(View.GONE);
            llNoHistory.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void moveToChat(HistoryResponse data) {
        Intent intent1 = new Intent(getActivity(), ChatActivity.class);
        intent1.putExtra("ct_user_id", data.getUserId());
        intent1.putExtra("sender_id", data.getProviderId());
        intent1.putExtra("sender_name", data.getProvider().getFirstName() + " " + data.getProvider().getLastName());
        startActivity(intent1);
    }
}
