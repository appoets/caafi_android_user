package com.caafisom.android.user.view.activity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cooltechworks.creditcarddesign.CardEditActivity;
import com.cooltechworks.creditcarddesign.CreditCardUtils;
import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.Cards;
import com.caafisom.android.user.presenter.WalletPresenter;
import com.caafisom.android.user.presenter.ipresenter.IWalletPresenter;
import com.caafisom.android.user.view.iview.IWalletView;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import butterknife.BindView;
import butterknife.OnClick;

public class WalletActivity extends BaseActivity<IWalletPresenter> implements IWalletView {

    final int GET_NEW_CARD = 2;

    @BindView(R.id.et_amount)
    EditText etamount;

    @BindView(R.id.btn_zaad)
    TextView btnzaad;
    @BindView(R.id.btn_edahab)
    TextView btnedahab;

    boolean back = false;
    private String mCardId = "";
    private String defaultCard = "XXXXXXXXXXXXXXXX";
    private String cardNumber = "";

    @Override
    protected int attachLayout() {
        return R.layout.activity_card;
    }

    @Override
    IWalletPresenter initialize() {
        return new WalletPresenter(this);
    }


    @Override
    public void setUp() {

    }

    @OnClick({R.id.btn_zaad, R.id.ibBack})
    public void OnViewClick(View view) {
        switch (view.getId()) {

            case R.id.btn_zaad:
                addAmount("ZAAD");
                break;

            case R.id.btn_edahab:
                addAmount("EDAHAB");
                break;
            case R.id.ibBack:
                onBackPressed();
                break;

        }
    }

    private void addAmount(String payment) {
        if (etamount.getText().toString().isEmpty()){
            Toast.makeText(this, "Enter the amount", Toast.LENGTH_SHORT).show();
        }
        else {
            iPresenter.addAmount(etamount.getText().toString(),payment);

        }
    }


    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            showProgressbar();

            String cardHolderName = data.getStringExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME);
            cardNumber = data.getStringExtra(CreditCardUtils.EXTRA_CARD_NUMBER);
            String expiry = data.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY);
            String cvv = data.getStringExtra(CreditCardUtils.EXTRA_CARD_CVV);

            // Your processing goes here.
            String[] temp = expiry.split("/");

            int month = Integer.parseInt(temp[0]);
            int year = Integer.parseInt(temp[1]);


            Card card = new Card(
                    cardNumber,
                    month,
                    year,
                    cvv
            );

            if (card.validateNumber() && card.validateCVC()) {
                //showSnackBar("Card Added Successfully");
                Stripe stripe = new Stripe(this, "pk_test_pAqnif8mJ1kwh16JXuApB3Y5");
                stripe.createToken(
                        card,
                        new TokenCallback() {
                            public void onSuccess(Token token) {
                                // Send token to your server
                                dismissProgressbar();
                                iPresenter.addCard(token.getId());
                            }

                            public void onError(Exception error) {
                                // Show localized error message
                                dismissProgressbar();
                                showSnackBar(error.getLocalizedMessage());
                            }
                        }
                );
            } else {
                dismissProgressbar();
                showSnackBar("Invalid card details");
            }

        }
    }*/

    @Override
    public void onSuccess(Cards cards) {
        if (cards != null) {
            String cardNo = defaultCard.substring(0, defaultCard.length() - 4) + cards.getLastFour();
            //tvCardView.setText(cardNo);
            mCardId = cards.getCardId();
        }
    }

    @Override
    public void onAddCard() {
        //iPresenter.getCard();
    }

    @Override
    public void getSuccessResponse() {
        Toast.makeText(this, "Success!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getFailureResponse() {
        Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show();
    }

    /*@Override
    public void onDeleteCard() {
        btnAddCard.setTextColor(getResources().getColor(R.color.colorMaterialIndigo300));
        btnAddCard.setText(R.string.add_card);
        String cardNo = formatCreditCard(defaultCard);
        //tvCardView.setText(cardNo);
    }*/

    /*private String formatCreditCard(String input) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            if (i % 4 == 0 && i != 0) {
                result.append("-");
            }

            result.append(input.charAt(i));
        }

        return result.toString();

    }*/
}
