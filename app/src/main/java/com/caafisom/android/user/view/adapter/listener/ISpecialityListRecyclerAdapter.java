package com.caafisom.android.user.view.adapter.listener;

import com.caafisom.android.user.model.dto.response.SpecialityList;

/**
 * Created by Tranxit Technologies.
 */

public interface ISpecialityListRecyclerAdapter extends BaseRecyclerListener<SpecialityList> {

}
