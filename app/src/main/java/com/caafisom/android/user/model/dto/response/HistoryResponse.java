package com.caafisom.android.user.model.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.caafisom.android.user.model.dto.common.History;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class HistoryResponse extends BaseResponse {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("provider_id")
    @Expose
    private Integer providerId;
    @SerializedName("current_provider_id")
    @Expose
    private Integer currentProviderId;
    @SerializedName("service_type_id")
    @Expose
    private Integer serviceTypeId;
    @SerializedName("before_image")
    @Expose
    private Object beforeImage;
    @SerializedName("before_comment")
    @Expose
    private Object beforeComment;
    @SerializedName("after_image")
    @Expose
    private Object afterImage;
    @SerializedName("after_comment")
    @Expose
    private Object afterComment;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("cancelled_by")
    @Expose
    private String cancelledBy;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("paid")
    @Expose
    private Integer paid;
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("s_address")
    @Expose
    private String sAddress;
    @SerializedName("s_latitude")
    @Expose
    private Integer sLatitude;
    @SerializedName("s_longitude")
    @Expose
    private Integer sLongitude;
    @SerializedName("d_address")
    @Expose
    private Object dAddress;
    @SerializedName("d_latitude")
    @Expose
    private Integer dLatitude;
    @SerializedName("d_longitude")
    @Expose
    private Integer dLongitude;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("assigned_at")
    @Expose
    private String assignedAt;
    @SerializedName("schedule_at")
    @Expose
    private String scheduleAt;
    @SerializedName("started_at")
    @Expose
    private Object startedAt;
    @SerializedName("finished_at")
    @Expose
    private Object finishedAt;
    @SerializedName("user_rated")
    @Expose
    private Integer userRated;
    @SerializedName("provider_rated")
    @Expose
    private Integer providerRated;
    @SerializedName("use_wallet")
    @Expose
    private Integer useWallet;
    @SerializedName("call_seconds")
    @Expose
    private Integer callSeconds;
    @SerializedName("broadcast")
    @Expose
    private Integer broadcast;
    @SerializedName("onsite")
    @Expose
    private Integer onsite;
    @SerializedName("video")
    @Expose
    private Integer video;
    @SerializedName("doctor_id")
    @Expose
    private Integer doctorId;
    @SerializedName("static_map")
    @Expose
    private String staticMap;
    @SerializedName("payment")
    @Expose
    private Object payment;
    @SerializedName("doctor")
    @Expose
    private Doctor doctor;
    @SerializedName("provider")
    @Expose
    private Provider provider;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProviderId() {
        return providerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public Integer getCurrentProviderId() {
        return currentProviderId;
    }

    public void setCurrentProviderId(Integer currentProviderId) {
        this.currentProviderId = currentProviderId;
    }

    public Integer getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(Integer serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public Object getBeforeImage() {
        return beforeImage;
    }

    public void setBeforeImage(Object beforeImage) {
        this.beforeImage = beforeImage;
    }

    public Object getBeforeComment() {
        return beforeComment;
    }

    public void setBeforeComment(Object beforeComment) {
        this.beforeComment = beforeComment;
    }

    public Object getAfterImage() {
        return afterImage;
    }

    public void setAfterImage(Object afterImage) {
        this.afterImage = afterImage;
    }

    public Object getAfterComment() {
        return afterComment;
    }

    public void setAfterComment(Object afterComment) {
        this.afterComment = afterComment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(String cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Integer getPaid() {
        return paid;
    }

    public void setPaid(Integer paid) {
        this.paid = paid;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getsAddress() {
        return sAddress;
    }

    public void setsAddress(String sAddress) {
        this.sAddress = sAddress;
    }

    public Integer getsLatitude() {
        return sLatitude;
    }

    public void setsLatitude(Integer sLatitude) {
        this.sLatitude = sLatitude;
    }

    public Integer getsLongitude() {
        return sLongitude;
    }

    public void setsLongitude(Integer sLongitude) {
        this.sLongitude = sLongitude;
    }

    public Object getdAddress() {
        return dAddress;
    }

    public void setdAddress(Object dAddress) {
        this.dAddress = dAddress;
    }

    public Integer getdLatitude() {
        return dLatitude;
    }

    public void setdLatitude(Integer dLatitude) {
        this.dLatitude = dLatitude;
    }

    public Integer getdLongitude() {
        return dLongitude;
    }

    public void setdLongitude(Integer dLongitude) {
        this.dLongitude = dLongitude;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getAssignedAt() {
        return assignedAt;
    }

    public void setAssignedAt(String assignedAt) {
        this.assignedAt = assignedAt;
    }

    public String getScheduleAt() {
        return scheduleAt;
    }

    public void setScheduleAt(String scheduleAt) {
        this.scheduleAt = scheduleAt;
    }

    public Object getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Object startedAt) {
        this.startedAt = startedAt;
    }

    public Object getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(Object finishedAt) {
        this.finishedAt = finishedAt;
    }

    public Integer getUserRated() {
        return userRated;
    }

    public void setUserRated(Integer userRated) {
        this.userRated = userRated;
    }

    public Integer getProviderRated() {
        return providerRated;
    }

    public void setProviderRated(Integer providerRated) {
        this.providerRated = providerRated;
    }

    public Integer getUseWallet() {
        return useWallet;
    }

    public void setUseWallet(Integer useWallet) {
        this.useWallet = useWallet;
    }

    public Integer getCallSeconds() {
        return callSeconds;
    }

    public void setCallSeconds(Integer callSeconds) {
        this.callSeconds = callSeconds;
    }

    public Integer getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(Integer broadcast) {
        this.broadcast = broadcast;
    }

    public Integer getOnsite() {
        return onsite;
    }

    public void setOnsite(Integer onsite) {
        this.onsite = onsite;
    }

    public Integer getVideo() {
        return video;
    }

    public void setVideo(Integer video) {
        this.video = video;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getStaticMap() {
        return staticMap;
    }

    public void setStaticMap(String staticMap) {
        this.staticMap = staticMap;
    }

    public Object getPayment() {
        return payment;
    }

    public void setPayment(Object payment) {
        this.payment = payment;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }
}
