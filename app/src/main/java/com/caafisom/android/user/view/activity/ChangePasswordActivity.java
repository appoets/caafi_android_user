package com.caafisom.android.user.view.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;


import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.request.ChangePasswordRequest;
import com.caafisom.android.user.presenter.ChangePasswordPresenter;
import com.caafisom.android.user.presenter.ipresenter.IChangePasswordPresenter;
import com.caafisom.android.user.view.iview.IChangePasswordView;

import butterknife.BindView;
import butterknife.OnClick;

public class ChangePasswordActivity extends BaseActivity<IChangePasswordPresenter> implements IChangePasswordView {

    @BindView(R.id.etOldPassword)
    EditText etOldPassword;
    @BindView(R.id.etNewPassword)
    EditText etNewPassword;
    @BindView(R.id.etConfirmPassword)
    EditText etConfirmPassword;

    @Override
    protected int attachLayout() {
        return R.layout.activity_change_password;
    }

    @Override
    IChangePasswordPresenter initialize() {
        return new ChangePasswordPresenter(this);
    }


    @OnClick({R.id.btnChangePassword,R.id.imgBack})
    public void OnClickView(View view) {
        switch (view.getId()) {

            case R.id.btnChangePassword:
                String oldPassword = etOldPassword.getText().toString().trim();
                String newPassword = etNewPassword.getText().toString().trim();
                String confirmNewPassword = etConfirmPassword.getText().toString().trim();

                if (TextUtils.isEmpty(oldPassword)){
                    showSnackBar(getString(R.string.error_please_enter_old_password));
                }else if(TextUtils.isEmpty(newPassword)){
                    showSnackBar(getString(R.string.error_please_enter_new_password));
                }else if(TextUtils.isEmpty(confirmNewPassword)){
                    showSnackBar(getString(R.string.error_please_enter_confirm_password));
                }else{
                    ChangePasswordRequest request = new ChangePasswordRequest();
                    request.setOld_password(oldPassword);
                    request.setPassword(newPassword);
                    request.setPassword_confirmation(confirmNewPassword);
                    iPresenter.changePassword(request);
                }

                break;

            case R.id.imgBack:
                getActivity().onBackPressed();
                break;

        }
    }


    @Override
    public void goToLogin() {
        startActivity(new Intent(this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }
}
