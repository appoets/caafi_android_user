package com.caafisom.android.user.presenter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.caafisom.android.user.R;
import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.ProviderModel;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.response.ProviderResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.IHomeFragmentPresenter;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.iview.IHomeFragementView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import timber.log.Timber;

import static com.caafisom.android.user.util.CodeSnippet.getImageURL;


public class HomeFragmentPresenter extends BasePresenter<IHomeFragementView> implements IHomeFragmentPresenter {

    public HomeFragmentPresenter(IHomeFragementView iView) {
        super(iView);
    }

    @Override
    public void prepareMarkerData(List<Provider> data, Activity activity) {
        new Thread(new PrepareMarker(data,activity)).start();
    }

    class PrepareMarker implements Runnable{
        Activity activity;
        List<Provider> data;

        PrepareMarker(List<Provider> data, Activity activity){
            this.data = data;
            this.activity= activity;
        }

        @Override
        public void run() {
            List<MarkerOptions> markerOptionsList = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {

                if(data.get(i).getLatitude() !=  null && data.get(i).getLongitude() != null) {

                    Log.d("MUDATA", "run: " + i);



                    /*View marker = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
                    final ImageView avatar = marker.findViewById(R.id.image);

                    final String path = data.get(i).getAvatar();
                    activity.runOnUiThread(() -> {
                    Glide.with(marker.getContext()).load(path)
                            .apply(RequestOptions.placeholderOf(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user)).into(avatar);
                    });
                    MarkerOptions proMarker = new MarkerOptions()
                            .position(new LatLng(data.get(i).getLatitude(), data.get(i).getLongitude()))
                            .title(data.get(i).getFirstName() + " " + data.get(i).getLastName())
                            .snippet(data.get(i).getMobile())
                            .icon(BitmapDescriptorFactory.fromBitmap(CodeSnippet.createDrawableFromView1(activity, marker)));
                    markerOptionsList.add(proMarker);*/



                    View markerView = ((LayoutInflater) Objects.requireNonNull(Objects.requireNonNull(activity).getSystemService(Context.LAYOUT_INFLATER_SERVICE)))
                            .inflate(R.layout.custom_marker, null);
                    final ImageView circularImageView = markerView.findViewById(R.id.image);
                    final String path = data.get(i).getAvatar();
                    Timber.e("showProvidersOnMap: %s", getImageURL(path));
                    String baseMergeUrl = getImageURL(path);
                    activity.runOnUiThread(() -> {
                        CodeSnippet.loadImageCircle(baseMergeUrl, circularImageView, R.drawable.ic_dummy_user);
                    });

                    LatLng currentLatLng = new LatLng(data.get(i).getLatitude(), data.get(i).getLongitude());
                    MarkerOptions proMarker = new MarkerOptions()
                            .position(currentLatLng)
                            .icon(BitmapDescriptorFactory.fromBitmap(CodeSnippet.createDrawableFromView(Objects.requireNonNull(activity), markerView))).snippet(i + "");
                    markerOptionsList.add(proMarker);


                    if (i == (data.size() - 1)) {
                        activity.runOnUiThread(() -> {
                            iView.drawOnMap(markerOptionsList);
                        });
                    }

                }
            }
        }
    }

    @Override
    public void getProvidersList() {
        iView.showProgressbar();
        new ProviderModel(new IModelListener<ProviderResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProviderResponse response) {
                iView.dismissProgressbar();
                iView.setMarkers(response.getProvider());

            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProviderResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).getProviderList();
    }

    @Override
    public void moveToDetailView(Provider data) {
        iView.moveToDetailView(data);
    }
}
