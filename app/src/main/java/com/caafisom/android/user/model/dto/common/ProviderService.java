package com.caafisom.android.user.model.dto.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Tranxit Technologies.
 */

public class ProviderService {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("provider_id")
    @Expose
    private Integer providerId;
    @SerializedName("service_type_id")
    @Expose
    private Integer serviceTypeId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("service_number")
    @Expose
    private Object serviceNumber;
    @SerializedName("service_model")
    @Expose
    private Object serviceModel;
    @SerializedName("provider_price")
    @Expose
    private Integer providerPrice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProviderId() {
        return providerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public Integer getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(Integer serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(Object serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public Object getServiceModel() {
        return serviceModel;
    }

    public void setServiceModel(Object serviceModel) {
        this.serviceModel = serviceModel;
    }

    public Integer getProviderPrice() {
        return providerPrice;
    }

    public void setProviderPrice(Integer providerPrice) {
        this.providerPrice = providerPrice;
    }

}
