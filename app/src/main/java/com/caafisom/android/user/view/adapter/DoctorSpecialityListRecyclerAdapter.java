package com.caafisom.android.user.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.DoctorsSpecialityList;
import com.caafisom.android.user.view.adapter.listener.IDoctorsSpecialityListRecyclerAdapter;
import com.caafisom.android.user.view.adapter.viewholder.DoctorSpecialityViewHolder;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class DoctorSpecialityListRecyclerAdapter extends BaseRecyclerAdapter<IDoctorsSpecialityListRecyclerAdapter, DoctorsSpecialityList, DoctorSpecialityViewHolder> {

    IDoctorsSpecialityListRecyclerAdapter iDoctorsSpecialityListRecyclerAdapter;

    public DoctorSpecialityListRecyclerAdapter(List<DoctorsSpecialityList> data, IDoctorsSpecialityListRecyclerAdapter iDoctorsSpecialityListRecyclerAdapter) {
        super(data, iDoctorsSpecialityListRecyclerAdapter);
        this.iDoctorsSpecialityListRecyclerAdapter = iDoctorsSpecialityListRecyclerAdapter;
    }

    @NonNull
    @Override
    public DoctorSpecialityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DoctorSpecialityViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_speciality_list_item,parent,false),iDoctorsSpecialityListRecyclerAdapter);
    }



}
