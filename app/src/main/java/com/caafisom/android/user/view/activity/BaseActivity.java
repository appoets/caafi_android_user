package com.caafisom.android.user.view.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.presenter.ipresenter.IPresenter;
import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.util.PermissionUtils;
import com.caafisom.android.user.view.iview.IView;
import com.caafisom.android.user.view.widget.CustomProgressbar;

import butterknife.ButterKnife;

public abstract class BaseActivity<P extends IPresenter> extends AppCompatActivity implements IView<P> {

    protected String TAG = getClass().getSimpleName();
    protected View mParentView;
    protected CodeSnippet mCodeSnippet;
    protected CustomProgressbar mCustomProgressbar;
    protected P iPresenter;
    protected PermissionUtils mPermission;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(attachLayout());
        System.out.println("BaseActivity.onCreate" + TAG);
        ButterKnife.bind(this);
        iPresenter = initialize();
        iPresenter.onCreate(getIntent().getExtras());
    }

    abstract int attachLayout();

    abstract P initialize();

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mParentView = getWindow().getDecorView().findViewById(android.R.id.content);
        return super.onCreateView(name, context, attrs);
    }

    @Override
    public String getTextRes(int res) {
        return getString(res);
    }

    @Override
    protected void onStart() {
        super.onStart();
        assert iPresenter != null;
        iPresenter.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        assert iPresenter != null;
        iPresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        assert iPresenter != null;
        iPresenter.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        assert iPresenter != null;
        iPresenter.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        assert iPresenter != null;
        iPresenter.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        assert iPresenter != null;
        iPresenter.onActivityForResult(requestCode, resultCode, data);
    }


    private CustomProgressbar getProgressBar() {
        if (mCustomProgressbar == null) mCustomProgressbar = new CustomProgressbar(this);
        return mCustomProgressbar;
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(int resId) {
        Toast.makeText(getActivity(), getString(resId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(CustomException e) {
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAlertDialog(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getText(android.R.string.ok), (dialog, which) -> dialog.cancel());
        alertDialog.show();
    }

    @Override
    public void showAlertDialog(int resId) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(getString(resId));
        alertDialog.setPositiveButton(getText(android.R.string.ok), (dialog, which) -> dialog.cancel());
        alertDialog.show();
    }

    @Override
    public void showAlertDialog(String message, whenClicked clicked) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getText(android.R.string.ok), (dialog, which) -> {
            dialog.cancel();
            clicked.clickEvent();
        });
        alertDialog.show();
    }


    public interface DialogClick {
        void positiveButtonClick();
        void negativeButtonClick();
    }

    @Override
    public void showAlertDialogWithNegativeButton(String message, DialogClick listener) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(getText(android.R.string.ok), (dialog, which) -> {
            dialog.cancel();
            listener.positiveButtonClick();
        });
        alertDialog.setNegativeButton(getText(android.R.string.cancel),((dialog, which) -> {
            dialog.cancel();
            listener.negativeButtonClick();
        }));
        alertDialog.show();
    }

    public void showSettingsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("GPS is settings");
        builder.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        builder.setPositiveButton("Settings", (dialog, which) -> startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)));
        builder.show();
    }

    @Override
    public void showProgressbar() {
        try {
            getProgressBar().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismissProgressbar() {
        runOnUiThread(() -> {
            try {
                getProgressBar().dismissProgress();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public FragmentActivity getActivity() {
        return this;
    }

    @Override
    public void showSnackBar(String message) {
        if (mParentView != null) {
            Snackbar snackbar = Snackbar.make(mParentView, message, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    public void showSnackBarWithDelayExit(String message,long delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        },delay);
        if (mParentView != null) {
            Snackbar snackbar = Snackbar.make(mParentView, message, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    public void showSnackBarWithDelayExit(String message) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        },1500);
        if (mParentView != null) {
            Snackbar snackbar = Snackbar.make(mParentView, message, Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    public void showSnackBar(@NonNull View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        snackbar.show();
    }

    @Override
    public void showNetworkMessage() {
        if (mParentView != null) {
            Snackbar snackbar = Snackbar.make(mParentView, Constants.HttpErrorMessage.NO_NETWORK_FOUND, Snackbar.LENGTH_INDEFINITE);
            snackbar.setActionTextColor(Color.RED);
            snackbar.setAction(Constants.HttpErrorMessage.SETTINGS, view -> getCodeSnippet().showNetworkSettings());
            snackbar.show();
        }
    }

    @Override
    public CodeSnippet getCodeSnippet() {
        if (mCodeSnippet == null) {
            mCodeSnippet = new CodeSnippet(getApplicationContext());
            return mCodeSnippet;
        }
        return mCodeSnippet;
    }

    @Override
    public boolean isNetworkEnabled() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null) if (info.getState() == NetworkInfo.State.CONNECTED) return true;
        }
        return false;
    }

    @Override
    public void makeLogout() {
        iPresenter.onLogout();
    }

    @Override
    public void navigateTo(Class<?> cls, boolean isFinishActivity, Bundle bundle) {
        startActivity(new Intent(getActivity(), cls).putExtras(bundle));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);      /*      anim in     */
        if (isFinishActivity) finish();
        //  overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);     /*      anim out    */
    }


    @Override
    public void changeFragment(Bundle bundle,
                               @IdRes int containerViewId,
                               @NonNull Fragment fragment,
                               @NonNull String fragmentTag) {
        fragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(this.getClass().getSimpleName())
                .commit();
    }

    @Override
    public void addFragment(Bundle bundle,
                            @IdRes int containerViewId,
                            @NonNull Fragment fragment,
                            @NonNull String fragmentTag) {
        fragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .add(containerViewId, fragment, fragmentTag)
                .addToBackStack(this.getClass().getSimpleName())
                .commit();
    }

    @Override
    public void showTextInputLayoutError(TextInputLayout til, EditText et, String msg) {
        til.setError(msg);
        if (et.requestFocus())
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    @Override
    public void finish() {
        super.finish();
        System.out.println("BaseActivity.finish" + TAG);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);     /*      anim out    */
    }

    public interface whenClicked {
        void clickEvent();
    }

    @Override
    public boolean checkLocationStatus() {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return false;
        }
        return true;
    }

    @Override
    public void onActivityForResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public String getStringRes(int resId) {
        return getString(resId);
    }

    @Override
    public void onLogout(CustomException e) {

    }

    @Override
    public void onLogout() {

    }


    @Override
    public PermissionUtils getPermissionUtils() {
        if (mPermission == null) {
            mPermission = new PermissionUtils();
            return mPermission;
        }
        return mPermission;
    }


}
