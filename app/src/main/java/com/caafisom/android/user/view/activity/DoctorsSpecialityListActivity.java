package com.caafisom.android.user.view.activity;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.DoctorsSpecialityList;
import com.caafisom.android.user.presenter.DocotorSpecialityListPresenter;
import com.caafisom.android.user.view.adapter.DoctorSpecialityListRecyclerAdapter;
import com.caafisom.android.user.view.iview.IDoctorSpecialityListView;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.OnClick;

public class DoctorsSpecialityListActivity extends BaseActivity<DocotorSpecialityListPresenter> implements IDoctorSpecialityListView {

    @BindView(R.id.rv_DoctorSpeciality)
    RecyclerView rvDoctorSpeciality;
    @BindView(R.id.heading)
    TextView heading;
    @BindView(R.id.tv_empty)
    TextView tvempty;
    private Integer speciality_id, hospital_id;
    @BindView(R.id.ibBack)
    ImageView ibBack;

    @Override
    protected int attachLayout() {
        return R.layout.activity_doctors_speciality_list;
    }

    @Override
    DocotorSpecialityListPresenter initialize() {
        return new DocotorSpecialityListPresenter(this);
    }

    @SuppressLint("LogNotTimber")
    @Override
    public void initSetUp() {

        Intent intent = getIntent();
        if (intent != null) {
            speciality_id = intent.getIntExtra("speciality_id", 0);
            hospital_id = intent.getIntExtra("hospital_id", 0);
        }

        Log.e("hospital_id", "" + hospital_id);

        if (speciality_id != 0) {
            iPresenter.getDoctorSpecialityList(speciality_id);
        } else {
            iPresenter.getDoctorsHospitalList(hospital_id);
        }

        rvDoctorSpeciality.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvDoctorSpeciality.setItemAnimator(new DefaultItemAnimator());
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.v(TAG, "Token Received ==>> " + token);
    }

    @OnClick({R.id.ibBack})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void setAdapter(DoctorSpecialityListRecyclerAdapter adapter) {
        rvDoctorSpeciality.setHasFixedSize(true);
        rvDoctorSpeciality.setLayoutManager(new LinearLayoutManager(this));
        if (adapter.getItemCount() > 0) {
            tvempty.setVisibility(View.GONE);
            rvDoctorSpeciality.setVisibility(View.VISIBLE);
            rvDoctorSpeciality.setAdapter(adapter);
        } else {
            rvDoctorSpeciality.setVisibility(View.GONE);
            tvempty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getDoctorsList(DoctorsSpecialityList data) {
//        Toast.makeText(this, "selected :" + data.getFirstName(), Toast.LENGTH_SHORT).show();
        if (data.getAvailable().size() > 0) {
            Intent intent = new Intent(DoctorsSpecialityListActivity.this, DoctorBookingActivity.class);
            intent.putExtra("doctor_id", data.getId());
            intent.putExtra("doctor_firstname", data.getFirstName());
            intent.putExtra("doctor_lastname", data.getLastName());
            intent.putExtra("doctor_image", data.getPicture());
            intent.putExtra("doctor_spl", data.getSpeciality().getName());
            intent.putExtra("doctor_consultationFee", data.getConsultationFee());
            intent.putExtra("doctor_spl_id", data.getSpeciality().getId());
            intent.putExtra("hospital_id", data.getProvider().getId());
            startActivity(intent);
        } else {
            Toast.makeText(this, "Doctor is not available", Toast.LENGTH_SHORT).show();
        }

       /* Intent intent = new Intent(DoctorsSpecialityListActivity.this, DoctorBookingActivity.class);
        intent.putExtra("doctor_id", data.getId());
        intent.putExtra("doctor_firstname", data.getFirstName());
        intent.putExtra("doctor_lastname", data.getLastName());
        intent.putExtra("doctor_image", data.getPicture());
        intent.putExtra("doctor_spl", data.getSpeciality().getName());
        intent.putExtra("doctor_consultationFee", data.getConsultationFee());
        intent.putExtra("doctor_spl_id", data.getSpeciality().getId());
        intent.putExtra("hospital_id", data.getProvider().getId());
        startActivity(intent);
        finish();*/


    }


}