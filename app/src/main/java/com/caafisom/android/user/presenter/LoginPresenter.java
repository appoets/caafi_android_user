package com.caafisom.android.user.presenter;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import android.util.Log;

import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.LoginModel;
import com.caafisom.android.user.model.ProfileModel;
import com.caafisom.android.user.model.dto.request.LoginRequest;
import com.caafisom.android.user.model.dto.response.LoginResponse;
import com.caafisom.android.user.model.dto.response.ProfileResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.ILoginPresenter;
import com.caafisom.android.user.view.iview.ILoginView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;


public class LoginPresenter extends BasePresenter<ILoginView> implements ILoginPresenter {

    public LoginPresenter(ILoginView iView) {
        super(iView);
    }

    @Override
    public void goToForgotPassword() {
        iView.goToForgotPassword();
    }

    @Override
    public void goToRegistration() {
        iView.goToRegistration();
    }

    @Override
    public void postLogin(LoginRequest request) {
        iView.showProgressbar();
        new LoginModel(new IModelListener<LoginResponse>() {
            @SuppressLint("LogNotTimber")
            @Override
            public void onSuccessfulApi(@NonNull LoginResponse response) {
                getApplicationInstance().setAccessToken(response.getAccessToken());
                Log.e("TOKEN=======>"," "+ response.getAccessToken());
                getUserInfo();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<LoginResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).postLogin(request);
    }

    private void getUserInfo() {
        new ProfileModel(new IModelListener<ProfileResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProfileResponse response) {
                getApplicationInstance().setCurrency(response.getCurrency());
                iView.dismissProgressbar();
                iView.goToHome();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProfileResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getUserDetails();
    }
}
