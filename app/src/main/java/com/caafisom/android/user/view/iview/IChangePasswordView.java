package com.caafisom.android.user.view.iview;


import com.caafisom.android.user.presenter.ipresenter.IChangePasswordPresenter;

public interface IChangePasswordView extends IView<IChangePasswordPresenter> {
                void goToLogin();
}
