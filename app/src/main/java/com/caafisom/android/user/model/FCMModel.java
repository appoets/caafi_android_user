package com.caafisom.android.user.model;

import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.dto.common.videocalldata.Fcm;
import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class FCMModel extends BaseModel<BaseResponse> {

    public FCMModel(IModelListener<BaseResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(BaseResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<BaseResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void sendFCM(Fcm fcm){
        enQueueTask(new ApiClient().getClient(Constants.URL.FCM_URL).create(ApiInterface.class).sendFCM(fcm));
    }
}
