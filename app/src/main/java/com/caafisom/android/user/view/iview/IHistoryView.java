package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.model.dto.common.History;
import com.caafisom.android.user.model.dto.response.HistoryResponse;
import com.caafisom.android.user.presenter.ipresenter.IHistoryPresenter;
import com.caafisom.android.user.view.adapter.HistoryRecyclerAdater;

/**
 * Created by Tranxit Technologies.
 */

public interface IHistoryView extends IView<IHistoryPresenter> {
    void setAdapter(HistoryRecyclerAdater adapter);
    void moveToChat(HistoryResponse data);
    void initSetUp();
}
