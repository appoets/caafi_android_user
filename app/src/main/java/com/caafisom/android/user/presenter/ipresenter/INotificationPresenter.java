package com.caafisom.android.user.presenter.ipresenter;

public interface INotificationPresenter extends IPresenter {

    void getNotification();
}
