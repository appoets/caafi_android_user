package com.caafisom.android.user.model.webservice;


import androidx.annotation.NonNull;
import android.text.TextUtils;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.caafisom.android.user.BuildConfig.BASE_URL;
import static com.caafisom.android.user.MyApplication.getApplicationInstance;
import static com.caafisom.android.user.common.Constants.Header.HEADER_AUTHORIZATION;
import static com.caafisom.android.user.common.Constants.Header.HEADER_CONTENT_NAME;
import static com.caafisom.android.user.common.Constants.Header.HEADER_CONTENT_VALUE;


public class ApiClient {

    private Retrofit retrofit;

    public Retrofit getClient() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        if (retrofit == null) {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient
                    .addNetworkInterceptor(new AddHeaderInterceptor())
//                    .addNetworkInterceptor(new StethoInterceptor())
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true);
            httpClient.addInterceptor(logging);

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }

        return retrofit;
    }

    public Retrofit getClient(String URL) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        if (retrofit == null) {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient
                    .addNetworkInterceptor(new AddHeaderInterceptor())
//                    .addNetworkInterceptor(new StethoInterceptor())
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true);
            httpClient.addInterceptor(logging);

            retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }

        return retrofit;
    }

    public class AddHeaderInterceptor implements Interceptor {
        @Override
        public Response intercept(@NonNull Chain chain) throws IOException {
            String type = "Bearer";
            String token = getApplicationInstance().getAccessToken();
            Request.Builder builder = chain.request().newBuilder();
            builder.addHeader(HEADER_CONTENT_NAME, HEADER_CONTENT_VALUE);
            if (!TextUtils.isEmpty(getApplicationInstance().getAccessToken()))
                builder.addHeader(HEADER_AUTHORIZATION, type + " " + token);
            return chain.proceed(builder.build());
        }
    }

}
