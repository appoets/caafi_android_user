package com.caafisom.android.user.model.listener;


import com.caafisom.android.user.model.CustomException;

import java.util.List;

public interface IBaseModelListener<BML> {

    void onSuccessfulApi(BML response);

    void onSuccessfulApi(List<BML> response);

    void onFailureApi(CustomException e);

    void onUnauthorizedUser(CustomException e);

    void onNetworkFailure();
}
