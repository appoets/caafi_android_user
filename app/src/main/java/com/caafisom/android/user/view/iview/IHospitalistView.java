package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.presenter.SearchProviderPresenter;
import com.caafisom.android.user.presenter.ipresenter.IHospitalListPresenter;
import com.caafisom.android.user.view.adapter.ProviderRecyclerAdapter;

/**
 * Created by Tranxit Technologies.
 */

public interface IHospitalistView extends IView<SearchProviderPresenter> {
    void setProviderAdapter(ProviderRecyclerAdapter providerRecyclerAdapter);
    void initSetUp();
    void moveToDoctoBook(Provider data);
}
