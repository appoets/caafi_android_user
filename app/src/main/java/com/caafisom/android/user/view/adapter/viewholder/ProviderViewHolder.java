package com.caafisom.android.user.view.adapter.viewholder;

import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.media.Rating;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.adapter.listener.IProviderRecyclerAdapter;

import java.util.Random;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;

import org.w3c.dom.Text;

/**
 * Created by Tranxit Technologies.
 */

public class ProviderViewHolder extends BaseViewHolder<Provider, IProviderRecyclerAdapter> {

    @BindView(R.id.civProfile)
    ImageView civProfile;
    @BindView(R.id.tvHospital_Name)
    TextView tvHospitalName;
    @BindView(R.id.tvHospital_Spl)
    TextView tvHospitalSpl;
    @BindView(R.id.tv_rating)
    TextView tvrating;
    @BindView(R.id.rating)
    RatingBar rating;
    @BindView(R.id.tv_book_appointment)
    ImageView tvbookappointment;
    @BindView(R.id.tv_open)
    TextView tvopen;
    @BindView(R.id.tv_close)
    TextView tvclose;
    @BindView(R.id.tvLocation)
    TextView tvLocation;

    public ProviderViewHolder(View itemView, IProviderRecyclerAdapter listener) {
        super(itemView, listener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    void populateData(Provider data) {
        if (data != null) {
            tvHospitalName.setText(data.getFirstName() + " " + data.getLastName());

            for (int i = 0; i < data.getService().size(); i++) {
                tvHospitalSpl.setText(data.getService().get(i).getServiceType().getName());
            }

            tvrating.setText(data.getRating());
            rating.setRating(Float.parseFloat(data.getRating()));

          /*  tvopen.setText(data.getRating());
            tvclose.setText(data.getRating());*/
//            tvLocation.setText(data.get());

            if (data.getAvatar() != null) {
                CodeSnippet.loadImage(data.getAvatar(), civProfile);
            }

        }
    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }
}
