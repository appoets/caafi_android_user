package com.caafisom.android.user.view.activity;

import android.Manifest;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;

import com.caafisom.android.user.presenter.SplashPresenter;
import com.caafisom.android.user.presenter.ipresenter.ISplashPresenter;
import com.caafisom.android.user.R;
import com.caafisom.android.user.view.iview.ISplashView;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;

@RuntimePermissions
public class SplashActivity extends BaseActivity<ISplashPresenter> implements ISplashView {

    @Override
    protected int attachLayout() {

        String token = getApplicationInstance().getAccessToken();

        Log.d(TAG, "attachLayout: token==>"+ token);
        Log.e("TOKEN=======>"," "+ token);


        return R.layout.activity_splash;
    }

    @Override
    ISplashPresenter initialize() {
        return new SplashPresenter(this);
    }


    @Override
    public void startTimer(int splashTimer) {
        SplashActivityPermissionsDispatcher.goToNextScreenWithPermissionCheck(this,splashTimer);

    }

    @NeedsPermission({ Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO,Manifest.permission.CAMERA})
    public void goToNextScreen(int splashTimer){
        new Handler().postDelayed(() -> {
            if (iPresenter.hasInternet()) {
                if (iPresenter.onCheckUserStatus()) iPresenter.goToHome();
                else iPresenter.goToLogin();
            } else showNetworkMessage();
        }, splashTimer);
    }

    @Override
    public void gotoLogin() {
        navigateTo(OnBoardActivity.class, true, new Bundle());
    }

    @Override
    public void gotoHome() {
        navigateTo(HomeActivity.class,true,new Bundle());
        finishAffinity();
    }


    @OnShowRationale({ Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO,Manifest.permission.CAMERA})
    void showRationaleForPermission(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_rationale)
                .setPositiveButton(R.string.allow, (dialog, button) -> request.proceed())
                .setNegativeButton(R.string.deny, (dialog, button) -> request.cancel())
                .show();
    }

    @OnPermissionDenied({ Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO,Manifest.permission.CAMERA})
    void showDeniedForPermission() {
        showSnackBar("You have denied permission");
    }

    @OnNeverAskAgain({ Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO,Manifest.permission.CAMERA})
    void showNeverAskForPermission() {
        showSnackBarWithDelayExit("You have denied permission");
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        SplashActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


}
