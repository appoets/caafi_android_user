package com.caafisom.android.user.view.adapter.listener;

import com.caafisom.android.user.model.dto.response.DoctorsSpecialityList;

/**
 * Created by Tranxit Technologies.
 */

public interface IDoctorsSpecialityListRecyclerAdapter extends BaseRecyclerListener<DoctorsSpecialityList> {

}
