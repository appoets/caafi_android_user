package com.caafisom.android.user.presenter;


import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.SpecialityListModel;
import com.caafisom.android.user.model.dto.response.SpecialityList;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.ISpecialityListPresenter;
import com.caafisom.android.user.view.adapter.ProviderRecyclerAdapter;
import com.caafisom.android.user.view.adapter.SpecialityListRecyclerAdapter;
import com.caafisom.android.user.view.adapter.listener.ISpecialityListRecyclerAdapter;
import com.caafisom.android.user.view.iview.ISpecialityListView;


import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class SpecialityListPresenter extends BasePresenter<ISpecialityListView> implements ISpecialityListPresenter {

    public SpecialityListPresenter(ISpecialityListView iView) {
        super(iView);
    }

    ISpecialityListRecyclerAdapter iSpecialityListRecyclerAdapter = new ISpecialityListRecyclerAdapter() {
        @Override
        public void onClickItem(int pos, SpecialityList data) {
            iView.getDoctorsList(data);
        }
    };


    @Override
    public void getSpecialityList() {
        iView.showProgressbar();

        new SpecialityListModel(new IModelListener<SpecialityList>() {
            @Override
            public void onSuccessfulApi(@NotNull SpecialityList response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull List<SpecialityList> response) {
                iView.dismissProgressbar();
                iView.setAdapter(new SpecialityListRecyclerAdapter(response, iSpecialityListRecyclerAdapter));
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).getSpecialityList();
    }


}
