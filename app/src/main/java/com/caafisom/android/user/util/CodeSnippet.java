package com.caafisom.android.user.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.Settings;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.caafisom.android.user.BuildConfig;
import com.caafisom.android.user.R;
import com.caafisom.android.user.MyApplication;
import com.caafisom.android.user.common.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;

public class CodeSnippet {

    private Context mContext;
    private String TAG = "CodeSnippet";
    public static String USER_ACTION = "CHECK_STATUS_RESPONSE";

    public CodeSnippet(Context mContext) {
        this.mContext = mContext;
    }

    public CodeSnippet() {
    }

    public static String getImageURL(String imgurl) {
        String imageURL = "";

        if (imgurl != null && !imgurl.equalsIgnoreCase("")) {
            if (!imgurl.startsWith("http")) {
                imageURL = Constants.URL.BASE_URL_STORAGE + imgurl;
            } else {
                imageURL = imgurl;
            }
        }
    /*    if (imgurl != null && !imgurl.equalsIgnoreCase("")) {
            if (!imgurl.startsWith("http")) {
                imageURL = "http://" + imgurl;
            } else {
                imageURL = Constants.URL.BASE_URL_STORAGE + imgurl;
            }
        }*/

        System.out.println("Get Image URL===>" + imageURL);
        return imageURL;
    }

    public String getCurrentTime() {
        String currentDateAndTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        return currentDateAndTime;
    }


    /**
     * @param message the message to be encoded
     *
     * @return the enooded from of the message
     */
    public static String encodeBase64(String message) {
        byte[] data;
        try {
            data = message.getBytes("UTF-8");
            String base64Sms = Base64.encodeToString(data, Base64.DEFAULT);
            return base64Sms;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * @param message the encoded message
     *
     * @return the decoded message
     */
    public static String decodeBase64(String message) {
        byte[] data = Base64.decode(message, Base64.DEFAULT);
        try {
            return new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }


    public String getChatPath(int calledid,int senderid){
        return calledid+"_"+ BuildConfig.APPLICATION_ID.replace(".","_")+"_"+senderid;
    }

    public String getRoomID(){
        return generateRandomString(15);
    }

    public static String parseDateToyyyyMMdd(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "yyyy-MM-dd (HH:mm a)";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseDateFormatTo12hoursFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MM-yyyy hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseDateTimeFormatToDate(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MM-yyyy";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date todaysDate = new Date();
        try {
            todaysDate = inputFormat.parse(new SimpleDateFormat(inputPattern).format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date.compareTo(todaysDate)==0){
            return "Today";
        }

        return str;
    }

    @SuppressLint("DefaultLocale")
    public String secToDisplayFormat(long value){
        int diffMinutes=0;
        int diffSeconds =(int) value;
        if (value>=60)
        diffSeconds = (int) value % 60;
        if (value > 60)
        diffMinutes = (int) value / 60;
        return format("%02d:%02d", diffMinutes, diffSeconds);

    }

    public static String getDisplayableTime(long value) {
        long delta = value;
        long difference = 0;
        Long mDate = java.lang.System.currentTimeMillis();

        if (mDate > delta) {
            difference = mDate - delta;
            final long seconds = difference / 1000;
            final long minutes = seconds / 60;
            final long hours = minutes / 60;
            final long days = hours / 24;
            final long months = days / 31;
            final long years = days / 365;

            if (seconds < 0) {
                return "not yet";
            } else if (seconds < 60) {
                return seconds == 1 ? "one second ago" : seconds + " seconds ago";
            } else if (seconds < 120) {
                return "a minute ago";
            } else if (seconds < 2700) // 45 * 60
            {
                return minutes + " minutes ago";
            } else if (seconds < 5400) // 90 * 60
            {
                return "an hour ago";
            } else if (seconds < 86400) // 24 * 60 * 60
            {
                return hours + " hours ago";
            } else if (seconds < 172800) // 48 * 60 * 60
            {
                return "yesterday";
            } else if (seconds < 2592000) // 30 * 24 * 60 * 60
            {
                return days + " days ago";
            } else if (seconds < 31104000) // 12 * 30 * 24 * 60 * 60
            {

                return months <= 1 ? "one month ago" : days + " months ago";
            } else {

                return years <= 1 ? "one year ago" : years + " years ago";
            }
        }
        return "justnow";
    }

    public static void loadImage(String url, ImageView target) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.sample)
                .priority(Priority.HIGH);

        Glide.with(MyApplication.getApplicationInstance())
                .load(url)
                .apply(options)
                .into(target);
    }

    public static void loadImage(Uri uri, ImageView target) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_dummy_user)
                .priority(Priority.HIGH);

        Glide.with(MyApplication.getApplicationInstance())
                .load(uri)
                .apply(options)
                .into(target);
    }

    public static void loadImageCircle(String url, ImageView target, int placeholderDrawable) {
        //Log.d("MUDATA", "loadImageCircle: "+url);
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(placeholderDrawable)
                .apply(RequestOptions.circleCropTransform())
                .priority(Priority.HIGH);

        Glide.with(MyApplication.getApplicationInstance())
                .load(url)
                .apply(options)
                .into(target);
    }

    public static void loadImageCircle(Uri uri, ImageView target, int placeholderDrawable) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(placeholderDrawable)
                .apply(RequestOptions.circleCropTransform())
                .priority(Priority.HIGH);

        Glide.with(MyApplication.getApplicationInstance())
                .load(uri)
                .apply(options)
                .into(target);
    }

    public static Bitmap createDrawableFromView(Context context, View v) {
        Bitmap b = null;
        if (v.getMeasuredHeight() <= 0) {
            v.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            //v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
            //v.draw(c);
            return b;
        }else{
            b = Bitmap.createBitmap( v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
            v.draw(c);
        }

        return b;
    }

    public Context getContext() {
        if (mContext == null) {
            mContext = MyApplication.getApplicationInstance();
        }
        return mContext;
    }

    @SuppressLint("all")
    public String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public String loadJSONFromAsset(Context context, String jsonFileName)
            throws IOException {

        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, "UTF-8");
    }

    public String getTimeStamp() {
        return new SimpleDateFormat(Constants.TimeFormats.TIMESTAMP_FORMAT, Locale.US).format(new Date());
    }

    public void showNetworkSettings() {
        Intent chooserIntent = Intent.createChooser(new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS), "Complete action using");
        List<Intent> networkIntents = new ArrayList<>();
        networkIntents.add(new Intent(Settings.ACTION_WIFI_SETTINGS));
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, networkIntents.toArray(new Parcelable[]{}));
        chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(chooserIntent);
    }

    public boolean hasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null)
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) return true;
            else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) return true;
        return false;
    }


    public String generateRandomString( int len ){
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }


    public long getSecondsDifference(String dateStart,String dateStop){
        long diffSeconds =0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = d2.getTime() - d1.getTime();
        diffSeconds = diff / 1000;

        return diffSeconds;
    }

    public static boolean checktimings(String time) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            String currentTime = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(currentTime);

            return date1.after(date2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

}
