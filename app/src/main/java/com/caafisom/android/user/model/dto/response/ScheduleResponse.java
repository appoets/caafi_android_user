package com.caafisom.android.user.model.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ScheduleResponse extends BaseResponse {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("provider_id")
    @Expose
    private Integer providerId;
    @SerializedName("current_provider_id")
    @Expose
    private Integer currentProviderId;
    @SerializedName("service_type_id")
    @Expose
    private Integer serviceTypeId;
    @SerializedName("before_image")
    @Expose
    private String beforeImage;
    @SerializedName("before_comment")
    @Expose
    private String beforeComment;
    @SerializedName("after_image")
    @Expose
    private String afterImage;
    @SerializedName("after_comment")
    @Expose
    private String afterComment;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("cancelled_by")
    @Expose
    private String cancelledBy;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("paid")
    @Expose
    private Integer paid;
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("s_address")
    @Expose
    private String sAddress;
    @SerializedName("s_latitude")
    @Expose
    private Double sLatitude;
    @SerializedName("s_longitude")
    @Expose
    private Double sLongitude;
    @SerializedName("d_address")
    @Expose
    private String dAddress;
    @SerializedName("d_latitude")
    @Expose
    private Double dLatitude;
    @SerializedName("d_longitude")
    @Expose
    private Double dLongitude;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("assigned_at")
    @Expose
    private String assignedAt;
    @SerializedName("schedule_at")
    @Expose
    private String scheduleAt;
    @SerializedName("started_at")
    @Expose
    private String startedAt;
    @SerializedName("finished_at")
    @Expose
    private String finishedAt;
    @SerializedName("user_rated")
    @Expose
    private Integer userRated;
    @SerializedName("provider_rated")
    @Expose
    private Integer providerRated;
    @SerializedName("use_wallet")
    @Expose
    private Integer useWallet;
    @SerializedName("call_seconds")
    @Expose
    private Integer callSeconds;
    @SerializedName("broadcast")
    @Expose
    private Integer broadcast;
    @SerializedName("service_type")
    @Expose
    private ServiceType service_type;
    @SerializedName("provider")
    @Expose
    private Provider provider;
    @SerializedName("provider_service")
    @Expose
    private Provider_Service provider_service;
    @SerializedName("payment")
    @Expose
    private Payment payment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProviderId() {
        return providerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public Integer getCurrentProviderId() {
        return currentProviderId;
    }

    public void setCurrentProviderId(Integer currentProviderId) {
        this.currentProviderId = currentProviderId;
    }

    public Integer getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(Integer serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getBeforeImage() {
        return beforeImage;
    }

    public void setBeforeImage(String beforeImage) {
        this.beforeImage = beforeImage;
    }

    public String getBeforeComment() {
        return beforeComment;
    }

    public void setBeforeComment(String beforeComment) {
        this.beforeComment = beforeComment;
    }

    public String getAfterImage() {
        return afterImage;
    }

    public void setAfterImage(String afterImage) {
        this.afterImage = afterImage;
    }

    public String getAfterComment() {
        return afterComment;
    }

    public void setAfterComment(String afterComment) {
        this.afterComment = afterComment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(String cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Integer getPaid() {
        return paid;
    }

    public void setPaid(Integer paid) {
        this.paid = paid;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getSAddress() {
        return sAddress;
    }

    public void setSAddress(String sAddress) {
        this.sAddress = sAddress;
    }

    public Double getSLatitude() {
        return sLatitude;
    }

    public void setSLatitude(Double sLatitude) {
        this.sLatitude = sLatitude;
    }

    public Double getSLongitude() {
        return sLongitude;
    }

    public void setSLongitude(Double sLongitude) {
        this.sLongitude = sLongitude;
    }

    public String getDAddress() {
        return dAddress;
    }

    public void setDAddress(String dAddress) {
        this.dAddress = dAddress;
    }

    public Double getDLatitude() {
        return dLatitude;
    }

    public void setDLatitude(Double dLatitude) {
        this.dLatitude = dLatitude;
    }

    public Double getDLongitude() {
        return dLongitude;
    }

    public void setDLongitude(Double dLongitude) {
        this.dLongitude = dLongitude;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getAssignedAt() {
        return assignedAt;
    }

    public void setAssignedAt(String assignedAt) {
        this.assignedAt = assignedAt;
    }

    public String getScheduleAt() {
        return scheduleAt;
    }

    public void setScheduleAt(String scheduleAt) {
        this.scheduleAt = scheduleAt;
    }

    public String getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    public String getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(String finishedAt) {
        this.finishedAt = finishedAt;
    }

    public Integer getUserRated() {
        return userRated;
    }

    public void setUserRated(Integer userRated) {
        this.userRated = userRated;
    }

    public Integer getProviderRated() {
        return providerRated;
    }

    public void setProviderRated(Integer providerRated) {
        this.providerRated = providerRated;
    }

    public Integer getUseWallet() {
        return useWallet;
    }

    public void setUseWallet(Integer useWallet) {
        this.useWallet = useWallet;
    }

    public Integer getCallSeconds() {
        return callSeconds;
    }

    public void setCallSeconds(Integer callSeconds) {
        this.callSeconds = callSeconds;
    }

    public Integer getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(Integer broadcast) {
        this.broadcast = broadcast;
    }

    public ServiceType getService_type() {
        return service_type;
    }

    public void setService_type(ServiceType service_type) {
        this.service_type = service_type;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Provider_Service getProvider_service() {
        return provider_service;
    }

    public void setProvider_service(Provider_Service provider_service) {
        this.provider_service = provider_service;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public class Payment {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("request_id")
        @Expose
        private Integer requestId;
        @SerializedName("promocode_id")
        @Expose
        private Object promocodeId;
        @SerializedName("payment_id")
        @Expose
        private String paymentId;
        @SerializedName("payment_mode")
        @Expose
        private String paymentMode;
        @SerializedName("fixed")
        @Expose
        private Double fixed;
        @SerializedName("distance")
        @Expose
        private Integer distance;
        @SerializedName("commision")
        @Expose
        private Double commision;
        @SerializedName("time_price")
        @Expose
        private Double timePrice;
        @SerializedName("discount")
        @Expose
        private Double discount;
        @SerializedName("tax")
        @Expose
        private Double tax;
        @SerializedName("wallet")
        @Expose
        private Double wallet;
        @SerializedName("total")
        @Expose
        private Double total;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getRequestId() {
            return requestId;
        }

        public void setRequestId(Integer requestId) {
            this.requestId = requestId;
        }

        public Object getPromocodeId() {
            return promocodeId;
        }

        public void setPromocodeId(Object promocodeId) {
            this.promocodeId = promocodeId;
        }

        public String getPaymentId() {
            return paymentId;
        }

        public void setPaymentId(String paymentId) {
            this.paymentId = paymentId;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public Double getFixed() {
            return fixed;
        }

        public void setFixed(Double fixed) {
            this.fixed = fixed;
        }

        public Integer getDistance() {
            return distance;
        }

        public void setDistance(Integer distance) {
            this.distance = distance;
        }

        public Double getCommision() {
            return commision;
        }

        public void setCommision(Double commision) {
            this.commision = commision;
        }

        public Double getTimePrice() {
            return timePrice;
        }

        public void setTimePrice(Double timePrice) {
            this.timePrice = timePrice;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

        public Double getWallet() {
            return wallet;
        }

        public void setWallet(Double wallet) {
            this.wallet = wallet;
        }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }
    }

    public class Provider_Service {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("provider_id")
        @Expose
        private Integer providerId;
        @SerializedName("service_type_id")
        @Expose
        private Integer serviceTypeId;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("service_number")
        @Expose
        private Object serviceNumber;
        @SerializedName("service_model")
        @Expose
        private Object serviceModel;
        @SerializedName("provider_price")
        @Expose
        private Integer providerPrice;
        @SerializedName("slot_time")
        @Expose
        private Integer slotTime;
        @SerializedName("service_type")
        @Expose
        private Service_Type service_type;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getProviderId() {
            return providerId;
        }

        public void setProviderId(Integer providerId) {
            this.providerId = providerId;
        }

        public Integer getServiceTypeId() {
            return serviceTypeId;
        }

        public void setServiceTypeId(Integer serviceTypeId) {
            this.serviceTypeId = serviceTypeId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Object getServiceNumber() {
            return serviceNumber;
        }

        public void setServiceNumber(Object serviceNumber) {
            this.serviceNumber = serviceNumber;
        }

        public Object getServiceModel() {
            return serviceModel;
        }

        public void setServiceModel(Object serviceModel) {
            this.serviceModel = serviceModel;
        }

        public Integer getProviderPrice() {
            return providerPrice;
        }

        public void setProviderPrice(Integer providerPrice) {
            this.providerPrice = providerPrice;
        }

        public Integer getSlotTime() {
            return slotTime;
        }

        public void setSlotTime(Integer slotTime) {
            this.slotTime = slotTime;
        }

        public Service_Type getService_type() {
            return service_type;
        }

        public void setService_type(Service_Type service_type) {
            this.service_type = service_type;
        }
    }

    public class Service_Type {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("provider_name")
        @Expose
        private String providerName;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("fixed")
        @Expose
        private Double fixed;
        @SerializedName("price")
        @Expose
        private Double price;
        @SerializedName("description")
        @Expose
        private Object description;
        @SerializedName("status")
        @Expose
        private Integer status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProviderName() {
            return providerName;
        }

        public void setProviderName(String providerName) {
            this.providerName = providerName;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Double getFixed() {
            return fixed;
        }

        public void setFixed(Double fixed) {
            this.fixed = fixed;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public Object getDescription() {
            return description;
        }

        public void setDescription(Object description) {
            this.description = description;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

    }
}
