package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.model.dto.response.SpecialityList;
import com.caafisom.android.user.presenter.SpecialityListPresenter;
import com.caafisom.android.user.view.adapter.SpecialityListRecyclerAdapter;

/**
 * Created by Tranxit Technologies.
 */

public interface ISpecialityListView extends IView<SpecialityListPresenter> {
    void setAdapter(SpecialityListRecyclerAdapter adapter);
    void getDoctorsList(SpecialityList data);
}
