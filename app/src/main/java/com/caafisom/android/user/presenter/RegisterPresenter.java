package com.caafisom.android.user.presenter;

import android.os.Bundle;
import androidx.annotation.NonNull;

import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.LoginModel;
import com.caafisom.android.user.model.RegisterModel;
import com.caafisom.android.user.model.dto.request.LoginRequest;
import com.caafisom.android.user.model.dto.request.RegisterRequest;
import com.caafisom.android.user.model.dto.response.LoginResponse;
import com.caafisom.android.user.model.dto.response.RegisterResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.IRegisterPresenter;
import com.caafisom.android.user.view.iview.IRegisterView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;


public class RegisterPresenter extends BasePresenter<IRegisterView> implements IRegisterPresenter {

    @Override
    public void goToLogin() {
        iView.goToLogin();
    }

    public RegisterPresenter(IRegisterView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }


    @Override
    public void postRegister(RegisterRequest registerRequest) {
        iView.showProgressbar();
        new RegisterModel(new IModelListener<RegisterResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull RegisterResponse response) {
                LoginRequest request = new LoginRequest();
                request.setUsername(registerRequest.getEmail());
                request.setPassword(registerRequest.getPassword());
                request.setDevice_id(registerRequest.getDevice_id());
                request.setDevice_token(registerRequest.getDevice_token());
                request.setDevice_type(Constants.WebConstants.DEVICE_TYPE);
                request.setGrant_type(Constants.WebConstants.GRANT_TYPE);
                request.setClient_id(Constants.WebConstants.CLINET_ID);
                request.setClient_secret(Constants.WebConstants.CLIENT_SCERET);
                request.setLogin_type("manual");
                postLogin(request);
            }

            @Override
            public void onSuccessfulApi(@NotNull List<RegisterResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).postRegister(registerRequest);
    }

    @Override
    public void postLogin(LoginRequest request) {
        new LoginModel(new IModelListener<LoginResponse>() {
            @Override
            public void onSuccessfulApi(@NonNull LoginResponse response) {
                iView.dismissProgressbar();
                getApplicationInstance().setAccessToken(response.getAccessToken());
                iView.goToHome();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<LoginResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).postLogin(request);
    }
}
