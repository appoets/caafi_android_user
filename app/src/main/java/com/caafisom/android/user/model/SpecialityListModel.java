package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.response.SpecialityList;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class SpecialityListModel extends BaseModel<SpecialityList> {


    public SpecialityListModel(IModelListener<SpecialityList> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(SpecialityList response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<SpecialityList> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getSpecialityList() {
        enQueueListTask(new ApiClient().getClient().create(ApiInterface.class).getSpecialityList());
    }
}
