package com.caafisom.android.user.view.activity;

import android.app.FragmentManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.caafisom.android.user.view.fragment.SpecialistFragment;
import com.caafisom.android.user.view.fragment.SpecialityFragment;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.caafisom.android.user.R;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.model.dto.response.ProfileResponse;
import com.caafisom.android.user.presenter.HomePresenter;
import com.caafisom.android.user.presenter.ipresenter.IHomePresenter;
import com.caafisom.android.user.view.fragment.GoldenMinutesDialog;
import com.caafisom.android.user.view.fragment.HomeFragment;
import com.caafisom.android.user.view.iview.IHomeView;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends BaseActivity<IHomePresenter> implements IHomeView,
        NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.ivNotification)
    ImageView ivNotification;
    @BindView(R.id.tvNotificationCount)
    TextView tvNotificationCount;

    TextView tvName;
    CircleImageView civProfile;
    RelativeLayout rlHeaderMain;
    public static AHBottomNavigation bottomNavigation;
    private Fragment fragment;
    private androidx.fragment.app.FragmentManager fragmentManager;
    FragmentTransaction transaction;


    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected int attachLayout() {
        return R.layout.activity_home;
    }

    @Override
    IHomePresenter initialize() {
        return new HomePresenter(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        iPresenter.getUserDetails();
        iPresenter.updateDeviceToken();
        iPresenter.getNotificationCount();
    }

    @Override
    public void setUp() {
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        tvName = header.findViewById(R.id.tvName);
        civProfile = header.findViewById(R.id.civProfile);
        rlHeaderMain = header.findViewById(R.id.rlHeaderMain);

        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();

        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

        // Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem("", R.drawable.ic_menu_icon_1, R.color.colorBlack);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("", R.drawable.ic_baseline_home_24, R.color.colorBlack);
//        AHBottomNavigationItem item3 = new AHBottomNavigationItem("", R.drawable.ic_menu_icon_2, R.color.colorBlack);

        // Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
//        bottomNavigation.addItem(item3);

        // Disable the translation inside the CoordinatorLayout
        bottomNavigation.setBehaviorTranslationEnabled(true);
        bottomNavigation.setTranslucentNavigationEnabled(true);

        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);

        // Set listeners
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                // Do something cool here...
                switch (position) {
                    case 0:
                        fragment = new SpecialityFragment();
                        break;
                    case 1:
                        fragment = new SpecialistFragment();
                        break;
                   /* case 2:
                        fragment = new HomeFragment();
                        break;*/
                }

                transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.main_container, fragment).commit();
                return true;
            }
        });


        rlHeaderMain.setOnClickListener(v -> navigateTo(ProfileActivity.class,false,new Bundle()));

       /* Intial load fragment*/
       SpecialityFragment fragment = new SpecialityFragment();
       addFragment(new Bundle(),R.id.main_container,fragment,SpecialityFragment.TAG);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fragmentManager = getFragmentManager();
            if(fragmentManager.getBackStackEntryCount()>1){
                fragmentManager.popBackStack();
            }else{
                if (doubleBackToExitPressedOnce) {
                    finish();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                showSnackBar("Please click BACK again to exit");

                new Handler().postDelayed(() -> doubleBackToExitPressedOnce=false, 2000);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            iPresenter.goToHome();
        } else if (id == R.id.nav_history) {
            iPresenter.goToHistory();
        } else if (id == R.id.nav_schedule) {
            iPresenter.goToSchedule();
        } else if (id == R.id.nav_videocall) {
            iPresenter.goToVideoCall();
        } else if (id == R.id.nav_help) {
            iPresenter.goToHelp();
        } else if (id == R.id.nav_wallet) {
            iPresenter.goToWallet();
        }else if (id == R.id.nav_logout) {
            showSnackBar("Logout clicked");
            iPresenter.onLogout();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void updateUserDetails(ProfileResponse response) {
        if (response!=null) {
            tvName.setText(response.getFirstName());
            CodeSnippet.loadImageCircle(CodeSnippet.getImageURL(response.getPicture()),civProfile,R.drawable.ic_dummy_user);
        }
    }

    @Override
    public void goToHome() {
        SpecialityFragment fragment = new SpecialityFragment();
        changeFragment(new Bundle(),R.id.main_container,fragment,SpecialityFragment.TAG);
    }

    @Override
    public void goToHistory() {
        navigateTo(HistoryActivity.class,false,new Bundle());
    }

    @Override
    public void goToSchedule() {
        navigateTo(ScheduleActivity.class,false,new Bundle());
    }

    @Override
    public void goToVideoCall() {
        navigateTo(VideoCallActivity.class,false,new Bundle());
    }

    @Override
    public void goToHelp() {
        navigateTo(HelpActivity.class,false,new Bundle());
    }

    @Override
    public void goToWallet() {
        navigateTo(WalletActivity.class,false,new Bundle());
    }


    @OnClick({R.id.ivNotification,R.id.ivGolderMins})
    public void submit(View view) {
       switch (view.getId()){
           case R.id.ivNotification:
               navigateTo(NotificationActivity.class,false,new Bundle());
               break;

           case R.id.ivGolderMins:
               iPresenter.showGoldenMins();
               break;
       }
    }


    @Override
    public void showGoldenMins() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        GoldenMinutesDialog.newInstance().show(getSupportFragmentManager(),"Dialog");
    }

    @Override
    public void updateNotification(int count) {
        if (count > 0) {
            if (count >99)
                tvNotificationCount.setText("99+");
            else
                tvNotificationCount.setText(count + "");

            tvNotificationCount.setVisibility(View.VISIBLE);
            playAnimation();
        } else {
            tvNotificationCount.setVisibility(View.GONE);
        }
    }

    private void playAnimation() {
        YoYo.with(Techniques.Tada)
                .duration(700)
                .repeat(3)
                .playOn(ivNotification);
    }
}
