package com.caafisom.android.user.presenter.ipresenter;

public interface IVideoCallPresenter extends IPresenter {
    void getVideoCallList();
}
