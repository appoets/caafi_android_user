package com.caafisom.android.user.view.adapter.viewholder;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.DoctorsSpecialityList;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.adapter.listener.IDoctorsSpecialityListRecyclerAdapter;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tranxit Technologies.
 */

public class DoctorSpecialityViewHolder extends BaseViewHolder<DoctorsSpecialityList, IDoctorsSpecialityListRecyclerAdapter> {

    @BindView(R.id.tvRating)
    TextView tvRating;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvSpl)
    TextView tvSpl;
    @BindView(R.id.tvAvailable)
    TextView tvAvailable;
    @BindView(R.id.tvSpeciality)
    TextView tvSpeciality;
    @BindView(R.id.tvExp)
    TextView tvExp;
    @BindView(R.id.ivLocation)
    TextView ivLocation;
    @BindView(R.id.tv_fee)
    TextView tvfee;
    @BindView(R.id.tv_book_appointment)
    TextView tvbookappointment;
    @BindView(R.id.civProfile)
    CircleImageView civProfile;

    public DoctorSpecialityViewHolder(View itemView, IDoctorsSpecialityListRecyclerAdapter listener) {
        super(itemView, listener);
    }

    @SuppressLint({"SetTextI18n", "ResourceAsColor"})
    @Override
    void populateData(DoctorsSpecialityList data) {

        if (data != null) {
            tvName.setText(data.getFirstName() + " " + data.getLastName());
            tvSpl.setText(data.getSpeciality().getName());
            tvSpeciality.setText(data.getSpeciality().getName());
            tvExp.setText(data.getExperience() + " years of experience");

            if (data.getAvailable().size() > 0) {
                tvAvailable.setText("Available");
            }
            else {
                tvAvailable.setText("Not Available");
                tvAvailable.setBackgroundColor(R.color.colorBlack);
                tvAvailable.setTextColor(R.color.colorWhite);
            }
            tvfee.setText("$" + " " + data.getConsultationFee());
            CodeSnippet.loadImageCircle(String.valueOf(data.getPicture()), civProfile, R.drawable.ic_dummy_user);
        }
    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }
}
