package com.caafisom.android.user.presenter.ipresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface ICityListPresenter extends IPresenter {
    void getCityList();
}
