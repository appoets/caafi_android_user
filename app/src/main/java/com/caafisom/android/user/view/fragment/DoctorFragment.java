package com.caafisom.android.user.view.fragment;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.response.SendRequestResponse;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;
import com.caafisom.android.user.presenter.DoctorFlowPresenter;
import com.caafisom.android.user.view.activity.DoctorDetailViewActivity;
import com.caafisom.android.user.view.activity.twilio.TwilloVideoActivity;
import com.caafisom.android.user.view.adapter.ProviderRecyclerAdapter;
import com.caafisom.android.user.view.iview.IDoctorFlowView;
import com.caafisom.android.user.view.widget.CustomProgressbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;

/**
 * A simple {@link Fragment} subclass.
 */
public class DoctorFragment extends BaseFragment<DoctorFlowPresenter> implements IDoctorFlowView {

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.rvDoctors)
    RecyclerView rvDoctors;
    @BindView(R.id.llPlaceHolder)
    LinearLayout llPlaceHolder;
    @BindView(R.id.fabSearch)
    FloatingActionButton fabSearch;

    Provider data;
    AlertDialog alertDialog;
    public static String currentRequestID;

    @Override
    protected DoctorFlowPresenter initialize() {
        return new DoctorFlowPresenter(this);
    }


    @Override
    protected void setUp() {
        super.setUp();
        iPresenter.getProviderList();

        etSearch.setOnEditorActionListener((v, keyCode, event) -> {
            if (keyCode == EditorInfo.IME_ACTION_DONE) {
                fabSearch.performClick();
            }
            return true;
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                getProviderDetails();
            }
        });
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_doctor_specialist;
    }

    @OnClick({R.id.fabMap, R.id.fabSearch})
    public void OnClickView(View view) {
        switch (view.getId()) {
            case R.id.fabMap:
                LocalBroadcastManager.getInstance(getApplicationInstance()).sendBroadcast(new Intent(Constants.BROADCAST.SWITCH_VIEW));
                break;

            case R.id.fabSearch:
                getProviderDetails();
                break;
        }
    }

    private void getProviderDetails() {
        String searchKey = etSearch.getText().toString().trim();
        if (searchKey.isEmpty()) {
            iPresenter.getProviderList();
        } else {
            iPresenter.searchProvider(searchKey);
        }
    }

    @Override
    public void setAdapter(ProviderRecyclerAdapter adapter) {
        rvDoctors.setHasFixedSize(true);
        rvDoctors.setLayoutManager(new LinearLayoutManager(DoctorFragment.this.getContext()));
        if (adapter.getItemCount() > 0) {
            llPlaceHolder.setVisibility(View.GONE);
            rvDoctors.setVisibility(View.VISIBLE);
            rvDoctors.setAdapter(adapter);
        } else {
            rvDoctors.setVisibility(View.GONE);
            llPlaceHolder.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showVideoAlert(Provider data) {

        this.data = data;

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.custom_video_alert, null);
        Button noBtn = view.findViewById(R.id.btn_no);
        Button yesBtn = view.findViewById(R.id.btn_yes);
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.setOnShowListener(arg -> {
            alertDialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getContext(), R.color.colorOrange));
            alertDialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(getContext(), R.color.colorOrange));
        });
        alertDialog.show();
        yesBtn.setOnClickListener(v -> {
            //Make a video call here


            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            String selectedDate = dateFormat.format(Calendar.getInstance().getTime());
            Log.d("selectedDate", "showVideoAlert: " + selectedDate);


            Date date = null;
            try {
                date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm");
            String displayTime = timeFormatter.format(date);

            Log.d("displayTime", "showVideoAlert: " + displayTime);

           /* iPresenter.makeVideoCall(data);
            alertDialog.dismiss();*/

            for (int i = 0; i < data.getService().size(); i++) {
                sendRequestVideoCall(data.getService().get(i).getServiceTypeId(), data.getId(), selectedDate, displayTime, "1");
            }


        });
        noBtn.setOnClickListener(v ->
        {
            alertDialog.dismiss();

        });

    }

    @Override
    public void makeVideoCall(Provider data) {
        int userid = getApplicationInstance().getUserID();
        String chatPath = getCodeSnippet().getRoomID();


        Bundle bundle = new Bundle();
        bundle.putString("request_id", currentRequestID);
        bundle.putInt("sender", data.getId());
        bundle.putString("sender_name", data.getFirstName());
        bundle.putString("sender_avatar", data.getAvatar());
        if (data.getDevice() != null)
            bundle.putString("device_token", data.getDevice().getToken());
        //  bundle.putString("sender_about", senderStatus);
        bundle.putString("sender_mobile", data.getMobile());
        bundle.putString("chat_path", chatPath);
        bundle.putString("call_type", "video_call");
        bundle.putString("provider_id", data.getId() + "");

        for (int i = 0; i < data.getService().size(); i++) {
            bundle.putString("service_type", data.getService().get(i).getServiceTypeId() + "");
        }

        //navigateTo(MakeCallActivity.class,false,bundle);
        navigateTo(TwilloVideoActivity.class, false, bundle);
    }

    @Override
    public void moveToDetailView(Provider data) {
        Bundle extras = new Bundle();
        extras.putParcelable("data", data);
        navigateTo(DoctorDetailViewActivity.class, false, extras);
        //navigateTo(InvoiceActivity.class,false,new Bundle());
    }

    @Override
    public void makeVideoCallPopUP() {


    }


    private void sendRequestVideoCall(Integer serviceTypeId, Integer providerid, String selectedDate, String displayTime, String broadcast) {

        CustomProgressbar mCustomProgressbar = new CustomProgressbar(getContext());
        mCustomProgressbar.setCancelable(false);
        mCustomProgressbar.show();

        Call<SendRequestResponse> call = new ApiClient().getClient().create(ApiInterface.class).sendRequestVideoCall(serviceTypeId, providerid + "", selectedDate, displayTime, broadcast,"1");
        call.enqueue(new Callback<SendRequestResponse>() {
            @Override
            public void onResponse(@NonNull Call<SendRequestResponse> call, @NonNull retrofit2.Response<SendRequestResponse> response) {
                mCustomProgressbar.dismiss();
                if (response.isSuccessful()) {
                    currentRequestID = response.body().getRequest_id();
                    iPresenter.makeVideoCall(data);
                    alertDialog.dismiss();

                } else {

                    if (response.code() == 500) {
                        alertDialog.dismiss();

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(getContext(), jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }  else if(response.code() == 422){
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(getContext(), jObjError.getString("error"), Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }


                    }
            }

            @Override
            public void onFailure(@NonNull Call<SendRequestResponse> call, @NonNull Throwable t) {
                Timber.e("Retrofit NetworkError" + t.getMessage());
                mCustomProgressbar.dismiss();
                alertDialog.dismiss();

            }
        });

    }


}

