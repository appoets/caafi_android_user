package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.presenter.ipresenter.ILoginPresenter;

public interface ILoginView extends IView<ILoginPresenter> {
    void goToRegistration();
    void goToForgotPassword();
    void goToHome();
}
