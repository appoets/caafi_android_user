package com.caafisom.android.user.model.dto.common;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Service implements Parcelable{

	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("provider_id")
	@Expose
	private Integer providerId;
	@SerializedName("service_type_id")
	@Expose
	private Integer serviceTypeId;
	@SerializedName("status")
	@Expose
	private String status;
	@SerializedName("service_number")
	@Expose
	private String serviceNumber;
	@SerializedName("service_model")
	@Expose
	private String serviceModel;
	@SerializedName("provider_price")
	@Expose
	private Integer providerPrice;
	@SerializedName("service_type")
	@Expose
	private ServiceType serviceType;

	protected Service(Parcel in) {
		id = in.readByte() == 0x00 ? null : in.readInt();
		providerId = in.readByte() == 0x00 ? null : in.readInt();
		serviceTypeId = in.readByte() == 0x00 ? null : in.readInt();
		status = in.readString();
		serviceNumber = in.readString();
		serviceModel = in.readString();
		providerPrice = in.readByte() == 0x00 ? null : in.readInt();
		serviceType = (ServiceType) in.readValue(ServiceType.class.getClassLoader());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		if (id == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeInt(id);
		}
		if (providerId == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeInt(providerId);
		}
		if (serviceTypeId == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeInt(serviceTypeId);
		}
		dest.writeString(status);
		dest.writeString(serviceNumber);
		dest.writeString(serviceModel);
		if (providerPrice == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeInt(providerPrice);
		}
		dest.writeValue(serviceType);
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<Service> CREATOR = new Parcelable.Creator<Service>() {
		@Override
		public Service createFromParcel(Parcel in) {
			return new Service(in);
		}

		@Override
		public Service[] newArray(int size) {
			return new Service[size];
		}
	};

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProviderId() {
		return providerId;
	}

	public void setProviderId(Integer providerId) {
		this.providerId = providerId;
	}

	public Integer getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(Integer serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public String getServiceModel() {
		return serviceModel;
	}

	public void setServiceModel(String serviceModel) {
		this.serviceModel = serviceModel;
	}

	public Integer getProviderPrice() {
		return providerPrice;
	}

	public void setProviderPrice(Integer providerPrice) {
		this.providerPrice = providerPrice;
	}

	public ServiceType getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}

}