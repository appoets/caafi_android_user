package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.common.Services;
import com.caafisom.android.user.presenter.SpecialistFragmentPresenter;
import com.caafisom.android.user.view.adapter.ProviderRecyclerAdapter;
import com.caafisom.android.user.view.adapter.ServiceRecyclerAdapter;

/**
 * Created by Tranxit Technologies.
 */

public interface ISpecialistView extends IView<SpecialistFragmentPresenter> {
    void setAdapter(ServiceRecyclerAdapter adapter);
    void showVideoAlert(Provider data);
    void makeVideoCall(Provider data);
    void moveToDetailView(Provider data);
    void startCheckStatus(String request_id);
    void gotoProvider(Integer id);
}
