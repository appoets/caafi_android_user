package com.caafisom.android.user.common;

import java.text.DecimalFormat;

import static com.caafisom.android.user.BuildConfig.BASE_URL;
import static com.caafisom.android.user.MyApplication.getApplicationInstance;

public interface Constants {

    interface URL {
        String FCM_URL="https://fcm.googleapis.com/";
        String BASE_URL_STORAGE = BASE_URL+"storage/" ;
    }

    interface BuildFlavour {
        String DEVELOPMENT = "development";
        String PRE_STAGING = "pre_staging";
        String STAGING = "staging";
    }

    interface InternalHttpCode {
        int SUCCESS_CODE = 200;
        int BAD_REQUEST = 400;
        int UNAUTHORIZED_ACCESS = 401;
        int FORBIDDEN = 403;
        int NOT_FOUND = 404;
        int METHOD_NOT_ALLOWED = 405;
        int INTERNAL_SERVER_ERROR = 500;
        int NOT_IMPLEMENTED = 501;
    }

    interface SharedPref {
        String SHARED_PREF = "SHARED_PREF_MCDONALDS";
        String PREF_TOKEN_TYPE = "PREF_TOKEN_TYPE";
        String PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN";
        String PREF_CURRENCY = "PREF_CURRENCY";
        String PREF_USER_ID = "PREF_USER_ID";
        String PREF_USER_NAME = "PREF_USER_NAME";
        String PREF_USER_IMAGE = "PREF_USER_IMAGE";
        String PREF_USER_MOBILE = "PREF_USER_MOBILE";
        String PREF_FCM_TOKEN= "PREF_FCM_TOKEN";
        String PREF_DEVICE_ID= "PREF_DEVICE_ID";
        String PREF_OTP= "PREF_OTP";
        String PREF_START_TIME = "PREF_START_TIME";
        String PREF_END_TIME = "PREF_END_TIME";
        String PREF_SENDER_DEVICE_TOKEN = "PREF_SENDER_DEVICE_TOKEN";
        String PREF_PROVIDER_ID = "PREF_PROVIDER_ID";
        String PREF_SERVICE_TYPE = "PREF_SERVICE_TYPE";
        String PREF_DOCTOR_NAME= "PREF_DOCTOR_NAME";
    }

    interface HttpErrorMessage {
        String INTERNAL_SERVER_ERROR = "Our server is under maintenance. We will resolve shortly!";
        String METHOD_NOT_ALLOWED_ERROR = "Operational Error!!";
        String UNAUTHORIZED_ACCESS_ERROR = "Your account information was incorrect!";
        String NO_NETWORK_FOUND = "No Network Found";
        String SETTINGS = "Settings";
    }


    interface DecimalValue {
        DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#0.00");
    }

    interface Common {
        String CARD = "card";
    }


    interface Header {
        String HEADER_CONTENT_NAME = "X-Requested-With";
        String HEADER_CONTENT_VALUE = "XMLHttpRequest";
        String HEADER_AUTHORIZATION = "Authorization";
    }

    interface TimeFormats{
        String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";
    }

    interface WebConstants{
        String DEVICE_TYPE = "android";
        String DEVICE_TOKEN =  getApplicationInstance().getFCMToken();
        String DEVICE_ID= getApplicationInstance().getDeviceId();
        String GRANT_TYPE = "password";
        String CLINET_ID = "2";
        String CLIENT_SCERET = "qAanmVfUllNLeWcgDn7NkThEHrJvVF0YC4AZhpwn";
        String LOGIN_BY = "manual";
    }

    interface DoctorStatus{
        String ONLINE = "active";
        String OFFLINE = "offline";
    }

    interface Requests{
        int REQUEST_TAKE_PHOTO = 1;
        int PICK_IMAGE_REQUEST = 2;
        int REQUEST_VIDEO_CAPTURE = 3;
        int ASK_MULTIPLE_PERMISSION_REQUEST_CODE = 101;
        int REQUEST_LOCATION = 102;
        int REQUEST_IMAGE = 103;
        int REQUEST_PERMISSION = 104;
    }


    interface StoragePath{
        String WEB_PATH = BASE_URL +"/storage/";
    }

    interface BROADCAST{
        String SWITCH_VIEW = "switch_view";
    }



    interface NotificationConstants{
        String PUSH_NOTIFICATION = "pushNotification";
        int NOTIFICATION_ID = 100;
        int NOTIFICATION_ID_BIG_IMAGE = 101;
    }
}
