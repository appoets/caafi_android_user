package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.request.WalletRequest;
import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.caafisom.android.user.model.dto.response.WalletResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class WalletModel extends BaseModel<BaseResponse> {

    public WalletModel(IModelListener<BaseResponse> listener) {
        super(listener);
    }

    public void addMoneyToWallet(String amount, String payment) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).addMoney(amount,payment));
    }

    @Override
    public void onSuccessfulApi(BaseResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<BaseResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
