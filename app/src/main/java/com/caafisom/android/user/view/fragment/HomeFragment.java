package com.caafisom.android.user.view.fragment;


import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.presenter.HomeFragmentPresenter;
import com.caafisom.android.user.view.activity.DoctorDetailViewActivity;
import com.caafisom.android.user.view.iview.IHomeFragementView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment<HomeFragmentPresenter> implements LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, IHomeFragementView {

    public static final String TAG = "HomeFragment";
    private static final int REQUEST_LOCATION = 1450;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;
    @BindView(R.id.cv_switch)
    CardView cvSwitch;
    @BindView(R.id.tv_doctor)
    TextView tvDoctor;
    @BindView(R.id.tv_specialist)
    TextView tvSpecialist;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.rl_list)
    RelativeLayout rlDoctorList;
    @BindView(R.id.rl_map)
    RelativeLayout rlMap;
    @BindView(R.id.fabList)
    FloatingActionButton fabList;

    /*Map*/
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap googleMap;
    private Marker mCtLocationMarker;

    private Location mCurrentLocation;
    private List<Provider> mProviderList;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rlMap.setVisibility(View.VISIBLE);
            rlDoctorList.setVisibility(View.GONE);
        }
    };

    @Override
    protected HomeFragmentPresenter initialize() {
        return new HomeFragmentPresenter(this);
    }

    @Override
    public void setUp() {
        super.setUp();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        setupViewPager(viewPager);
        prepareLocation();
        //fabList.performClick();
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_home;
    }

    @OnClick({R.id.tv_doctor, R.id.tv_specialist, R.id.fabList, R.id.fabCurrentLocation})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_doctor:
                tvDoctor.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.status_button_bg_dark));
                tvSpecialist.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.status_button_bg));
                viewPager.setCurrentItem(0);
                break;
            case R.id.tv_specialist:
                tvDoctor.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.status_button_bg));
                tvSpecialist.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.status_button_bg_dark));
                viewPager.setCurrentItem(1);
                break;

            case R.id.fabList:
                rlMap.setVisibility(View.GONE);
                rlDoctorList.setVisibility(View.VISIBLE);
                break;

            case R.id.fabCurrentLocation:
                if (mCurrentLocation != null) {
                    changeMapLocation(mCurrentLocation, true);
                }
                break;
        }
    }


    private void prepareLocation() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getPermissionUtils().hasPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                //Location Permission already granted
                buildGoogleApiClient();
                checkLocationEnableGPS();
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            checkLocationEnableGPS();
        }
    }

    private void checkLocationEnableGPS() {
        if (!checkLocationStatus()) {
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(result1 -> {
                final Status status = result1.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }

                        break;
                }
            });
        }
    }


    @Override
    public void setMarkers(List<Provider> providerList) {
        mProviderList = providerList;
        iPresenter.prepareMarkerData(providerList, Objects.requireNonNull(getActivity()));
    }

    @Override
    public void drawOnMap(List<MarkerOptions> markerOptionsList) {
        for (MarkerOptions markerOption : markerOptionsList) {
            googleMap.addMarker(markerOption);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(Objects.requireNonNull(getActivity()).getSupportFragmentManager());
        adapter.addFragment(new DoctorFragment());
        adapter.addFragment(new SpecialistFragment());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        tvDoctor.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.status_button_bg_dark));
                        tvSpecialist.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.status_button_bg));
                        break;
                    case 1:
                        tvDoctor.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.status_button_bg));
                        tvSpecialist.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.status_button_bg_dark));
                        break;
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).registerReceiver(mMessageReceiver,
                new IntentFilter(Constants.BROADCAST.SWITCH_VIEW));
    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        googleMap = gMap;
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);

        try {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity().getApplicationContext(), R.raw.style_json));
        } catch (Exception e) {
            e.printStackTrace();
        }

        googleMap.setOnMarkerClickListener(marker -> {
            if (marker.getSnippet() != null && !marker.getSnippet().equalsIgnoreCase("")) {
                int position = Integer.parseInt(marker.getSnippet());
                Provider selectedData = mProviderList.get(position);
                iPresenter.moveToDetailView(selectedData);
            }
            return true;
        });

        iPresenter.getProvidersList();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_LOCATION) if (data != null) {
            Timber.i("onActivityResult: %s", requestCode);
            Timber.i("onActivityResult: %s", data);
        } else {
            Timber.i("onActivityResult: %s", requestCode);
            Timber.i("onActivityResult: %s", (Object) null);
        }
    }

    public void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
            if (ActivityCompat.shouldShowRequestPermissionRationale(Objects.requireNonNull(getActivity()),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                requestPermissions(new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                requestPermissions(new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        else {
            buildGoogleApiClient();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (getPermissionUtils().hasPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        checkLocationEnableGPS();
                    }

                } else {
                    showSnackBar("Permission Denied");
                }
                break;

        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(Objects.requireNonNull(getContext()))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //showToast("Connected");
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        showToast("Connection Suspended");
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.reconnect();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        showToast("Connection Failed");
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.reconnect();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            mCurrentLocation = location;
            changeMapLocation(location, false);
        }
    }

    private void changeMapLocation(Location location, boolean isForceMove) {
        if (googleMap != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            float zoom = 15.0f;
            if (mCtLocationMarker == null) {
                mCtLocationMarker = googleMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location))
                        .anchor(0.5f, 0.5f)
                        .position(latLng));
            }
            animateMarker(mCtLocationMarker, location);

            boolean contains = googleMap.getProjection()
                    .getVisibleRegion()
                    .latLngBounds
                    .contains(latLng);

            if (!contains || isForceMove) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
            }

            if (mGoogleApiClient != null)
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    public void animateMarker(final Marker marker, final Location location) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final LatLng startLatLng = marker.getPosition();
        final double startRotation = marker.getRotation();
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);

                double lng = t * location.getLongitude() + (1 - t)
                        * startLatLng.longitude;
                double lat = t * location.getLatitude() + (1 - t)
                        * startLatLng.latitude;

                float rotation = (float) (t * location.getBearing() + (1 - t)
                        * startRotation);

                marker.setPosition(new LatLng(lat, lng));
                marker.setRotation(rotation);

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null)
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add("");
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public void moveToDetailView(Provider data) {
        Bundle extras = new Bundle();
        extras.putParcelable("data", data);
        navigateTo(DoctorDetailViewActivity.class, false, extras);
    }


}
