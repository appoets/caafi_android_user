package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.response.SendRequestResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class SendRequestModel extends BaseModel<SendRequestResponse> {


    public SendRequestModel(IModelListener<SendRequestResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(SendRequestResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<SendRequestResponse> response) {

    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void sendSpecialistRequest(Integer serviceTypeId, String date, String time, String broadcast) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).sendSpecialistRequest(serviceTypeId, date, time, broadcast,"1"));
    }

    public void sendIndividualRequest(Integer serviceTypeId, String providerID, String date, String time, String broadcast) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).sendIndividualRequest(serviceTypeId, providerID, date, time, broadcast,"1"));
    }
}
