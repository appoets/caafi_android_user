package com.caafisom.android.user.presenter.ipresenter;

import com.caafisom.android.user.model.dto.common.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface ISpecialistFragmentPresenter extends IPresenter {
    void getServicesList();
    void searchService(String searchKey);
    void makeVideoCall(Provider data);
    void scheduleAppointment(Integer serviceTypeId, String date, String time, String broadcast);
}
