package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.response.Doctor;
import com.caafisom.android.user.presenter.DoctorBookingPresenter;
import com.caafisom.android.user.presenter.ipresenter.IDoctorBookingPresenter;
import com.caafisom.android.user.presenter.ipresenter.IDoctorDetailViewPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IDoctorBookingView extends IView<DoctorBookingPresenter> {
    void startCheckStatus();
    void initSetUp();
    void startCheckFailStatus();
}
