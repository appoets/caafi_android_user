package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.presenter.ipresenter.IRegisterPresenter;

public interface IRegisterView extends IView<IRegisterPresenter> {
    void goToLogin();
    void goToHome();
}
