package com.caafisom.android.user.model.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Available {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("doctor_id")
    @Expose
    private String doctor_id;
    @SerializedName("morning")
    @Expose
    private Integer morning;
    @SerializedName("afternoon")
    @Expose
    private Integer afternoon;
    @SerializedName("night")
    @Expose
    private Integer night;
    @SerializedName("onsite")
    @Expose
    private Integer onsite;
    @SerializedName("video_consultancy")
    @Expose
    private Integer video_consultancy;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("available_date")
    @Expose
    private String available_date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public Integer getMorning() {
        return morning;
    }

    public void setMorning(Integer morning) {
        this.morning = morning;
    }

    public Integer getAfternoon() {
        return afternoon;
    }

    public void setAfternoon(Integer afternoon) {
        this.afternoon = afternoon;
    }

    public Integer getNight() {
        return night;
    }

    public void setNight(Integer night) {
        this.night = night;
    }

    public Integer getOnsite() {
        return onsite;
    }

    public void setOnsite(Integer onsite) {
        this.onsite = onsite;
    }

    public Integer getVideo_consultancy() {
        return video_consultancy;
    }

    public void setVideo_consultancy(Integer video_consultancy) {
        this.video_consultancy = video_consultancy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAvailable_date() {
        return available_date;
    }

    public void setAvailable_date(String available_date) {
        this.available_date = available_date;
    }
}
