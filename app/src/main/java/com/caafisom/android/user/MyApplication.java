package com.caafisom.android.user;

import android.app.Application;
import android.content.res.AssetManager;

import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.util.PreferencesUtils;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

public class MyApplication extends Application {

    private static MyApplication mApplication;
    private PreferencesUtils preferences = new PreferencesUtils();

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = this;
        FirebaseApp.initializeApp(this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

    public static MyApplication getApplicationInstance() {
        return mApplication;
    }


    public void setAccessToken(String s) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_ACCESS_TOKEN, s);
    }

    public String getAccessToken() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_ACCESS_TOKEN);
    }

    public void setCurrency(String s) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_CURRENCY, s);
    }

    public String getCurrency() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_CURRENCY);
    }

    public void setUserID(int user_id) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_USER_ID, user_id);
    }

    public int getUserID() {
        return preferences.getIntValue(mApplication, Constants.SharedPref.PREF_USER_ID, 0);
    }

    public void setUserName(String s) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_USER_NAME, s);
    }

    public String getUserName() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_USER_NAME);
    }


    public void setUserImage(String s) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_USER_IMAGE, s);
    }

    public String getUserImage() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_USER_IMAGE);
    }


    public void setUserMobile(String s) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_USER_MOBILE, s);
    }

    public String getUserMobile() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_USER_MOBILE);
    }

    public void setFCMToken(String s) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_FCM_TOKEN, s);
    }

    public String getFCMToken() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_FCM_TOKEN);
    }

    public void setDeviceId(String s) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_DEVICE_ID, s);
    }

    public String getDeviceId() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_DEVICE_ID);
    }

    public void setOTP(String s) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_OTP, s);
    }

    public String getOTP() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_OTP);
    }

    public void setStartTime(String time) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_START_TIME, time);
    }

    public String getStartTime() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_START_TIME, "");
    }


    public void setSenderDeviceToken(String time) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_SENDER_DEVICE_TOKEN, time);
    }

    public String getSenderDeviceToken() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_SENDER_DEVICE_TOKEN, "");
    }

    public void setProviderId(String id) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_PROVIDER_ID, id);
    }

    public String getProviderId() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_PROVIDER_ID, "");
    }

    public void setServiceType(String type_id) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_SERVICE_TYPE, type_id);
    }

    public String getServiceType() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_SERVICE_TYPE, "");
    }

    public void setDoctorName(String name) {
        preferences.setSharedPrefValue(mApplication, Constants.SharedPref.PREF_DOCTOR_NAME, name);
    }

    public String getDoctorName() {
        return preferences.getStringValue(mApplication, Constants.SharedPref.PREF_DOCTOR_NAME, "");
    }

    public void logout() {

        String Device_id = Constants.WebConstants.DEVICE_ID;
        String Device_token = Constants.WebConstants.DEVICE_TOKEN;

        preferences.clearPreference(getApplicationContext());

        getApplicationInstance().setDeviceId(Device_id);
        getApplicationInstance().setFCMToken(Device_token);
    }

    public SSLContext trustCert() throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        AssetManager assetManager = getAssets();
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        Certificate ca = cf.generateCertificate(assetManager.open("COMODORSADomainValidationSecureServerCA.crt"));

        // Create a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

        // Create a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        // Create an SSLContext that uses our TrustManager
        SSLContext context = SSLContext.getInstance("TLS");
        context.init(null, tmf.getTrustManagers(), null);
        return context;
    }
}
