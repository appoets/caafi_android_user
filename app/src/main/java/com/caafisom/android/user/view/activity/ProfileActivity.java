package com.caafisom.android.user.view.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.ProfileResponse;
import com.caafisom.android.user.presenter.ProfilePresenter;
import com.caafisom.android.user.presenter.ipresenter.IProfilePresenter;
import com.caafisom.android.user.view.iview.IProfileView;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pub.devrel.easypermissions.EasyPermissions;

import static com.caafisom.android.user.common.Constants.Requests.REQUEST_IMAGE;
import static com.caafisom.android.user.common.Constants.Requests.REQUEST_PERMISSION;
import static com.caafisom.android.user.common.Constants.StoragePath.WEB_PATH;


public class ProfileActivity extends BaseActivity<IProfilePresenter> implements IProfileView,EasyPermissions.PermissionCallbacks {



    @BindView(R.id.etFirstName)
    EditText etFirstName;
    @BindView(R.id.etLastName)
    EditText etLastName;
    @BindView(R.id.etMobileNumber)
    EditText etMobileNumber;
    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.civProfile)
    CircleImageView civProfile;

    @BindView(R.id.tvSaveEdit)
    TextView tvSaveEdit;
    @BindView(R.id.tvChangePassword)
    TextView tvChangePassword;


    File profileImageFile;

    @Override
    protected int attachLayout() {
        return R.layout.activity_profile;
    }

    @Override
    IProfilePresenter initialize() {
        return new ProfilePresenter(this);
    }

    @OnClick({R.id.ivBack,R.id.tvSaveEdit,R.id.civProfile,R.id.tvChangePassword})
    public void onViewClicked(View view){
        switch (view.getId()){
            case R.id.ivBack:
                onBackPressed();
                break;

            case R.id.tvSaveEdit:
                if (!etFirstName.isEnabled()) {
                    tvSaveEdit.setText(R.string.save);
                    etFirstName.setEnabled(true);
                    etLastName.setEnabled(true);
                    etMobileNumber.setEnabled(true);
                    etEmail.setEnabled(true);

                    etFirstName.requestFocus();
                } else {
                    tvSaveEdit.setText(R.string.edit);
                    etFirstName.setEnabled(false);
                    etLastName.setEnabled(false);
                    etMobileNumber.setEnabled(false);
                    etEmail.setEnabled(false);
                    updateProfile();
                }
                break;

            case R.id.civProfile:
                if (etFirstName.isEnabled()) {
                    checkStoragePermission();
                }
                break;

            case R.id.tvChangePassword:
               navigateTo(ChangePasswordActivity.class,false,new Bundle());
                break;
        }
    }

    private void updateProfile() {
        String firstname = etFirstName.getText().toString().trim();
        String lastname = etLastName.getText().toString().trim();
        String phonenumber = etMobileNumber.getText().toString().trim();
        String email = etEmail.getText().toString().trim();

        if (TextUtils.isEmpty(firstname)){
            showSnackBar(getString(R.string.please_enter_name));
        }else if(TextUtils.isEmpty(lastname)){
            showSnackBar(getString(R.string.please_enter_last_name));
        }else if(TextUtils.isEmpty(phonenumber)){
            showSnackBar(getString(R.string.please_enter_mobile_number));
        }else if(TextUtils.isEmpty(email)){
            showSnackBar(getString(R.string.please_enter_email));
        }else{
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("first_name", RequestBody.create(MediaType.parse("text/plain"), firstname));
            map.put("last_name", RequestBody.create(MediaType.parse("text/plain"), lastname));
            map.put("mobile", RequestBody.create(MediaType.parse("text/plain"), phonenumber));
            map.put("email", RequestBody.create(MediaType.parse("text/plain"), email));

            MultipartBody.Part filePart1 = null;
            if(profileImageFile!=null && profileImageFile.exists()){
                filePart1 = MultipartBody.Part.createFormData("picture", profileImageFile.getName(), RequestBody.create(MediaType.parse("image/*"), profileImageFile));
            }

            iPresenter.updateProfile(map,filePart1);
        }
    }

    private void checkStoragePermission() {
        String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            EasyImage.openChooserWithDocuments(ProfileActivity.this, "Select", REQUEST_IMAGE);
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.image_selection_rationale),
                    REQUEST_PERMISSION, perms);
        }
    }


    @Override
    public void updateUserDetails(ProfileResponse response) {
        if (response!=null) {
            etFirstName.setText(response.getFirstName());
            etLastName.setText(response.getLastName());
            String image_url = WEB_PATH+ response.getPicture();
            Glide.with(ProfileActivity.this).load(image_url).apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user)).into(civProfile);
            etMobileNumber.setText(response.getMobile());
            etEmail.setText(response.getEmail());
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        EasyImage.openChooserWithDocuments(ProfileActivity.this, "Select", REQUEST_IMAGE);
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                if(type == REQUEST_IMAGE){
                    profileImageFile = imageFiles.get(0);
                    Glide.with(ProfileActivity.this)
                            .load(profileImageFile)
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.ic_dummy_user)
                                    .error(R.drawable.ic_dummy_user).dontAnimate())
                            .into(civProfile);
                }
            }
        });
    }
}
