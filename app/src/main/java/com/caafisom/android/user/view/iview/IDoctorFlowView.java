package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.presenter.DoctorFlowPresenter;
import com.caafisom.android.user.view.adapter.ProviderRecyclerAdapter;

/**
 * Created by Tranxit Technologies.
 */

public interface IDoctorFlowView extends IView<DoctorFlowPresenter> {
    void setAdapter(ProviderRecyclerAdapter adapter);
    void showVideoAlert(Provider data);
    void makeVideoCall(Provider data);
    void moveToDetailView(Provider data);
    void makeVideoCallPopUP();
}
