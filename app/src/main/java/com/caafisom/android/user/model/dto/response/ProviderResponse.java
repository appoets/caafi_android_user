package com.caafisom.android.user.model.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.caafisom.android.user.model.dto.common.Card;
import com.caafisom.android.user.model.dto.common.Invoice;
import com.caafisom.android.user.model.dto.common.Provider;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class ProviderResponse extends BaseResponse {
    @SerializedName("provider")
    @Expose
    private List<Provider> provider;
    @SerializedName("card")
    @Expose
    private Card card;
    @SerializedName("invoice")
    @Expose
    private Invoice invoice;

    public List<Provider> getProvider() {
        return provider;
    }

    public void setProvider(List<Provider> provider) {
        this.provider = provider;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
