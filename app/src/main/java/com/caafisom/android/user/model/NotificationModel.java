package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.response.NotificationResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class NotificationModel extends BaseModel<NotificationResponse>{

    public NotificationModel(IModelListener<NotificationResponse> listener) {
        super(listener);
    }


    @Override
    public void onSuccessfulApi(List<NotificationResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(NotificationResponse response) {

    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getNotificationDetail() {
        enQueueListTask(new ApiClient().getClient().create(ApiInterface.class).getNotification());
    }
}
