package com.caafisom.android.user.view.adapter.listener;

import com.caafisom.android.user.model.dto.response.VideoCallResponse;

public interface IVideoCallListener extends BaseRecyclerListener<VideoCallResponse> {
}
