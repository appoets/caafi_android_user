package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.presenter.ipresenter.IForgotPasswordPresenter;

public interface IForgotPasswordView extends IView<IForgotPasswordPresenter> {
        void goToOneTimePassword();
}
