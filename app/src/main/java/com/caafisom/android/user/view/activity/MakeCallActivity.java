package com.caafisom.android.user.view.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.dto.common.videocalldata.Data;
import com.caafisom.android.user.model.dto.common.videocalldata.Fcm;
import com.caafisom.android.user.model.dto.common.videocalldata.FcmNotification;
import com.caafisom.android.user.model.dto.request.MissedCallRequest;
import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;
import com.caafisom.android.user.presenter.MakeCallPresenter;
import com.caafisom.android.user.presenter.ipresenter.IMakeCallPresenter;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.videocall.VideoChatActivity;
import com.caafisom.android.user.view.iview.IMakeCallView;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;
import static timber.log.Timber.v;

/*https://github.com/julianshen/apprtc-android-demo*/

public class MakeCallActivity extends BaseActivity<IMakeCallPresenter> implements IMakeCallView {

    Activity thisActivity;
    @BindView(R.id.lblName)
    TextView lblName;
    @BindView(R.id.imgProfile)
    CircleImageView imgProfile;
    @BindView(R.id.imgEndCall)
    ImageView imgEndCall;

    Integer sender = -1;
    String senderName, deviceToken, senderStatus, senderMobile, call_type,provider_id,service_type;
    String senderAvatar = "", chatPath = "";

    BroadcastReceiver mReceiver_Accept;
    IntentFilter mIntentFilter_Accept;

    BroadcastReceiver mReceiver_Reject;
    IntentFilter mIntentFilter_Reject;

    private SharedPreferences sharedPref;
    String TAG = "MakeCallActivity";
    @BindView(R.id.lblCallType)
    TextView lblCallType;

    @Override
    protected int attachLayout() {
        return R.layout.activity_make_call;
    }

    @Override
    IMakeCallPresenter initialize() {
        return new MakeCallPresenter(this);
    }

    @Override
    public void setUp() {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }


        sender = extras.getInt("sender");
        senderName = extras.getString("sender_name");
        senderAvatar = extras.getString("sender_avatar");
        deviceToken = extras.getString("device_token");
        senderStatus = extras.getString("sender_about");
        senderMobile = extras.getString("sender_mobile");
        chatPath = extras.getString("chat_path");
        call_type = extras.getString("call_type");
        provider_id = extras.getString("provider_id");
        service_type = extras.getString("service_type");

        getApplicationInstance().setSenderDeviceToken(deviceToken);
        getApplicationInstance().setProviderId(provider_id);
        getApplicationInstance().setServiceType(service_type);


        lblName.setText(senderName);
        lblCallType.setText(R.string.ztomed_video_call);

        if (senderAvatar != null && !senderAvatar.equalsIgnoreCase("") && senderAvatar.length() > 0) {
            String imageUrl = Constants.URL.BASE_URL_STORAGE + senderAvatar;
            CodeSnippet.loadImageCircle(imageUrl, imgProfile, R.drawable.ic_dummy_user);
        }

        initCall("MAKECALL");


        mIntentFilter_Accept = new IntentFilter("attend_call");


        mReceiver_Accept = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, " onRecieve attend_call"); //do something with intent


                //attent the call

                finish();

                    /*SharedHelper.putKey(thisActivity, "call_sender_id", String.valueOf(sender));
                    SharedHelper.putKey(thisActivity, "call_sender_type", "OUTGOING");


                    SharedHelper.putKey(thisActivity, "call_sender_name", senderName);
                    SharedHelper.putKey(thisActivity, "call_sender_pic", senderAvatar);*/


                Intent i = new Intent(getApplicationContext(), VideoChatActivity.class);
                i.putExtra("chat_path", chatPath);
                startActivity(i);


            }
        };


        mIntentFilter_Reject = new IntentFilter("reject_call");


        mReceiver_Reject = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, " onRecieve reject_call"); //do something with intent
                updateMissedCall();
                finish();
                //Update API CALL OUTGOING

                //updateAPICall();

            }
        };

    }


    @OnClick({R.id.imgEndCall})
    public void onViewClick(View view){
        switch (view.getId()){
            case R.id.imgEndCall:
                initCall("CUTCALL");
                updateMissedCall();
                finish();
                break;
        }
    }


    private void initCall(String type) {


        String timeStamp = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) + "";
        //Utilities.printV("timeStamp===>", timeStamp);

//        {data={"payload":{"type":"audio"},"mobile":"+918220274902","name":"Esack","from":"make_call","avatar":"http:\/\/whatsapp.venturedemos.com\/uploads\/7de1733c7927d36f5ad1ee620965606617d91fc6.jpg"}}

        String userid = getApplicationInstance().getUserID() + "";

        String device_token = getApplicationInstance().getFCMToken();
        device_token = new CodeSnippet().encodeBase64(device_token);

        Data fcmData = new Data();
        fcmData.setId(userid);
        fcmData.setCallType(type);
        fcmData.setPatientName(senderName);
        fcmData.setAvatar(senderAvatar);
        fcmData.setMobile(senderMobile);
        fcmData.setDevice_token(device_token);
        fcmData.setName(chatPath);
        fcmData.setCall_duration("");
        fcmData.setRequest_id("");
        getApplicationInstance().setDoctorName(senderName);

        FcmNotification notification = new FcmNotification();
        notification.setTitle(senderName);
        notification.setBody(senderName+" Calling");

        Fcm fcm = new Fcm();
        fcm.setTo(deviceToken);
        fcm.setData(fcmData);
        //fcm.setFcmNotification(notification);

        iPresenter.sendFCMMessage(fcm);

    }


    @Override
    protected void onResume() {

        registerReceiver(mReceiver_Accept, mIntentFilter_Accept);
        registerReceiver(mReceiver_Reject, mIntentFilter_Reject);

        super.onResume();
    }

    @Override
    protected void onPause() {


        stopmReceiver();
        super.onPause();
    }


    void stopmReceiver() {
        if (mReceiver_Accept != null) {
            unregisterReceiver(mReceiver_Accept);
            mReceiver_Accept = null;
        }

        if (mReceiver_Reject != null) {
            unregisterReceiver(mReceiver_Reject);
            mReceiver_Reject = null;
        }
    }

    private void updateMissedCall(){
        String userid = getApplicationInstance().getUserID() + "";

        MissedCallRequest request = new MissedCallRequest();
        request.setProvider_id(provider_id);
        request.setService_type(service_type);
        request.setUser_id(userid);

        new Thread(() -> {
            Call<BaseResponse> call = new ApiClient().getClient().create(ApiInterface.class).sendMissedCall(request);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(@NonNull Call<BaseResponse> call, @NonNull Response<BaseResponse> response) {
                    Log.e("","Missed call Success");
                }

                @Override
                public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                    Log.e("","Missed call Failure");
                }
            });
        }).start();


    }

}
