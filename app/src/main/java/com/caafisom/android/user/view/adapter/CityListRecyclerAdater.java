package com.caafisom.android.user.view.adapter;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.common.CityList;
import com.caafisom.android.user.view.adapter.listener.ICityListRecyclerAdapter;
import com.caafisom.android.user.view.adapter.viewholder.CityListViewHolder;

import java.util.List;


public class CityListRecyclerAdater extends BaseRecyclerAdapter<ICityListRecyclerAdapter, CityList, CityListViewHolder> {

    List<CityList> cityLists;
    ICityListRecyclerAdapter iCityListRecyclerAdapter;

    public CityListRecyclerAdater(List<CityList> cityLists, ICityListRecyclerAdapter iCityListRecyclerAdapter) {
        super(cityLists, iCityListRecyclerAdapter);
        this.cityLists = cityLists;
        this.iCityListRecyclerAdapter = iCityListRecyclerAdapter;

    }

    @NonNull
    @Override
    public CityListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CityListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.city_list_items_layout,parent,false),iCityListRecyclerAdapter);
    }
}
