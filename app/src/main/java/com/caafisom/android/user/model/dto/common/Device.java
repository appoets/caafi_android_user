package com.caafisom.android.user.model.dto.common;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Device implements Parcelable{

	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("provider_id")
	@Expose
	private Integer providerId;
	@SerializedName("udid")
	@Expose
	private String udid;
	@SerializedName("token")
	@Expose
	private String token;
	@SerializedName("sns_arn")
	@Expose
	private String snsArn;
	@SerializedName("type")
	@Expose
	private String type;

	protected Device(Parcel in) {
		id = in.readByte() == 0x00 ? null : in.readInt();
		providerId = in.readByte() == 0x00 ? null : in.readInt();
		udid = in.readString();
		token = in.readString();
		snsArn = in.readString();
		type = in.readString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		if (id == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeInt(id);
		}
		if (providerId == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeInt(providerId);
		}
		dest.writeString(udid);
		dest.writeString(token);
		dest.writeString(snsArn);
		dest.writeString(type);
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<Device> CREATOR = new Parcelable.Creator<Device>() {
		@Override
		public Device createFromParcel(Parcel in) {
			return new Device(in);
		}

		@Override
		public Device[] newArray(int size) {
			return new Device[size];
		}
	};
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProviderId() {
		return providerId;
	}

	public void setProviderId(Integer providerId) {
		this.providerId = providerId;
	}

	public String getUdid() {
		return udid;
	}

	public void setUdid(String udid) {
		this.udid = udid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSnsArn() {
		return snsArn;
	}

	public void setSnsArn(String snsArn) {
		this.snsArn = snsArn;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}