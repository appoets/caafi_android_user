package com.caafisom.android.user.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.SpecialityList;
import com.caafisom.android.user.presenter.SpecialityListPresenter;
import com.caafisom.android.user.view.activity.DoctorsSpecialityListActivity;
import com.caafisom.android.user.view.adapter.SpecialityListRecyclerAdapter;
import com.caafisom.android.user.view.iview.ISpecialityListView;

import butterknife.BindView;

public class SpecialityFragment extends BaseFragment<SpecialityListPresenter> implements ISpecialityListView {

    public static final String TAG = "HomeFragment";
    @BindView(R.id.rvSpeciality)
    RecyclerView rvspeciality;
    @BindView(R.id.tv_empty)
    TextView tvempty;
    Context context = getActivity();


    @Override
    protected int getLayoutID() {
        return R.layout.fragment_speciality;
    }

    @Override
    protected SpecialityListPresenter initialize() {
        return new SpecialityListPresenter(this);
    }

    @Override
    protected void setUp() {
        super.setUp();
        iPresenter.getSpecialityList();
    }

    @Override
    public void setAdapter(SpecialityListRecyclerAdapter adapter) {
        rvspeciality.setHasFixedSize(true);
        rvspeciality.setLayoutManager(new GridLayoutManager(getContext(),3));
        if (adapter.getItemCount() > 0) {
            tvempty.setVisibility(View.GONE);
            rvspeciality.setVisibility(View.VISIBLE);
            rvspeciality.setAdapter(adapter);
        } else {
            rvspeciality.setVisibility(View.GONE);
            tvempty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getDoctorsList(SpecialityList data) {
        Intent intent = new Intent(getContext(), DoctorsSpecialityListActivity.class);
        intent.putExtra("speciality_id", data.getId());
        startActivity(intent);
    }
}
