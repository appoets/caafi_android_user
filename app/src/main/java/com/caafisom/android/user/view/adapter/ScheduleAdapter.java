package com.caafisom.android.user.view.adapter;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.ScheduleResponse;
import com.caafisom.android.user.view.adapter.listener.IScheduleListener;
import com.caafisom.android.user.view.adapter.viewholder.ScheduleViewHolder;

import java.util.List;

public class ScheduleAdapter extends BaseRecyclerAdapter<IScheduleListener, ScheduleResponse, ScheduleViewHolder> {

    private List<ScheduleResponse> scheduleList;
    private IScheduleListener iScheduleListener;

    public ScheduleAdapter(List<ScheduleResponse> scheduleList, IScheduleListener iScheduleListener) {
        super(scheduleList, iScheduleListener);
        this.scheduleList = scheduleList;
        this.iScheduleListener = iScheduleListener;
    }

    @NonNull
    @Override
    public ScheduleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ScheduleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_list_item, parent, false), iScheduleListener);
    }
}
