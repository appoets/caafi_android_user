package com.caafisom.android.user.presenter.ipresenter;

public interface IHomePresenter extends IPresenter {
    void goToHome();
    void goToHistory();
    void goToSchedule();
    void goToVideoCall();
    void goToHelp();
    void goToWallet();
    void getUserDetails();
    void updateDeviceToken();
    void showGoldenMins();
    void getNotificationCount();
}