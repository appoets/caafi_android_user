package com.caafisom.android.user.view.adapter.viewholder;

import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.common.Services;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.adapter.listener.IServiceRecyclerAdapter;

import java.util.Random;

import butterknife.BindView;

/**
 * Created by Tranxit Technologies.
 */

public class ServiceViewHolder extends BaseViewHolder<Services,IServiceRecyclerAdapter> {

    @BindView(R.id.ivPicture)
    ImageView ivPicture;
    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.cv_switch)
    CardView cv_switch;

    private int lastPosition = -1;


    public ServiceViewHolder(View itemView, IServiceRecyclerAdapter listener) {
        super(itemView, listener);
    }

    @Override
    void populateData(Services data) {
        tvName.setText(data.getName());
        CodeSnippet.loadImage(CodeSnippet.getImageURL(data.getImage()),ivPicture);

        setAnimation(cv_switch,getAdapterPosition());
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(),data);
    }
}
