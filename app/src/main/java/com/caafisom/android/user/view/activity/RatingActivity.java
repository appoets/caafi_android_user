package com.caafisom.android.user.view.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.request.RatingRequest;
import com.caafisom.android.user.presenter.RatingPresenter;
import com.caafisom.android.user.presenter.ipresenter.IRatingPresenter;
import com.caafisom.android.user.view.iview.IRatingView;

import butterknife.BindView;
import butterknife.OnClick;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;

public class RatingActivity extends BaseActivity<IRatingPresenter> implements IRatingView {

    @BindView(R.id.rb_doctor)
    RatingBar rb_doctor;

    @BindView(R.id.et_review)
    EditText et_review;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.tvDoctorName)
    TextView tvDoctorName;

    private String mRequestId = "";

    @Override
    protected int attachLayout() {
        return R.layout.activity_rating;
    }

    @Override
    public void setUp(Bundle bundle) {
        if (bundle != null && bundle.containsKey("request_id")) {
            mRequestId = bundle.getString("request_id");
        }
        tvDoctorName.setText(getApplicationInstance().getDoctorName());
    }

    @Override
    IRatingPresenter initialize() {
        return new RatingPresenter(this);
    }

    @OnClick(R.id.btn_submit)
    public void Submit() {
        String comment = et_review.getText().toString().trim();
        int rating = (int) rb_doctor.getRating();
        if (rating < 1) {
            showSnackBar(getString(R.string.please_rate));
        } else if (TextUtils.isEmpty(comment)) {
            showSnackBar(getString(R.string.please_write_your_review));
        } else {
            RatingRequest request = new RatingRequest();
            request.setComment(comment);
            request.setRequest_id(mRequestId);
            request.setRating(rating + "");
            iPresenter.postRating(request);
        }
    }

    @Override
    public void onSuccess(String message) {
        showSnackBarWithDelayExit(message);
        onBackPressed();
    }
}
