package com.caafisom.android.user.presenter;

import android.os.Bundle;

import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.SendRequestModel;
import com.caafisom.android.user.model.SendRequestModelNew;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.caafisom.android.user.model.dto.response.Doctor;
import com.caafisom.android.user.model.dto.response.SendRequestResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.IDoctorBookingPresenter;
import com.caafisom.android.user.presenter.ipresenter.IDoctorDetailViewPresenter;
import com.caafisom.android.user.view.iview.IDoctorBookingView;
import com.caafisom.android.user.view.iview.IDoctorDetailView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class DoctorBookingPresenter extends BasePresenter<IDoctorBookingView> implements IDoctorBookingPresenter {

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();

    }

    public DoctorBookingPresenter(IDoctorBookingView iView) {
        super(iView);
    }

    @Override
    public void doctorAppointment(Integer doctor_ID, String date, String payment,Integer provider_id) {
        iView.showProgressbar();

        new SendRequestModelNew(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                iView.startCheckStatus();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {
                iView.dismissProgressbar();
                iView.startCheckFailStatus();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).sendRequest(doctor_ID,date,payment,provider_id);


    }
}
