package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.request.RegisterRequest;
import com.caafisom.android.user.model.dto.response.RegisterResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class RegisterModel extends BaseModel<RegisterResponse> {

    public RegisterModel(IModelListener<RegisterResponse> listener) {
        super(listener);
    }

    public void postRegister(RegisterRequest request) {
       enQueueTask(new ApiClient().getClient().create(ApiInterface.class).postRegister(request));
    }

    @Override
    public void onSuccessfulApi(RegisterResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<RegisterResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
