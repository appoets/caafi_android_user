package com.caafisom.android.user.presenter.ipresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IWalletPresenter extends IPresenter {
    void getCard();
    void addCard(String token);
    void deleteCard(String cardId);
    void addAmount(String toString, String zaad);
}
