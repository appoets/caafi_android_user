package com.caafisom.android.user.model.dto.common;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceType implements Parcelable{

	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("provider_name")
	@Expose
	private String providerName;
	@SerializedName("image")
	@Expose
	private String image;
	@SerializedName("fixed")
	@Expose
	private Double fixed;
	@SerializedName("price")
	@Expose
	private Double price;
	@SerializedName("description")
	@Expose
	private String description;
	@SerializedName("status")
	@Expose
	private Integer status;

	protected ServiceType(Parcel in) {
		id = in.readByte() == 0x00 ? null : in.readInt();
		name = in.readString();
		providerName = in.readString();
		image = in.readString();
		fixed = in.readByte() == 0x00 ? null : in.readDouble();
		price = in.readByte() == 0x00 ? null : in.readDouble();
		description = in.readString();
		status = in.readByte() == 0x00 ? null : in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		if (id == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeInt(id);
		}
		dest.writeString(name);
		dest.writeString(providerName);
		dest.writeString(image);
		if (fixed == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeDouble(fixed);
		}
		if (price == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeDouble(price);
		}
		dest.writeString(description);
		if (status == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeInt(status);
		}
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<ServiceType> CREATOR = new Parcelable.Creator<ServiceType>() {
		@Override
		public ServiceType createFromParcel(Parcel in) {
			return new ServiceType(in);
		}

		@Override
		public ServiceType[] newArray(int size) {
			return new ServiceType[size];
		}
	};

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Double getFixed() {
		return fixed;
	}

	public void setFixed(Double fixed) {
		this.fixed = fixed;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}