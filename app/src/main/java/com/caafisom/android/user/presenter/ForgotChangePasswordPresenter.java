package com.caafisom.android.user.presenter;

import com.caafisom.android.user.presenter.ipresenter.IForgotChangePasswordPresenter;
import com.caafisom.android.user.view.iview.IForgotChangePasswordView;


public class ForgotChangePasswordPresenter extends BasePresenter<IForgotChangePasswordView> implements IForgotChangePasswordPresenter {

    public ForgotChangePasswordPresenter(IForgotChangePasswordView iView) {
        super(iView);
    }

    @Override
    public void goToLogin() {
        iView.goToLogin();
    }
}
