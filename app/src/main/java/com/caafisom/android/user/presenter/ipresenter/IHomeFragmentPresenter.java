package com.caafisom.android.user.presenter.ipresenter;

import android.app.Activity;

import com.caafisom.android.user.model.dto.common.Provider;

import java.util.List;

public interface IHomeFragmentPresenter extends IPresenter {
    void getProvidersList();
    void prepareMarkerData(List<Provider> data, Activity activity);
    void moveToDetailView(Provider data);

}