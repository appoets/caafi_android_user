package com.caafisom.android.user.presenter.ipresenter;

import com.caafisom.android.user.model.dto.common.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface IDoctorFlowPresenter extends IPresenter {
    void getProviderList();
    void searchProvider(String searchKey);
    void makeVideoCall(Provider data);
    void sendRequestVideoCall(Integer serviceTypeId, Integer providerID, String date, String time, String broadcast);

}
