package com.caafisom.android.user.presenter;

import android.os.Bundle;

import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.SendRequestModel;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.response.SendRequestResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.IDoctorDetailViewPresenter;
import com.caafisom.android.user.view.iview.IDoctorDetailView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class DoctorDetailViewPresenter extends BasePresenter<IDoctorDetailView> implements IDoctorDetailViewPresenter{

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            Provider data = (Provider) bundle.getParcelable("data");
            iView.setUp(data);
        }
    }

    public DoctorDetailViewPresenter(IDoctorDetailView iView) {
        super(iView);
    }

    @Override
    public void showVideoAlert(Provider data) {
        iView.showVideoAlert(data);
    }

    @Override
    public void makeVideoCall(Provider data) {
        iView.makeVideoCall(data);
    }

    @Override
    public void scheduleAppointment(Integer serviceTypeId, Integer providerID, String date, String time, String broadcast) {
        iView.showProgressbar();
        new SendRequestModel(new IModelListener<SendRequestResponse>() {

            @Override
            public void onSuccessfulApi(@NotNull SendRequestResponse response) {
                iView.dismissProgressbar();
                iView.startCheckStatus(response.getRequest_id());
            }

            @Override
            public void onSuccessfulApi(@NotNull List<SendRequestResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).sendIndividualRequest(serviceTypeId, String.valueOf(providerID), date, time, broadcast);
    }
}
