package com.caafisom.android.user.model;


import com.caafisom.android.user.model.dto.request.ChangePasswordRequest;
import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class ChangePasswordModel extends BaseModel<BaseResponse> {

    public ChangePasswordModel(IModelListener<BaseResponse> listener) {
        super(listener);
    }

    public void updatePassword(ChangePasswordRequest request) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).changePassword(request));
    }

    @Override
    public void onSuccessfulApi(BaseResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<BaseResponse> response) {

    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
