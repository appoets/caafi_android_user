package com.caafisom.android.user.view.adapter.viewholder;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.HistoryResponse;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.model.dto.common.History;
import com.caafisom.android.user.view.adapter.listener.IHistoryRecyclerAdapter;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tranxit Technologies.
 */

public class HistoryViewHolder extends BaseViewHolder<HistoryResponse, IHistoryRecyclerAdapter> {

    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_doctor_name)
    TextView tvDoctorName;
    @BindView(R.id.tv_doctor_spl)
    TextView tvDoctorSpl;
    @BindView(R.id.tv_date_time)
    TextView tvDateTime;
    @BindView(R.id.iv_chat)
    ImageView ivChat;
    @BindView(R.id.tv_hosptial_name)
    TextView tvhosptialname;
    @BindView(R.id.tv_booking_ID)
    TextView tv_booking_ID;


    public HistoryViewHolder(View itemView, IHistoryRecyclerAdapter listener) {
        super(itemView, listener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    void populateData(HistoryResponse data) {

        if (data.getDoctor() != null) {
            tvDoctorName.setText(data.getDoctor().getFirstName() + " " + data.getDoctor().getLastName());
            if (data.getDoctor().getSpeciality() != null) {
                tvDoctorSpl.setText(data.getDoctor().getSpeciality().getName());
            }
        }

        if (data.getProvider() != null) {
            tvhosptialname.setText(data.getProvider().getFirstName() + " " + data.getProvider().getLastName());
            String url = CodeSnippet.getImageURL(data.getProvider().getAvatar());
            showImage(ivProfile, url);
        }

        tvDateTime.setText(CodeSnippet.parseDateToyyyyMMdd(data.getAssignedAt()));
        tv_booking_ID.setText(data.getBookingId());
    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }
}
