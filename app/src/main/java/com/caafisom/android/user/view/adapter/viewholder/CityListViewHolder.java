package com.caafisom.android.user.view.adapter.viewholder;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.common.City;
import com.caafisom.android.user.model.dto.common.CityList;
import com.caafisom.android.user.model.dto.common.History;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.adapter.listener.ICityListRecyclerAdapter;
import com.caafisom.android.user.view.adapter.listener.IHistoryRecyclerAdapter;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tranxit Technologies.
 */

public class CityListViewHolder extends BaseViewHolder<CityList, ICityListRecyclerAdapter> {

    @BindView(R.id.tv_city)
    TextView tvcity;

    public CityListViewHolder(View itemView, ICityListRecyclerAdapter listener) {
        super(itemView, listener);
    }

    @Override
    void populateData(CityList data) {
        tvcity.setText(data.getName());

    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }
}
