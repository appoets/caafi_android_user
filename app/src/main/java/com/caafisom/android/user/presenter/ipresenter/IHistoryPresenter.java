package com.caafisom.android.user.presenter.ipresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IHistoryPresenter extends IPresenter {
    void getHistoryList();
}
