package com.caafisom.android.user.view.activity;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.caafisom.android.user.R;
import com.caafisom.android.user.presenter.NotificationPresenter;
import com.caafisom.android.user.presenter.ipresenter.INotificationPresenter;
import com.caafisom.android.user.view.adapter.NotificationRecyclerAdapter;
import com.caafisom.android.user.view.iview.INotificationView;

import butterknife.BindView;
import butterknife.OnClick;

public class NotificationActivity extends BaseActivity<INotificationPresenter> implements INotificationView {

    @BindView(R.id.rcvNotification)
    RecyclerView rcvNotification;

    @BindView(R.id.llNoNotification)
    LinearLayout llNoNotification;


    @Override
    protected int attachLayout() {
        return R.layout.activity_notification;
    }

    @Override
    INotificationPresenter initialize() {
        return new NotificationPresenter(this);
    }

    @Override
    public void initSetUp() {
        rcvNotification.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvNotification.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    protected void onResume() {
        super.onResume();
        iPresenter.getNotification();
    }

    @Override
    public void setAdapter(NotificationRecyclerAdapter adapter) {
        if (adapter.getItemCount() > 0) {
            llNoNotification.setVisibility(View.GONE);
            rcvNotification.setVisibility(View.VISIBLE);
            rcvNotification.setAdapter(adapter);
        } else {
            rcvNotification.setVisibility(View.GONE);
            llNoNotification.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.ibBack})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                finish();
                break;
        }
    }
}
