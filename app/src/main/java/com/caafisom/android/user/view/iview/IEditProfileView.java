package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.presenter.ipresenter.IEditProfilePresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IEditProfileView extends IView<IEditProfilePresenter> {
}
