package com.caafisom.android.user.view.iview;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;

import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.util.PermissionUtils;
import com.caafisom.android.user.view.activity.BaseActivity;


public interface IView<P> {

    void showToast(String message);

    void showToast(int resId);

    void showToast(CustomException e);

    void showSnackBar(String message);

    void showSnackBarWithDelayExit(String message,long delay);

    void showSnackBarWithDelayExit(String message);

    void showSnackBar(@NonNull View view, String message);

    void showAlertDialog(String message);

    void showAlertDialog(String message, BaseActivity.whenClicked clicked);

    void showAlertDialogWithNegativeButton(String message, BaseActivity.DialogClick listener);

    void showAlertDialog(int resId);

    void showProgressbar();

    void dismissProgressbar();

    void showTextInputLayoutError(TextInputLayout til, EditText et, String msg);

    String getTextRes(int resId);

    boolean isNetworkEnabled();

    void showNetworkMessage();

    void navigateTo(Class<?> cls, boolean isFinishActivity, Bundle bundle);

    void changeFragment(Bundle bundle, @IdRes int containerViewId, @NonNull Fragment fragment, @NonNull String fragmentTag);

    void addFragment(Bundle bundle, @IdRes int containerViewId, @NonNull Fragment fragment, @NonNull String fragmentTag);

    CodeSnippet getCodeSnippet();

    PermissionUtils getPermissionUtils();

    FragmentActivity getActivity();


    void makeLogout();

    boolean checkLocationStatus();

    void onActivityForResult(int requestCode, int resultCode, Intent data);

    String getStringRes(int resId);

    void onLogout(CustomException e);

    void onLogout();
}