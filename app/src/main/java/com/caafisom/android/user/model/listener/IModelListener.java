package com.caafisom.android.user.model.listener;



import com.caafisom.android.user.model.CustomException;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IModelListener<ML> {

    void onSuccessfulApi(@NotNull ML response);

    void onSuccessfulApi(@NotNull List<ML> response);

    void onFailureApi(CustomException e);

    void onUnauthorizedUser(CustomException e);

    void onNetworkFailure();
}
