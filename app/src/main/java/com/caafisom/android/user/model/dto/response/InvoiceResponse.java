
package com.caafisom.android.user.model.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.caafisom.android.user.model.dto.response.invoice.Invoice;

public class InvoiceResponse extends BaseResponse{

    @SerializedName("invoice")
    @Expose
    private Invoice invoice;

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

}
