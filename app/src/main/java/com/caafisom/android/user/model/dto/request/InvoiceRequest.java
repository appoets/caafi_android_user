package com.caafisom.android.user.model.dto.request;

/**
 * Created by Tranxit Technologies.
 */

public class InvoiceRequest {
    private String provider_id = "";
    private String service_type = "";
    private String use_wallet = "";
    private String call_seconds = "";

    public String getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getUse_wallet() {
        return use_wallet;
    }

    public void setUse_wallet(String use_wallet) {
        this.use_wallet = use_wallet;
    }

    public String getCall_seconds() {
        return call_seconds;
    }

    public void setCall_seconds(String call_seconds) {
        this.call_seconds = call_seconds;
    }
}
