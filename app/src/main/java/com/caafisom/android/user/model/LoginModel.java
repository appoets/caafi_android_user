package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.request.LoginRequest;
import com.caafisom.android.user.model.dto.response.LoginResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class LoginModel extends BaseModel<LoginResponse> {

    public LoginModel(IModelListener<LoginResponse> listener) {
        super(listener);
    }

    public void postLogin(LoginRequest request) {
       enQueueTask(new ApiClient().getClient().create(ApiInterface.class).postLogin(request));
    }

    @Override
    public void onSuccessfulApi(LoginResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<LoginResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
