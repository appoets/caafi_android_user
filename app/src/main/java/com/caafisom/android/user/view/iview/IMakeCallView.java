package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.presenter.ipresenter.IMakeCallPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IMakeCallView extends IView<IMakeCallPresenter> {
        void setUp();
}
