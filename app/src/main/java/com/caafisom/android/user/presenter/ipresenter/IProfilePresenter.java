package com.caafisom.android.user.presenter.ipresenter;


import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface IProfilePresenter extends IPresenter {
    void updateProfile(HashMap<String, RequestBody> params, MultipartBody.Part filePart);
}