package com.caafisom.android.user.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.caafisom.android.user.model.dto.response.CheckStatusResponse;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;
import com.caafisom.android.user.util.CodeSnippet;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class CheckStatusService extends Service {

    private Handler mCheckStatusHandler = new Handler();
    private boolean isWithID = false;
    private String id;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            checkStatusCall();
            mCheckStatusHandler.postDelayed(runnable, 5000);
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        isWithID = intent.getBooleanExtra("isWithID", false);
        if (intent.getStringExtra("id") != null)
            id = intent.getStringExtra("id");

        Timber.e("onStartCommand");
        mCheckStatusHandler.postDelayed(runnable, 5000);
        return START_STICKY;
    }

    private void checkStatusCall() {
        Call<CheckStatusResponse> call;
        if (isWithID) {
            call = new ApiClient().getClient().create(ApiInterface.class).checkStatusCallwithID(id);
        } else {
            call = new ApiClient().getClient().create(ApiInterface.class).checkStatusCall();
        }

        call.enqueue(new Callback<CheckStatusResponse>() {
            @Override
            public void onResponse(@NonNull Call<CheckStatusResponse> call, @NonNull Response<CheckStatusResponse> response) {
                if (response.isSuccessful()) {
                    CheckStatusResponse tripResponse = response.body();
                    if (tripResponse != null) {
                        if (isWithID) {
                            Intent intent = new Intent();
                            intent.setAction(CodeSnippet.USER_ACTION);
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("response", tripResponse);
                            intent.putExtra("bundle", bundle);
                            sendBroadcast(intent.putExtra("response", tripResponse));
                        } else {
                            Intent intent = new Intent();
                            intent.setAction(CodeSnippet.USER_ACTION);
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("response", tripResponse);
                            intent.putExtra("bundle", bundle);
                            sendBroadcast(intent.putExtra("response", tripResponse));
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CheckStatusResponse> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCheckStatusHandler.removeCallbacks(runnable);
        Log.e("TripService", "stopped");
    }


}
