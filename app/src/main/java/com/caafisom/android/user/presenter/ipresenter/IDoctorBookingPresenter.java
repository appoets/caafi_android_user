package com.caafisom.android.user.presenter.ipresenter;

import com.caafisom.android.user.model.dto.common.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface IDoctorBookingPresenter extends IPresenter {
    void doctorAppointment(Integer providerID, String date,String payment,Integer provider_id);
}
