package com.caafisom.android.user.view.adapter;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.view.adapter.listener.IProviderRecyclerAdapter;
import com.caafisom.android.user.view.adapter.viewholder.ProviderViewHolder;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class ProviderRecyclerAdapter extends BaseRecyclerAdapter<IProviderRecyclerAdapter,Provider,ProviderViewHolder> {

    IProviderRecyclerAdapter iProviderRecyclerAdapter;

    public ProviderRecyclerAdapter(List<Provider> data, IProviderRecyclerAdapter iProviderRecyclerAdapter) {
        super(data, iProviderRecyclerAdapter);
        this.iProviderRecyclerAdapter = iProviderRecyclerAdapter;
    }

    @NonNull
    @Override
    public ProviderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProviderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_list_item,parent,false),iProviderRecyclerAdapter);
    }



}
