package com.caafisom.android.user.model;

public class WalkThrough {

    public String title, description;
    public int drawable;

    public WalkThrough(int drawable, String title, String description) {
        this.drawable = drawable;
        this.title = title;
        this.description = description;
    }
}