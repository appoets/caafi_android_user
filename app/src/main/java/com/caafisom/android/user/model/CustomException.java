package com.caafisom.android.user.model;


import com.caafisom.android.user.model.dto.response.BaseResponse;

import okhttp3.ResponseBody;

import static android.text.TextUtils.isEmpty;

public class CustomException extends Exception {

    private int code;

    private String message;

    CustomException(int code, String message) {
        this.code = code;
        this.message = message;
        System.out.println("RRR :String: message = " + message);
    }

    public CustomException(int code, Throwable t) {
        this.code = code;
        this.message = t.getMessage();
    }

    public CustomException(int code, BaseResponse response) {
        this.code = code;
        this.message = isEmpty(response.getMessage())
                ? response.getError()
                : response.getMessage();
    }

    public CustomException() {
    }

    public <T extends BaseResponse> CustomException(T convert) {
        this.message = convert.getMessage();
    }

    public CustomException(int code, ResponseBody response) {
        this.code = code;
        this.message = response.toString();
        System.out.println("RRR :: message = " + message);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
