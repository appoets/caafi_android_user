package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.response.ProviderResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class ProviderModel extends BaseModel<ProviderResponse> {

    public ProviderModel(IModelListener<ProviderResponse> listener) {
        super(listener);
    }


    @Override
    public void onSuccessfulApi(ProviderResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<ProviderResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getProviderList() {
         enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getProviderList());
    }

    public void searchProvider(String searchKey) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).searchProvider(searchKey));
    }

    public void searchProviderByService(String strSearchKey) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).searchProviderByService(strSearchKey));
    }

    public void sendRequestVideoCall(Integer serviceTypeId, String providerID, String date, String time, String broadcast) {
    }
}
