package com.caafisom.android.user.view.activity;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;

import com.caafisom.android.user.R;
import com.caafisom.android.user.presenter.OneTimePasswordPresenter;
import com.caafisom.android.user.presenter.ipresenter.IOneTimePasswordPresenter;
import com.caafisom.android.user.view.iview.IOneTimePasswordView;

import butterknife.BindView;
import butterknife.OnClick;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;

public class OneTimePasswordActivity extends BaseActivity<IOneTimePasswordPresenter> implements IOneTimePasswordView {

    @BindView(R.id.etOTP)
    EditText etOTP;

    @Override
    protected int attachLayout() {
        return R.layout.activity_onetime_password;
    }

    @Override
    IOneTimePasswordPresenter initialize() {
        return new OneTimePasswordPresenter(this);
    }

    @OnClick({R.id.btnSendOTP})
    public void OnClickView(View view){
        switch (view.getId()){
            case R.id.btnSendOTP:
                validateOTP();
                break;
        }
    }

    @Override
    public void setUp() {
        String OTP = getApplicationInstance().getOTP();
        etOTP.setText(OTP);
    }

    private void validateOTP() {
        String otp = etOTP.getText().toString().trim();
        if (otp.equalsIgnoreCase("")){
            showSnackBar(getString(R.string.please_enter_otp));
        }else{
            iPresenter.goToForgotChangePassword();
        }
    }

    @Override
    public void goToForgotChangePassword() {
            startActivity(new Intent(this,ForgotChangePasswordActivity.class));
    }
}
