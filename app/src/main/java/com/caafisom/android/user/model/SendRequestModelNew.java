package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.caafisom.android.user.model.dto.response.SendRequestResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class SendRequestModelNew extends BaseModel<BaseResponse> {


    public SendRequestModelNew(IModelListener<BaseResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(BaseResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<BaseResponse> response) {

    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void sendRequest(Integer DoctorID, String date,  String payment,Integer provider_id) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).sendRequest(DoctorID, date, payment,provider_id));
    }

   /* public void sendIndividualRequest(String providerID, String date,  String payment) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).sendRequest(providerID, date, payment));
    }*/
}
