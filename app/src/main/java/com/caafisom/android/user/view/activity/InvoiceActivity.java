package com.caafisom.android.user.view.activity;

import android.os.Bundle;
import androidx.annotation.NonNull;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.dto.common.videocalldata.Data;
import com.caafisom.android.user.model.dto.common.videocalldata.Fcm;
import com.caafisom.android.user.model.dto.common.videocalldata.FcmNotification;
import com.caafisom.android.user.model.dto.request.InvoiceRequest;
import com.caafisom.android.user.model.dto.request.PaymentRequest;
import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.caafisom.android.user.model.dto.response.invoice.Invoice;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;
import com.caafisom.android.user.presenter.InvoicePresenter;
import com.caafisom.android.user.presenter.ipresenter.IInvoicePresenter;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.iview.IInvoiceView;

import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;
import static timber.log.Timber.v;

public class InvoiceActivity extends BaseActivity<IInvoicePresenter> implements IInvoiceView{


    @BindView(R.id.tv_invoice_id)
    TextView tv_invoice_id;
    @BindView(R.id.tv_call_duration)
    TextView tv_call_duration;
    @BindView(R.id.tv_base_rate)
    TextView tv_base_rate;
    @BindView(R.id.tv_pay_doctor)
    TextView tv_pay_doctor;
    @BindView(R.id.tv_ztoid_fee)
    TextView tv_ztoid_fee;
    @BindView(R.id.tv_promotion)
    TextView tv_promotion;
    @BindView(R.id.tv_balance_wallet)
    TextView tv_balance_wallet;
    @BindView(R.id.tv_tax)
    TextView tv_tax;
    @BindView(R.id.tv_total)
    TextView tv_total;
    @BindView(R.id.tv_amount_paid)
    TextView tv_amount_paid;

    String call_duration="00";
    PaymentRequest payRequest = new PaymentRequest();
    String mRequestID="";

    @Override
    protected int attachLayout() {
        return R.layout.activity_invoice;
    }

    @Override
    IInvoicePresenter initialize() {
        return new InvoicePresenter(this);
    }

    @Override
    public void setUp(Bundle bundle) {
        if (bundle!=null){
            String provider_id = bundle.getString("provider_id");
            String service_type = bundle.getString("service_type");
            String use_wallet = bundle.getString("use_wallet");
            call_duration = bundle.getString("call_duration");

            InvoiceRequest request = new InvoiceRequest();
            request.setProvider_id(provider_id);
            request.setService_type(service_type);
            request.setUse_wallet(use_wallet);
            request.setCall_seconds(call_duration);


            iPresenter.requestInvoice(request);
        }


    }



    @Override
    public void updateInvoiceData(Invoice data) {

        String timeFormat = new CodeSnippet().secToDisplayFormat(Long.parseLong(call_duration));

        tv_invoice_id.setText(MessageFormat.format("{0}{1}", getString(R.string.invoice_id), data.getId()));
        tv_call_duration.setText(timeFormat);
        String base_rate = getApplicationInstance().getCurrency()+data.getProviderService().getProviderPrice()+"/min";
        tv_base_rate.setText(base_rate);
        tv_pay_doctor.setText(MessageFormat.format("{0}{1}",getApplicationInstance().getCurrency(), data.getInvoice().getFixed()));
        tv_ztoid_fee.setText(MessageFormat.format("{0}{1}",getApplicationInstance().getCurrency(), data.getInvoice().getCommision()));
        tv_promotion.setText(MessageFormat.format("{0}{1}",getApplicationInstance().getCurrency(), data.getInvoice().getDiscount()));
        tv_balance_wallet.setText(MessageFormat.format("{0}{1}",getApplicationInstance().getCurrency(), "0"));
        tv_tax.setText(MessageFormat.format("{0}{1}",getApplicationInstance().getCurrency(), data.getInvoice().getTax()));
        tv_total.setText(MessageFormat.format("{0}{1}",getApplicationInstance().getCurrency(), data.getInvoice().getTotal()));
        tv_amount_paid.setText(MessageFormat.format("{0}{1}",getApplicationInstance().getCurrency(), data.getInvoice().getTotal()));
        mRequestID =data.getInvoice().getRequestId()+"";

        //payRequest.setCard_id(data.getCard().getCardId());
        payRequest.setPayment_mode("CASH");
        payRequest.setRequest_id(data.getInvoice().getRequestId()+"");

        sendPush("INVOICE");

    }

    private void sendPush(String type) {


        String timeStamp = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) + "";


        Data fcmData = new Data();
        fcmData.setId("");
        fcmData.setCallType(type);
        fcmData.setPatientName("");
        fcmData.setAvatar("");
        fcmData.setMobile("");
        fcmData.setDevice_token("");
        fcmData.setName("");
        fcmData.setCall_duration(call_duration);
        fcmData.setRequest_id(mRequestID);


        FcmNotification notification = new FcmNotification();
        notification.setTitle("");
        notification.setBody(type);

        Fcm fcm = new Fcm();
        fcm.setTo(getApplicationInstance().getSenderDeviceToken());
        fcm.setData(fcmData);
       // fcm.setFcmNotification(notification);


        SendFCM(fcm);


    }


    private void SendFCM(Fcm fcm) {

        Call<BaseResponse> call = new ApiClient().getClient(Constants.URL.FCM_URL).create(ApiInterface.class).sendFCM(fcm);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull Response<BaseResponse> response) {
                v("Send FCM ");
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                v("FCM Failure");
            }
        });
    }


    @OnClick(R.id.btn_confirm)
    public void OnViewClick(){
        iPresenter.requestPayment(payRequest);
    }


    @Override
    public void paymentSuccess() {
        Bundle bundle = new Bundle();
        bundle.putString("request_id",mRequestID);
        navigateTo(RatingActivity.class,true,bundle);
    }
}
