package com.caafisom.android.user.presenter.ipresenter;

import com.caafisom.android.user.model.dto.common.videocalldata.Fcm;

public interface IMakeCallPresenter extends IPresenter {
    void sendFCMMessage(Fcm data);
}