package com.caafisom.android.user.view.iview;



import com.caafisom.android.user.model.dto.response.ProfileResponse;
import com.caafisom.android.user.presenter.ipresenter.IProfilePresenter;

public interface IProfileView extends IView<IProfilePresenter> {
    void updateUserDetails(ProfileResponse response);
}
