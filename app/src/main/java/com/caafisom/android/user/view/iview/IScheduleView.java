package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.model.dto.response.ScheduleResponse;
import com.caafisom.android.user.presenter.ipresenter.ISchedulePresenter;
import com.caafisom.android.user.view.adapter.listener.IScheduleListener;

import java.util.List;

public interface IScheduleView extends IView<ISchedulePresenter> {
    void setAdapter(List<ScheduleResponse> list, IScheduleListener iScheduleListener);
    void moveToDetail(ScheduleResponse data);
    void initSetUp();
}
