package com.caafisom.android.user.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.response.SpecialityList;
import com.caafisom.android.user.view.adapter.listener.IProviderRecyclerAdapter;
import com.caafisom.android.user.view.adapter.listener.ISpecialityListRecyclerAdapter;
import com.caafisom.android.user.view.adapter.viewholder.ProviderViewHolder;
import com.caafisom.android.user.view.adapter.viewholder.SpecialityViewHolder;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class SpecialityListRecyclerAdapter extends BaseRecyclerAdapter<ISpecialityListRecyclerAdapter, SpecialityList, SpecialityViewHolder> {

    ISpecialityListRecyclerAdapter iSpecialityListRecyclerAdapter;

    public SpecialityListRecyclerAdapter(List<SpecialityList> data, ISpecialityListRecyclerAdapter iSpecialityListRecyclerAdapter) {
        super(data, iSpecialityListRecyclerAdapter);
        this.iSpecialityListRecyclerAdapter = iSpecialityListRecyclerAdapter;
    }

    @NonNull
    @Override
    public SpecialityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SpecialityViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.speciality_list_item,parent,false),iSpecialityListRecyclerAdapter);
    }



}
