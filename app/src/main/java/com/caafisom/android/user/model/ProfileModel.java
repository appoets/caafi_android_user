package com.caafisom.android.user.model;


import com.caafisom.android.user.model.dto.request.ProfileRequest;
import com.caafisom.android.user.model.dto.response.ProfileResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class ProfileModel extends BaseModel<ProfileResponse> {

    public ProfileModel(IModelListener<ProfileResponse> listener) {
        super(listener);
    }

    public void getUserDetails() {
       enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getUserDetails());
    }

    @Override
    public void onSuccessfulApi(ProfileResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<ProfileResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void updateProfile(ProfileRequest request) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).updateProfile(request));
    }
}
