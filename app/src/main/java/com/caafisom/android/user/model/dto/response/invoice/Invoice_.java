
package com.caafisom.android.user.model.dto.response.invoice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Invoice_ {

    @SerializedName("request_id")
    @Expose
    private Integer requestId;
    @SerializedName("fixed")
    @Expose
    private Double fixed;
    @SerializedName("time_price")
    @Expose
    private Double timePrice;
    @SerializedName("commision")
    @Expose
    private Double commision;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("tax")
    @Expose
    private Double tax;
    @SerializedName("id")
    @Expose
    private Integer id;

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public Double getFixed() {
        return fixed;
    }

    public void setFixed(Double fixed) {
        this.fixed = fixed;
    }

    public Double getTimePrice() {
        return timePrice;
    }

    public void setTimePrice(Double timePrice) {
        this.timePrice = timePrice;
    }

    public Double getCommision() {
        return commision;
    }

    public void setCommision(Double commision) {
        this.commision = commision;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
