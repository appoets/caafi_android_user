package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.response.ServicesResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class ServicesModel extends BaseModel<ServicesResponse> {

    public ServicesModel(IModelListener<ServicesResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(ServicesResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<ServicesResponse> response) {

    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getServiceList() {
         enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getServiceList());
    }

    public void searchSpecialist(String strSearchKey) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).searchServices(strSearchKey));
    }


}
