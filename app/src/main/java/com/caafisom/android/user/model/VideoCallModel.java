package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.response.VideoCallResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class VideoCallModel extends BaseModel<VideoCallResponse> {


    public VideoCallModel(IModelListener<VideoCallResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(VideoCallResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<VideoCallResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getVideoCallList() {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getVideoCallList());
    }

}
