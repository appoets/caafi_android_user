package com.caafisom.android.user.presenter;

import android.os.Bundle;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.DeviceTokenModel;
import com.caafisom.android.user.model.NotificationModel;
import com.caafisom.android.user.model.ProfileModel;
import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.caafisom.android.user.model.dto.response.NotificationResponse;
import com.caafisom.android.user.model.dto.response.ProfileResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.IHomePresenter;
import com.caafisom.android.user.view.iview.IHomeView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;


public class HomePresenter extends BasePresenter<IHomeView> implements IHomePresenter {

    public HomePresenter(IHomeView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.setUp();
        String device_token = getApplicationInstance().getFCMToken();

        Log.e("the token","====="+device_token);
    }


    @Override
    public void getUserDetails() {
        new ProfileModel(new IModelListener<ProfileResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProfileResponse response) {
                iView.dismissProgressbar();
                getApplicationInstance().setCurrency(response.getCurrency());
                getApplicationInstance().setUserID(response.getId());
                getApplicationInstance().setUserName(response.getFirstName());
                getApplicationInstance().setUserImage(response.getPicture());
                getApplicationInstance().setUserMobile(response.getMobile());

                iView.updateUserDetails(response);
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProfileResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getUserDetails();
    }

    @Override
    public void updateDeviceToken() {
        String deviceToken = FirebaseInstanceId.getInstance().getToken();
        getApplicationInstance().setFCMToken(deviceToken);

        new DeviceTokenModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {
                iView.dismissProgressbar();
                iView.updateNotification(response.size());
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).updateToken(deviceToken);
    }

    @Override
    public void goToHome() {
        iView.goToHome();
    }

    @Override
    public void goToHistory() {
        iView.goToHistory();
    }

    @Override
    public void goToSchedule() {
        iView.goToSchedule();
    }

    @Override
    public void goToVideoCall() {
        iView.goToVideoCall();
    }

    @Override
    public void goToHelp() {
        iView.goToHelp();
    }

    @Override
    public void goToWallet() {
        iView.goToWallet();
    }

    @Override
    public void showGoldenMins() {
        iView.showGoldenMins();
    }

    @Override
    public void getNotificationCount() {
        new NotificationModel(new IModelListener<NotificationResponse>() {

            @Override
            public void onSuccessfulApi(@NotNull NotificationResponse response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull List<NotificationResponse> response) {
                iView.dismissProgressbar();
                iView.updateNotification(response.size());
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.updateNotification(0);
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.updateNotification(0);
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getNotificationDetail();
    }
}
