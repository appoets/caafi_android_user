package com.caafisom.android.user.presenter.ipresenter;

import com.caafisom.android.user.model.dto.common.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface IHospitalListPresenter extends IPresenter {
    void getHosptialsList(String searchKey);

}
