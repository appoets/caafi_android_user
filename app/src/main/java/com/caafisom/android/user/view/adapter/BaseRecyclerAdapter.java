package com.caafisom.android.user.view.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.caafisom.android.user.view.adapter.viewholder.BaseViewHolder;

import java.util.List;

abstract class BaseRecyclerAdapter<L, D, V extends BaseViewHolder> extends RecyclerView.Adapter<V> {

    //L mean Listener
    //D mean Data
    //V mean viewHolder

    protected String TAG = getClass().getSimpleName();
    protected List<D> data;
    protected L listener;

    public BaseRecyclerAdapter(List<D> data, L listener) {
        this.data = data;
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull V holder, int position) {
        holder.setData(getItem(position));
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public D getItem(int position) throws IndexOutOfBoundsException {
        return data.get(position);
    }

    public void addItem(D object) {
        data.add(object);
        notifyItemInserted(data.indexOf(object));
    }

    public void restoreItem(D item, int position) {
        data.add(position, item);
        notifyItemInserted(position);
    }

    public List<D> getAllItem() {
        return data;
    }

    public void removeItem(int position) throws IndexOutOfBoundsException {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public void resetItems(List<D> data) {
        if (data != null) {
            this.data = data;
            notifyDataSetChanged();
        }
    }

    public void addItems(List<D> data) {
        if (data != null) {
            int startRange = (this.data.size() - 1) > 0 ? data.size() - 1 : 0;
            this.data.addAll(data);
            notifyItemRangeChanged(startRange, data.size());
        }
    }

    public void setItem(int position, D data) {
        if (data != null && this.data.size() > position) {
            this.data.set(position, data);
            notifyItemChanged(position);
        }
    }
}
