package com.caafisom.android.user.presenter.ipresenter;

import com.caafisom.android.user.model.dto.request.InvoiceRequest;
import com.caafisom.android.user.model.dto.request.PaymentRequest;

/**
 * Created by Tranxit Technologies.
 */

public interface IInvoicePresenter extends IPresenter {
        void requestInvoice(InvoiceRequest request);
        void requestPayment(PaymentRequest request);
}
