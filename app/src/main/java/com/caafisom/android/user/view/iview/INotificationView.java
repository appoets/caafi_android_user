package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.presenter.ipresenter.INotificationPresenter;
import com.caafisom.android.user.view.adapter.NotificationRecyclerAdapter;

public interface INotificationView extends IView<INotificationPresenter> {
    void initSetUp();
    void setAdapter(NotificationRecyclerAdapter adapter);
}
