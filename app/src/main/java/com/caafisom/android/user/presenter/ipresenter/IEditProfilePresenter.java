package com.caafisom.android.user.presenter.ipresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IEditProfilePresenter extends IPresenter {
    void updateProfile(String fname,String lname,String mobile,String img);
}
