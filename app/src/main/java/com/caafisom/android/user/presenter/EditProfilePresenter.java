package com.caafisom.android.user.presenter;

import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.ProfileModel;
import com.caafisom.android.user.model.dto.request.ProfileRequest;
import com.caafisom.android.user.model.dto.response.ProfileResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.IEditProfilePresenter;
import com.caafisom.android.user.view.iview.IEditProfileView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class EditProfilePresenter extends BasePresenter<IEditProfileView> implements IEditProfilePresenter {

    public EditProfilePresenter(IEditProfileView iView) {
        super(iView);
    }

    @Override
    public void updateProfile(String fname, String lname, String mobile, String img) {
        ProfileRequest request = new ProfileRequest();
        request.setFirst_name(fname);
        request.setLast_name(lname);
        request.setMobile(mobile);
        request.setPicture("");
        new ProfileModel(new IModelListener<ProfileResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProfileResponse response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProfileResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).updateProfile(request);
    }
}
