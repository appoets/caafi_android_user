package com.caafisom.android.user.view.adapter.listener;

import com.caafisom.android.user.model.dto.common.History;
import com.caafisom.android.user.model.dto.response.HistoryResponse;

/**
 * Created by Tranxit Technologies.
 */

public interface IHistoryRecyclerAdapter extends BaseRecyclerListener<HistoryResponse> {
}
