package com.caafisom.android.user.view.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.ScheduleResponse;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.adapter.listener.IScheduleListener;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ScheduleViewHolder extends BaseViewHolder<ScheduleResponse, IScheduleListener> {

    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_doctor_name)
    TextView tvDoctorName;
    @BindView(R.id.tv_doctor_spl)
    TextView tvDoctorSpl;
    @BindView(R.id.tv_date_time)
    TextView tvDateTime;

    public ScheduleViewHolder(View itemView, IScheduleListener listener) {
        super(itemView, listener);
    }

    @Override
    void populateData(ScheduleResponse data) {
        if (data.getProvider() != null) {
            if (data.getProvider().getAvatar() != null) {
                String url = CodeSnippet.getImageURL(data.getProvider().getAvatar());
                showImage(ivProfile, url);
            }
            tvDoctorName.setText(data.getProvider().getFirstName() + data.getProvider().getLastName());
            tvDoctorSpl.setText(data.getProvider_service().getService_type().getName() != null &&
                    data.getProvider_service().getService_type().getName().length() > 0 ? data.getProvider_service().getService_type().getName() : " - ");
            tvDateTime.setText(CodeSnippet.parseDateToyyyyMMdd(data.getScheduleAt() != null && data.getScheduleAt().length() > 0 ? data.getScheduleAt() : " - "));
        }
    }
    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }

}
