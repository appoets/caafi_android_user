package com.caafisom.android.user.view.adapter.listener;

import com.caafisom.android.user.model.dto.response.ScheduleResponse;

public interface IScheduleListener extends BaseRecyclerListener<ScheduleResponse> {
}
