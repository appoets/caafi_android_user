package com.caafisom.android.user.fcm;

/**
 * Created by jayakumar on 16/02/17.
 */

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.caafisom.android.user.R;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.util.NotificationUtils;
import com.caafisom.android.user.videocall.IncomingCallActivity;
import com.caafisom.android.user.view.activity.HomeActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;
import static com.caafisom.android.user.common.Constants.NotificationConstants.PUSH_NOTIFICATION;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    Integer senderID = -1;
    String chatPath = "";
    String chatName = "", chatFrom = "", chatAvatar = "", chatMobile, chatDeviceToken = "", chatGroupName = "";
    private NotificationUtils notificationUtils;
    int notificationId = 0;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        getApplicationInstance().setFCMToken(s);
        @SuppressLint("HardwareIds")
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        getApplicationInstance().setDeviceId(deviceId);
        Log.d("DEVICE_ID: ", deviceId);
        Log.d("FCM_TOKEN", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        //trigger push
        sendNotification(remoteMessage.getData().get("message"));
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
               /* JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);*/
                Map<String, String> notificationMap = remoteMessage.getData();
                String device_type = "", device_token = "", callType = "", id = "", name = "", patientName = "", call_duration = "", avatar = "", mobile = "", request_id = "";
                if (notificationMap.containsKey("device_type"))
                    device_type = notificationMap.get("device_type");
                if (notificationMap.containsKey("device_token"))
                    device_token = notificationMap.get("device_token");
                if (notificationMap.containsKey("callType"))
                    callType = notificationMap.get("callType");
                if (notificationMap.containsKey("id"))
                    id = notificationMap.get("id");
                if (notificationMap.containsKey("name"))
                    name = notificationMap.get("name");
                if (notificationMap.containsKey("patientName"))
                    patientName = notificationMap.get("patientName");
                //Additional Nodes
                if (notificationMap.containsKey("call_duration"))
                    call_duration = notificationMap.get("call_duration");
                if (notificationMap.containsKey("avatar"))
                    avatar = notificationMap.get("avatar");
                if (notificationMap.containsKey("mobile"))
                    mobile = notificationMap.get("mobile");
                if (notificationMap.containsKey("request_id"))
                    request_id = notificationMap.get("request_id");
                Log.e(TAG, "Received Data from Notification " + device_type + " " + device_token + " " + callType + " " + id + " " + name + " " + patientName);
                if (!device_token.equalsIgnoreCase("") && !device_token.contains("=")) {
                    device_token = device_token + "=";
                }
                if (!device_token.equalsIgnoreCase("") && device_token.length() > 1) {
                    device_token = new CodeSnippet().decodeBase64(device_token);
                }
                if (callType.equalsIgnoreCase("MAKECALL")) {
                    Intent mainIntent = new Intent(this, IncomingCallActivity.class);
                    mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mainIntent.putExtra("id", id + "");
                    mainIntent.putExtra("name", patientName);
                    mainIntent.putExtra("avatar", avatar);
                    mainIntent.putExtra("chatpath", name);
                    mainIntent.putExtra("mobile", mobile);
                    mainIntent.putExtra("device_token", device_token);
                    startActivity(mainIntent);
                } else if (callType.equalsIgnoreCase("CUTCALL")) {
                    Intent intent = new Intent("cut_call");
                    sendBroadcast(intent);
                } else if (callType.equalsIgnoreCase("ATTENDCALL")) {
                    Intent intent = new Intent("attend_call");
                    sendBroadcast(intent);
                } else if (callType.equalsIgnoreCase("REJECTCALL")) {
                    Intent intent = new Intent("reject_call");
                    sendBroadcast(intent);
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        String channelId = "fcm_default_channel";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_push)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(notificationId++ /* ID of notification */, notificationBuilder.build());
    }
}