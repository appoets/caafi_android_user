package com.caafisom.android.user.view.adapter;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.NotificationResponse;
import com.caafisom.android.user.view.adapter.listener.INotificationRecyclerAdapter;
import com.caafisom.android.user.view.adapter.viewholder.NotificationViewHolder;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class NotificationRecyclerAdapter extends BaseRecyclerAdapter<INotificationRecyclerAdapter, NotificationResponse, NotificationViewHolder> {

    List<NotificationResponse> notificationResponseList;
    INotificationRecyclerAdapter iNotificationRecyclerAdapter;

    public NotificationRecyclerAdapter(List<NotificationResponse> notificationResponseList, INotificationRecyclerAdapter iNotificationRecyclerAdapter) {
        super(notificationResponseList, iNotificationRecyclerAdapter);
        this.notificationResponseList = notificationResponseList;
        this.iNotificationRecyclerAdapter = iNotificationRecyclerAdapter;

    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotificationViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false), iNotificationRecyclerAdapter);
    }
}
