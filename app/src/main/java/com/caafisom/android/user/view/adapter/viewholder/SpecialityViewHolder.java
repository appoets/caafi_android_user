package com.caafisom.android.user.view.adapter.viewholder;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.response.SpecialityList;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.adapter.listener.IProviderRecyclerAdapter;
import com.caafisom.android.user.view.adapter.listener.ISpecialityListRecyclerAdapter;

import java.util.Random;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tranxit Technologies.
 */

public class SpecialityViewHolder extends BaseViewHolder<SpecialityList, ISpecialityListRecyclerAdapter> {

    @BindView(R.id.tv_specialityName)
    TextView tvspecialityName;
    @BindView(R.id.rl_speciality)
    RelativeLayout rlspeciality;
    @BindView(R.id.iv_speciality)
    CircleImageView ivspeciality;

    public SpecialityViewHolder(View itemView, ISpecialityListRecyclerAdapter listener) {
        super(itemView, listener);
    }


    @Override
    void populateData(SpecialityList data) {
        tvspecialityName.setText(data.getName());
        CodeSnippet.loadImageCircle(String.valueOf(data.getPicture()), ivspeciality, R.drawable.ic_dummy_user);

    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }
}
