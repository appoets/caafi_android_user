package com.caafisom.android.user.model.dto.request;

/**
 * Created by Tranxit Technologies.
 */

public class WalletRequest {

    String payment_mode;
    String amount;
    String card_id;

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }
}
