package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class AddDeleteCardModel extends BaseModel<BaseResponse> {

    public AddDeleteCardModel(IModelListener<BaseResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(BaseResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<BaseResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void addCard(String token) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).addCard(token));
    }

    public void deleteCard(String cardId) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).deleteCard(cardId));
    }
}
