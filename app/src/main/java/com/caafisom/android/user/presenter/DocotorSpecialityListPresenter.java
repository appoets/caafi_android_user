package com.caafisom.android.user.presenter;


import android.os.Bundle;

import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.DoctorSpecialityListModel;
import com.caafisom.android.user.model.SpecialityListModel;
import com.caafisom.android.user.model.dto.response.DoctorsSpecialityList;
import com.caafisom.android.user.model.dto.response.SpecialityList;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.IDoctorSpecialityListPresenter;
import com.caafisom.android.user.presenter.ipresenter.ISpecialityListPresenter;
import com.caafisom.android.user.view.adapter.DoctorSpecialityListRecyclerAdapter;
import com.caafisom.android.user.view.adapter.SpecialityListRecyclerAdapter;
import com.caafisom.android.user.view.adapter.listener.IDoctorsSpecialityListRecyclerAdapter;
import com.caafisom.android.user.view.adapter.listener.ISpecialityListRecyclerAdapter;
import com.caafisom.android.user.view.iview.IDoctorSpecialityListView;
import com.caafisom.android.user.view.iview.ISpecialityListView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class DocotorSpecialityListPresenter extends BasePresenter<IDoctorSpecialityListView> implements IDoctorSpecialityListPresenter {


    public DocotorSpecialityListPresenter(IDoctorSpecialityListView iView) {
        super(iView);
    }

    IDoctorsSpecialityListRecyclerAdapter iSpecialityListRecyclerAdapter = new IDoctorsSpecialityListRecyclerAdapter() {
        @Override
        public void onClickItem(int pos, DoctorsSpecialityList data) {
            iView.getDoctorsList(data);
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();

    }


    @Override
    public void getDoctorSpecialityList(Integer id) {
        iView.showProgressbar();

        new DoctorSpecialityListModel(new IModelListener<DoctorsSpecialityList>() {
            @Override
            public void onSuccessfulApi(@NotNull DoctorsSpecialityList response) {
                iView.dismissProgressbar();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<DoctorsSpecialityList> response) {
                iView.dismissProgressbar();
                iView.setAdapter(new DoctorSpecialityListRecyclerAdapter(response, iSpecialityListRecyclerAdapter));
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).getDoctorsSpeciality(id);
    }


    @Override
    public void getDoctorsHospitalList(Integer id) {
        iView.showProgressbar();

        new DoctorSpecialityListModel(new IModelListener<DoctorsSpecialityList>() {
            @Override
            public void onSuccessfulApi(@NotNull DoctorsSpecialityList response) {
                iView.dismissProgressbar();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<DoctorsSpecialityList> response) {
                iView.dismissProgressbar();
                iView.setAdapter(new DoctorSpecialityListRecyclerAdapter(response, iSpecialityListRecyclerAdapter));
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).getDoctorsHospitalList(id);
    }





}
