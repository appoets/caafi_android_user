package com.caafisom.android.user.model.dto.common.videocalldata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by santhosh@appoets.com on 03-03-2018.
 */

public class Fcm {

    @SerializedName("to")
    @Expose
    private String to;
    /*@SerializedName("notification")
    @Expose
    private FcmNotification fcmNotification;*/

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    /*public FcmNotification getFcmNotification() {
        return fcmNotification;
    }

    public void setFcmNotification(FcmNotification fcmNotification) {
        this.fcmNotification = fcmNotification;
    }*/



}


