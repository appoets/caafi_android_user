package com.caafisom.android.user.presenter;

import android.os.Bundle;

import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.RatingModel;
import com.caafisom.android.user.model.dto.request.RatingRequest;
import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.presenter.ipresenter.IRatingPresenter;
import com.caafisom.android.user.view.iview.IRatingView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class RatingPresenter extends BasePresenter<IRatingView> implements IRatingPresenter {

    public RatingPresenter(IRatingView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.setUp(bundle);
    }

    @Override
    public void postRating(RatingRequest request) {
        new RatingModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                iView.onSuccess(response.getMessage());
            }

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).postRating(request);
    }
}
