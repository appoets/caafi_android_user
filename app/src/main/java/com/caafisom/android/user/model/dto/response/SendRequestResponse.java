package com.caafisom.android.user.model.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendRequestResponse extends BaseResponse {

    @SerializedName("request_id")
    @Expose
    private String request_id;
    @SerializedName("message")
    private String message;
    @SerializedName("provider")
    private Integer providers;


 /*   @SerializedName("provider")
    @Expose
    private String provider;*/

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    /*public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }*/

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getProviders() {
        return providers;
    }

    public void setProviders(Integer providers) {
        this.providers = providers;
    }
}
