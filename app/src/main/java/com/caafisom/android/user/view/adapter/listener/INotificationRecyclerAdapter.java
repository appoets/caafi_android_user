package com.caafisom.android.user.view.adapter.listener;

import com.caafisom.android.user.model.dto.response.NotificationResponse;

/**
 * Created by Tranxit Technologies.
 */

public interface INotificationRecyclerAdapter extends BaseRecyclerListener<NotificationResponse> {
}
