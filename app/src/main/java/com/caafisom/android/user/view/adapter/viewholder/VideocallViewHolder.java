package com.caafisom.android.user.view.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.model.dto.response.VideoCallResponse;
import com.caafisom.android.user.view.adapter.listener.IVideoCallListener;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class VideocallViewHolder extends BaseViewHolder<VideoCallResponse, IVideoCallListener> {

    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_doctor_name)
    TextView tvDoctorName;
    @BindView(R.id.tv_doctor_spl)
    TextView tvDoctorSpl;
    @BindView(R.id.tv_date_time)
    TextView tvDateTime;


    public VideocallViewHolder(View itemView, IVideoCallListener listener) {
        super(itemView, listener);
    }

    @Override
    void populateData(VideoCallResponse data) {
/*        String url = CodeSnippet.getImageURL(data.getProvider().getAvatar());
        showImage(ivProfile,url);
        tvDoctorName.setText(data.getProvider().getFirstName() + data.getProvider().getLastName());
        tvDoctorSpl.setText(data.getServiceType().getName());
        tvDateTime.setText(CodeSnippet.parseDateToyyyyMMdd(data.getAssignedAt()));*/
    }

}
