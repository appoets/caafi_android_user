package com.caafisom.android.user.view.activity;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.RatingModel;
import com.caafisom.android.user.model.dto.request.PaymentRequest;
import com.caafisom.android.user.model.dto.request.RatingRequest;
import com.caafisom.android.user.model.dto.response.BaseResponse;
import com.caafisom.android.user.model.dto.response.CardResponse;
import com.caafisom.android.user.model.dto.response.CheckStatusResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;
import com.caafisom.android.user.service.CheckStatusService;
import com.caafisom.android.user.util.CodeSnippet;
import com.caafisom.android.user.view.widget.CustomProgressbar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jetbrains.annotations.NotNull;

import java.text.MessageFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;

import static com.caafisom.android.user.MyApplication.getApplicationInstance;

public class ScheduleDetailActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.statusLayout)
    LinearLayout statusLayout;
    @BindView(R.id.iv_profile)
    CircleImageView iv_profile;
    @BindView(R.id.tv_doctor_name)
    TextView tv_doctor_name;
    @BindView(R.id.tv_doctor_spl)
    TextView tv_doctor_spl;
    @BindView(R.id.tv_treatment)
    TextView tv_treatment;
    @BindView(R.id.tv_rate_txt)
    TextView tv_rate_txt;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tv_desc_txt)
    TextView tv_desc_txt;
    @BindView(R.id.btnCancelRequest)
    Button btnCancelRequest;

    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.llInvoice)
    LinearLayout llInvoice;
    @BindView(R.id.ivInvoiceProfile)
    CircleImageView ivInvoiceProfile;
    @BindView(R.id.tv_base_rate)
    TextView tv_base_rate;
    @BindView(R.id.tv_pay_doctor)
    TextView tv_pay_doctor;
    @BindView(R.id.tv_ztoid_fee)
    TextView tv_ztoid_fee;
    @BindView(R.id.tv_promotion)
    TextView tv_promotion;
    @BindView(R.id.tv_balance_wallet)
    TextView tv_balance_wallet;
    @BindView(R.id.tv_tax)
    TextView tv_tax;
    @BindView(R.id.tv_total)
    TextView tv_total;
    @BindView(R.id.tv_amount_paid)
    TextView tv_amount_paid;
    @BindView(R.id.btnConfirm)
    Button btnConfirm;

    private MyReceiver myReceiver;
    private String requestID;

    private GoogleMap mMap;
    private Marker mCtLocationMarker;
    GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    @BindView(R.id.rlBottomSheetRating)
    RelativeLayout rlBottomSheetRating;
    EditText et_review;
    RatingBar rb_doctor;
    BottomSheetBehavior ratingSheet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_detail);
        ButterKnife.bind(this);

        requestID = getIntent().getStringExtra("id");
        getScheduledDetail(getIntent().getStringExtra("id"));
        buildGoogleApiClient();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        ratingSheet = BottomSheetBehavior.from(rlBottomSheetRating);
        et_review = rlBottomSheetRating.findViewById(R.id.et_review);
        rb_doctor = rlBottomSheetRating.findViewById(R.id.rb_doctor);
    }

    @OnClick({R.id.ivBack, R.id.btnCancelRequest, R.id.btnConfirm, R.id.btn_submit})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.btnCancelRequest:
                callCancelAPI();
                break;
            case R.id.btnConfirm:
                llInvoice.setVisibility(View.GONE);
                ratingSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
//                getCardAPI();
                break;
            case R.id.btn_submit:
                if (rb_doctor.getRating() < 1) {
                    showSnackBar(getString(R.string.please_rate));
                } else if (TextUtils.isEmpty(et_review.getText().toString().trim())) {
                    showSnackBar(getString(R.string.please_write_your_review));
                } else {
                    payNowAPICall("");
                }
                break;
            default:
                break;
        }
    }

    private void postRating(RatingRequest request) {
        new RatingModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                ratingSheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                finish();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {

            }

            @Override
            public void onNetworkFailure() {

            }
        }).postRating(request);
    }

    private void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(et_review, message, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.show();
    }

    private void getCardAPI() {
        CustomProgressbar mCustomProgressbar = new CustomProgressbar(this);
        mCustomProgressbar.setCancelable(false);
        mCustomProgressbar.show();

        Call<CardResponse> call = new ApiClient().getClient().create(ApiInterface.class).getCardDetails();
        call.enqueue(new Callback<CardResponse>() {
            @Override
            public void onResponse(@NonNull Call<CardResponse> call, @NonNull retrofit2.Response<CardResponse> response) {
                mCustomProgressbar.dismiss();
                if (response.isSuccessful()) {
                    try {
                        CardResponse card = response.body();
                        if (card.getCards() != null)
                            payNowAPICall(card.getCards().getCardId());
                        else
                            Toast.makeText(ScheduleDetailActivity.this, "No Cards Found", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CardResponse> call, @NonNull Throwable t) {
                Timber.e("Retrofit NetworkError" + t.getMessage());
                mCustomProgressbar.dismiss();
            }
        });

    }

    private void payNowAPICall(String cardId) {
        CustomProgressbar mCustomProgressbar = new CustomProgressbar(this);
        mCustomProgressbar.setCancelable(false);
        mCustomProgressbar.show();
        PaymentRequest payRequest = new PaymentRequest();
//        payRequest.setCard_id(cardId);
        payRequest.setPayment_mode("CASH");
        payRequest.setRequest_id(requestID);

        Call<BaseResponse> call = new ApiClient().getClient().create(ApiInterface.class).requestPayment(payRequest);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull retrofit2.Response<BaseResponse> response) {
                mCustomProgressbar.dismiss();
                String comment = et_review.getText().toString().trim();
                int rating = (int) rb_doctor.getRating();
                RatingRequest request = new RatingRequest();
                request.setComment(comment);
                request.setRequest_id(requestID);
                request.setRating(rating + "");
                postRating(request);
                /*if (response.isSuccessful()) {
                    try {
                        Bundle bundle = new Bundle();
                        bundle.putString("request_id", requestID);
                        Intent intent = new Intent(ScheduleDetailActivity.this, RatingActivity.class).putExtras(bundle);
                        startActivity(intent);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }*/
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                Timber.e("Retrofit NetworkError" + t.getMessage());
                mCustomProgressbar.dismiss();
            }
        });

    }

    private void callCancelAPI() {
        CustomProgressbar mCustomProgressbar = new CustomProgressbar(this);
        mCustomProgressbar.setCancelable(false);
        mCustomProgressbar.show();

        Call<BaseResponse> call = new ApiClient().getClient().create(ApiInterface.class).cancelRequest(requestID);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull retrofit2.Response<BaseResponse> response) {
                mCustomProgressbar.dismiss();
                if (response.isSuccessful()) {
                    try {
                        finish();
                        Toast.makeText(ScheduleDetailActivity.this, "Your request cancelled successfully!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                Timber.e("Retrofit NetworkError" + t.getMessage());
                mCustomProgressbar.dismiss();
            }
        });
    }

    private void getScheduledDetail(String id) {
       /* CustomProgressbar mCustomProgressbar = new CustomProgressbar(this);
        mCustomProgressbar.setCancelable(false);
        mCustomProgressbar.show();*/

        Call<CheckStatusResponse> call = new ApiClient().getClient().create(ApiInterface.class).checkStatusCallwithID(id);
        call.enqueue(new Callback<CheckStatusResponse>() {
            @Override
            public void onResponse(@NonNull Call<CheckStatusResponse> call, @NonNull retrofit2.Response<CheckStatusResponse> response) {
                //  mCustomProgressbar.dismiss();
                if (response.isSuccessful()) {
                    try {
                        CheckStatusResponse checkStatusResponse = response.body();
                        if (checkStatusResponse.getData() != null && checkStatusResponse.getData().size() > 0)
                            setData(checkStatusResponse);
                        if (!isMyServiceRunning(CheckStatusService.class)) {
                            Intent intent = new Intent(ScheduleDetailActivity.this, CheckStatusService.class);
                            intent.putExtra("isWithID", true);
                            intent.putExtra("id", requestID);
                            startService(intent);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CheckStatusResponse> call, @NonNull Throwable t) {
                Timber.e("Retrofit NetworkError" + t.getMessage());
                //   mCustomProgressbar.dismiss();
                statusLayout.setVisibility(View.GONE);
                llInvoice.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setData(CheckStatusResponse checkStatusResponse) {
        if (checkStatusResponse.getData().get(0).getStatus().equalsIgnoreCase("SCHEDULED") ||
                checkStatusResponse.getData().get(0).getStatus().equalsIgnoreCase("ARRIVED")) {
            if (!tvStatus.getText().toString().equalsIgnoreCase(checkStatusResponse.getData().get(0).getStatus())) {
                tvStatus.setText(checkStatusResponse.getData().get(0).getStatus());
                statusLayout.setVisibility(View.VISIBLE);
                llInvoice.setVisibility(View.GONE);
                btnCancelRequest.setVisibility(View.VISIBLE);
                setScheduleorStartedData(checkStatusResponse);
            }
        } else if (checkStatusResponse.getData().get(0).getStatus().equalsIgnoreCase("STARTED") ||
                checkStatusResponse.getData().get(0).getStatus().equalsIgnoreCase("PICKEDUP")) {
            if (!tvStatus.getText().toString().equalsIgnoreCase(getResources().getString(R.string.treatment_started))) {
                tvStatus.setText(R.string.treatment_started);
                statusLayout.setVisibility(View.VISIBLE);
                llInvoice.setVisibility(View.GONE);
                btnCancelRequest.setVisibility(View.GONE);
                setScheduleorStartedData(checkStatusResponse);
            }
        } else if (checkStatusResponse.getData().get(0).getStatus().equalsIgnoreCase("DROPPED") ||
                checkStatusResponse.getData().get(0).getStatus().equalsIgnoreCase("COMPLETED") && checkStatusResponse.getData().get(0).getPaid().equals(0)) {
            if (!tvStatus.getText().toString().equalsIgnoreCase(getResources().getString(R.string.treatment_completed))) {
                tvStatus.setText(R.string.treatment_completed);
                statusLayout.setVisibility(View.GONE);
                llInvoice.setVisibility(View.VISIBLE);
                String base_rate = getApplicationInstance().getCurrency() + checkStatusResponse.getData().get(0).getService_type().getPrice() + "/min";
                tv_base_rate.setText(base_rate);
                tv_pay_doctor.setText(MessageFormat.format("{0}{1}", getApplicationInstance().getCurrency(), checkStatusResponse.getData().get(0).getPayment().getFixed()));
                tv_ztoid_fee.setText(MessageFormat.format("{0}{1}", getApplicationInstance().getCurrency(), checkStatusResponse.getData().get(0).getPayment().getCommision()));
                tv_promotion.setText(MessageFormat.format("{0}{1}", getApplicationInstance().getCurrency(), "0"));
                tv_balance_wallet.setText(MessageFormat.format("{0}{1}", getApplicationInstance().getCurrency(), "0"));
                tv_tax.setText(MessageFormat.format("{0}{1}", getApplicationInstance().getCurrency(), checkStatusResponse.getData().get(0).getPayment().getTax()));
                tv_total.setText(MessageFormat.format("{0}{1}", getApplicationInstance().getCurrency(), checkStatusResponse.getData().get(0).getPayment().getTotal()));
                tv_amount_paid.setText(MessageFormat.format("{0}{1}", getApplicationInstance().getCurrency(), checkStatusResponse.getData().get(0).getPayment().getTotal()));
            }
        } else if (checkStatusResponse.getData().get(0).getStatus().equalsIgnoreCase("COMPLETED") && checkStatusResponse.getData().get(0).getPaid().equals(1)) {
            /*Bundle bundle = new Bundle();
            bundle.putString("request_id", requestID);
            Intent intent = new Intent(ScheduleDetailActivity.this, RatingActivity.class).putExtras(bundle);
            startActivity(intent);
            finish();*/
        }


        if (checkStatusResponse.getData().get(0).getStatus().equalsIgnoreCase("SEARCHING")) {
            if (!tvStatus.getText().toString().equalsIgnoreCase(checkStatusResponse.getData().get(0).getStatus())) {
                tvStatus.setText(checkStatusResponse.getData().get(0).getStatus());
                statusLayout.setVisibility(View.VISIBLE);
                llInvoice.setVisibility(View.GONE);
                btnCancelRequest.setVisibility(View.VISIBLE);
                setScheduleorStartedData(checkStatusResponse);
            }
        }

    }

    private void setScheduleorStartedData(CheckStatusResponse checkStatusResponse) {
        tv_doctor_name.setText(checkStatusResponse.getData().get(0).getProvider().getFirstName() + " " + checkStatusResponse.getData().get(0).getProvider().getLastName());
        tv_doctor_spl.setText(checkStatusResponse.getData().get(0).getService_type().getName());
        Glide.with(this).load(Constants.URL.BASE_URL_STORAGE + checkStatusResponse.getData().get(0).getProvider().getAvatar()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user)).into(iv_profile);
        rb_doctor.setRating(Float.parseFloat(checkStatusResponse.getData().get(0).getProvider().getRating()));

        tv_treatment.setText(checkStatusResponse.getData().get(0).getService_type().getName());
        String base_rate = getApplicationInstance().getCurrency() + checkStatusResponse.getData().get(0).getService_type().getPrice() + "/min";
        tv_rate_txt.setText(base_rate);
        tv_desc_txt.setText(checkStatusResponse.getData().get(0).getService_type().getDescription());
        if (checkStatusResponse.getData().get(0).getScheduleAt() != null && !checkStatusResponse.getData().get(0).getScheduleAt().equalsIgnoreCase("null")) {
            tvDate.setText(checkStatusResponse.getData().get(0).getScheduleAt().split(" ")[0]);
            tvTime.setText(checkStatusResponse.getData().get(0).getScheduleAt().split(" ")[1]);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CodeSnippet.USER_ACTION);
        registerReceiver(myReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(myReceiver);
        if (isMyServiceRunning(CheckStatusService.class)) {
            stopService(new Intent(ScheduleDetailActivity.this, CheckStatusService.class));
        }
    }

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            CheckStatusResponse response;
            if (intent != null && intent.getExtras() != null) {

                response = intent.getParcelableExtra("response");
                if (response.getData().size() > 0) {
                    if (intent.getParcelableExtra("response") != null) {
                        response = intent.getParcelableExtra("response");
                        Log.e("onReceive Response", response.toString());

                        setData(response);
                    }
                }
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);

        try {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style_json));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //mMap.getUiSettings().setAllGesturesEnabled(false);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            changeMapLocation(location);
        }
    }

    private void changeMapLocation(Location location) {
        if (mMap != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            float zoom = 15.0f;
            if (mCtLocationMarker == null) {
                mCtLocationMarker = mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location))
                        .anchor(0.5f, 0.5f)
                        .position(latLng));
            }

            boolean contains = mMap.getProjection()
                    .getVisibleRegion()
                    .latLngBounds
                    .contains(latLng);

            if (!contains) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.reconnect();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.reconnect();
        }
    }


    private Handler mCheckStatusHandler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            mCheckStatusHandler.postDelayed(runnable, 5000);
            Log.e("ScheduleDetailActivity", "running");

            getScheduledDetail(requestID);


        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        mCheckStatusHandler.postDelayed(runnable, 5000);
        Log.e("ScheduleDetailActivity", "started");
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCheckStatusHandler.removeCallbacks(runnable);
        Log.e("ScheduleDetailActivity", "stoped");
    }

}
