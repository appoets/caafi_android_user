package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.model.dto.response.Cards;
import com.caafisom.android.user.presenter.ipresenter.IWalletPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IWalletView extends IView<IWalletPresenter> {
    void setUp();
    void onSuccess(Cards cards);
    void onAddCard();
    void getSuccessResponse();
    void getFailureResponse();
//    void onDeleteCard();
}
