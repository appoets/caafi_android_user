package com.caafisom.android.user.presenter.ipresenter;

import com.caafisom.android.user.model.dto.request.ChangePasswordRequest;

public interface IChangePasswordPresenter extends IPresenter {
    void goToLogin();
    void changePassword(ChangePasswordRequest request);
}