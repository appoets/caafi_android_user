package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.response.HistoryResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class HistoryModel extends BaseModel<HistoryResponse> {

    public HistoryModel(IModelListener<HistoryResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(HistoryResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<HistoryResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getHistoryList() {
//        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getHistory());
        enQueueListTask(new ApiClient().getClient().create(ApiInterface.class).getHistory());
    }

}
