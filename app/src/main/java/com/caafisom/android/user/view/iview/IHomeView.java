package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.model.dto.response.ProfileResponse;
import com.caafisom.android.user.presenter.ipresenter.IHomePresenter;

public interface IHomeView extends IView<IHomePresenter> {
        void updateUserDetails(ProfileResponse response);
        void goToHome();
        void goToHistory();
        void goToSchedule();
        void goToVideoCall();
        void goToHelp();
        void goToWallet();
        void setUp();
        void showGoldenMins();
        void updateNotification(int count);
}
