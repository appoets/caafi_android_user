package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.response.ScheduleResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class ScheduleModel extends BaseModel<ScheduleResponse> {


    public ScheduleModel(IModelListener<ScheduleResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(ScheduleResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<ScheduleResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getScheduleList() {
        enQueueListTask(new ApiClient().getClient().create(ApiInterface.class).getScheduledList(1));
    }
}
