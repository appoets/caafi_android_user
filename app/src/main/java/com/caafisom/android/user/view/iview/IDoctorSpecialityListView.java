package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.model.dto.response.DoctorsSpecialityList;
import com.caafisom.android.user.model.dto.response.SpecialityList;
import com.caafisom.android.user.presenter.DocotorSpecialityListPresenter;
import com.caafisom.android.user.presenter.SpecialityListPresenter;
import com.caafisom.android.user.view.adapter.DoctorSpecialityListRecyclerAdapter;
import com.caafisom.android.user.view.adapter.SpecialityListRecyclerAdapter;

/**
 * Created by Tranxit Technologies.
 */

public interface IDoctorSpecialityListView extends IView<DocotorSpecialityListPresenter> {
    void setAdapter(DoctorSpecialityListRecyclerAdapter adapter);
    void getDoctorsList(DoctorsSpecialityList data);
    void initSetUp();
}
