package com.caafisom.android.user.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.caafisom.android.user.common.Constants;


public class PreferencesUtils {

    private SharedPreferences preference = null;

    public PreferencesUtils() {
    }

    private SharedPreferences getPreferenceInstance(Context context) {
        if (preference != null) return preference;
        else {
            preference = context.getSharedPreferences(Constants.SharedPref.SHARED_PREF, Context.MODE_PRIVATE);
            return preference;
        }
    }

    public void setSharedPrefValue(Context context, String key, String value) {
        getPreferenceInstance(context);
        Editor editor = preference.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void setSharedPrefValue(Context context, String key, int value) {
        getPreferenceInstance(context);
        Editor editor = preference.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void setSharedPrefValue(Context context, String key, Boolean value) {
        getPreferenceInstance(context);
        Editor editor = preference.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void setSharedPrefValue(Context context, String key, long value) {
        getPreferenceInstance(context);
        Editor editor = preference.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public void setSharedPrefValue(Context context, String key, double value) {
        getPreferenceInstance(context);
        Editor editor = preference.edit();
        editor.putLong(key, Double.doubleToRawLongBits(value));
        editor.apply();
    }

    public Boolean getBooleanValue(Context context, String key) {
        return getPreferenceInstance(context).getBoolean(key, false);
    }

    public Boolean getBooleanValue(Context context, String key, boolean defaultValue) {
        return getPreferenceInstance(context).getBoolean(key, defaultValue);
    }

    public int getIntValue(Context context, String key) {
        return getPreferenceInstance(context).getInt(key, -1);
    }

    public int getIntValue(Context context, String key, int defaultValue) {
        return getPreferenceInstance(context).getInt(key, defaultValue);
    }

    public String getStringValue(Context context, String key) {
        return getPreferenceInstance(context).getString(key, null);
    }

    public String getStringValue(Context context, String key, String defaultValue) {
        return getPreferenceInstance(context).getString(key, defaultValue);
    }

    public long getLongValue(Context context, String key) {
        return getPreferenceInstance(context).getLong(key, 0);
    }

    public Double getDouble(Context context, String key) {
        return Double.longBitsToDouble(getPreferenceInstance(context).getLong(key, Double.doubleToRawLongBits(0)));
    }

    public void clearPreference(Context context) {
        getPreferenceInstance(context);
        Editor editor = preference.edit();
        editor.clear();
        editor.apply();
    }

}
