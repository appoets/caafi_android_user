package com.caafisom.android.user.view.activity;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.presenter.VideoCallPresenter;
import com.caafisom.android.user.presenter.ipresenter.IVideoCallPresenter;
import com.caafisom.android.user.view.adapter.NotificationRecyclerAdapter;
import com.caafisom.android.user.view.iview.IVideoCallView;

import butterknife.BindView;
import butterknife.OnClick;

public class VideoCallActivity extends BaseActivity<IVideoCallPresenter> implements IVideoCallView {

    @BindView(R.id.mainLayout)
    ConstraintLayout mainLayout;
    @BindView(R.id.llNoHistory)
    ConstraintLayout llNoHistory;
    @BindView(R.id.ibBack)
    ImageView ibBack;
    @BindView(R.id.rcvVideoCalList)
    RecyclerView rcvVideoCalList;

    @Override
    protected int attachLayout() {
        return R.layout.activity_videocall;
    }

    @Override
    IVideoCallPresenter initialize() {
        return new VideoCallPresenter(this);
    }

    @Override
    public void setAdapter(NotificationRecyclerAdapter adapter) {
        if (adapter.getItemCount() > 0) {
            llNoHistory.setVisibility(View.GONE);
            mainLayout.setVisibility(View.VISIBLE);
            rcvVideoCalList.setAdapter(adapter);
        } else {
            mainLayout.setVisibility(View.GONE);
            llNoHistory.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.ibBack})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void initSetUp() {
        rcvVideoCalList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvVideoCalList.setItemAnimator(new DefaultItemAnimator());
    }
}
