package com.caafisom.android.user.presenter;

import com.caafisom.android.user.model.CustomException;
import com.caafisom.android.user.model.dto.common.Provider;
import com.caafisom.android.user.model.dto.response.ProviderResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.ProviderModel;
import com.caafisom.android.user.presenter.ipresenter.IDoctorFlowPresenter;
import com.caafisom.android.user.view.adapter.ProviderRecyclerAdapter;
import com.caafisom.android.user.view.adapter.listener.IProviderRecyclerAdapter;
import com.caafisom.android.user.view.iview.IDoctorFlowView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class DoctorFlowPresenter extends BasePresenter<IDoctorFlowView> implements IDoctorFlowPresenter {

    public DoctorFlowPresenter(IDoctorFlowView iView) {
        super(iView);
    }

    IProviderRecyclerAdapter iProviderRecyclerAdapter = new IProviderRecyclerAdapter() {
        @Override
        public void onClickItem(int pos, Provider data) {
            iView.moveToDetailView(data);
        }


    };

    @Override
    public void makeVideoCall(Provider data) {
        iView.makeVideoCall(data);
    }

    @Override
    public void getProviderList() {
        iView.showProgressbar();
        new ProviderModel(new IModelListener<ProviderResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProviderResponse response) {
                iView.dismissProgressbar();
                iView.setAdapter(new ProviderRecyclerAdapter(response.getProvider(), iProviderRecyclerAdapter));
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProviderResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).getProviderList();
    }

    @Override
    public void searchProvider(String searchKey) {
        // iView.showProgressbar();
        new ProviderModel(new IModelListener<ProviderResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProviderResponse response) {
                //    iView.dismissProgressbar();
                iView.setAdapter(new ProviderRecyclerAdapter(response.getProvider(), iProviderRecyclerAdapter));
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProviderResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                //   iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                //   iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                //   iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).searchProvider(searchKey);
    }


    @Override
    public void sendRequestVideoCall(Integer serviceTypeId, Integer providerID, String date, String time, String broadcast) {
        iView.showProgressbar();
        new ProviderModel(new IModelListener<ProviderResponse>() {

            @Override
            public void onSuccessfulApi(@NotNull ProviderResponse response) {
                iView.dismissProgressbar();
                iView.makeVideoCallPopUP();
//                iView.startCheckStatus(response.getRequest_id());
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProviderResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).sendRequestVideoCall(serviceTypeId, String.valueOf(providerID), date, time, broadcast);
    }


}
