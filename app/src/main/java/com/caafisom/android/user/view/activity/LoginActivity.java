package com.caafisom.android.user.view.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.caafisom.android.user.R;
import com.caafisom.android.user.common.Constants;
import com.caafisom.android.user.model.dto.request.LoginRequest;
import com.caafisom.android.user.presenter.LoginPresenter;
import com.caafisom.android.user.presenter.ipresenter.ILoginPresenter;
import com.caafisom.android.user.view.iview.ILoginView;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity<ILoginPresenter> implements ILoginView {

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.btnSignIn)
    Button btnSignIn;

    @BindView(R.id.tvForgotpassword)
    TextView tvForgotpassword;

    @BindView(R.id.tvSignUP)
    TextView tvSignUP;

    @BindView(R.id.llRegister)
    LinearLayout llRegister;

    @Override
    protected int attachLayout() {
        return R.layout.activity_login;
    }

    @Override
    ILoginPresenter initialize() {
        return new LoginPresenter(this);
    }

    @OnClick({R.id.btnSignIn, R.id.tvForgotpassword, R.id.llRegister,R.id.tvSignUP})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSignIn:
                validateSigIn();
                break;
            case R.id.tvForgotpassword:
                iPresenter.goToForgotPassword();
                break;

            case R.id.tvSignUP:
                iPresenter.goToRegistration();
                break;
        }
    }

    private void validateSigIn() {
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            showSnackBar(getString(R.string.please_enter_email));
        }else if (!getCodeSnippet().isEmailValid(email)){
            showSnackBar(getString(R.string.please_enter_valid_email));
        }else if (TextUtils.isEmpty(password)){
            showSnackBar(getString(R.string.please_enter_password));
        }else{
            LoginRequest request = new LoginRequest();
            request.setUsername(email);
            request.setPassword(password);
            request.setDevice_id(Constants.WebConstants.DEVICE_ID);
            request.setDevice_token(Constants.WebConstants.DEVICE_TOKEN);
            request.setDevice_type(Constants.WebConstants.DEVICE_TYPE);
            request.setGrant_type(Constants.WebConstants.GRANT_TYPE);
            request.setClient_id(Constants.WebConstants.CLINET_ID);
            request.setClient_secret(Constants.WebConstants.CLIENT_SCERET);
            iPresenter.postLogin(request);
        }
    }


    @Override
    public void goToRegistration() {
        navigateTo(RegisterActivity.class,false,new Bundle());
    }


    @Override
    public void goToForgotPassword() {
        navigateTo(ForgotPasswordActivity.class,false,new Bundle());
    }

    @Override
    public void goToHome() {
        navigateTo(HomeActivity.class,false,new Bundle());
        finishAffinity();
    }
}
