package com.caafisom.android.user.view.iview;

import com.caafisom.android.user.presenter.ipresenter.IOneTimePasswordPresenter;

public interface IOneTimePasswordView extends IView<IOneTimePasswordPresenter> {
        void goToForgotChangePassword();
        void setUp();
}
