package com.caafisom.android.user.view.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.caafisom.android.user.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GoldenMinutesDialog extends DialogFragment {

    @BindView(R.id.btnDismiss)
    Button btnDismiss;
    @BindView(R.id.btnCall911)
    Button btnCall911;
    @BindView(R.id.btnVideoToER)
    Button btnVideoToER;

    public static GoldenMinutesDialog newInstance() {
        return new GoldenMinutesDialog();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.Theme_AppCompat_Dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_golden_minutes_dialog, container, false);
        ButterKnife.bind(this, view);
        return view;
    }




    @OnClick({R.id.btnDismiss,R.id.btnCall911,R.id.btnVideoToER})
    public void OnClick(View view) {
        switch (view.getId()){
            case R.id.btnDismiss:
            case R.id.btnVideoToER:
                getDialog().dismiss();
                break;

            case R.id.btnCall911:
                dialPhone();
                break;

        }
    }

    private void dialPhone() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+"911"));
        if (intent.resolveActivityInfo(getActivity().getPackageManager(), intent.getFlags()) != null)
            startActivity(intent);
        else
            showToast(getString(R.string.call_feature_not_supported));
    }

    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

}
