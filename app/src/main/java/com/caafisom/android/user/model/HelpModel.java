package com.caafisom.android.user.model;

import com.caafisom.android.user.model.dto.response.HelpResponse;
import com.caafisom.android.user.model.listener.IModelListener;
import com.caafisom.android.user.model.webservice.ApiClient;
import com.caafisom.android.user.model.webservice.ApiInterface;

import java.util.List;

public class HelpModel extends BaseModel<HelpResponse> {

    public HelpModel(IModelListener<HelpResponse> listener) {
        super(listener);
    }

    public void getHelpDetails(){
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getHelpDetails());
    }

    @Override
    public void onSuccessfulApi(HelpResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<HelpResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
